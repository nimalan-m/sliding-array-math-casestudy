#pragma once

#include <cooperative_groups.h>

#ifndef __HIP_PLATFORM_HCC__
#include "compute_helpers.cuh"
#else
#include "compute_helpers.cch"
#endif

namespace cg = cooperative_groups;

__device__ inline void Comparator(float &A, float &B, uint dir) {
  float t;

  if ((A > B) == dir) {
    t = A;
    A = B;
    B = t;
  }
}

__global__ void bitonicSortKernelLarge(float *in, uint *wsizes,
                                       params_t *params) {

  uint block_size_offset = blockIdx.x / (params->k);
  uint nelem = wsizes[block_size_offset];

  if (nelem > 1) {
    cg::thread_block cta = cg::this_thread_block();
    __shared__ float shmem[SHARED_MEMORY_SIZE];

    in += blockIdx.x * SHARED_MEMORY_SIZE + threadIdx.x;

    shmem[threadIdx.x] = in[0];
    shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)] =
        in[(SHARED_MEMORY_SIZE / 2)];

    // Bitonic merge
    for (uint size = 2; size < SHARED_MEMORY_SIZE; size <<= 1) {
      uint ddd = (threadIdx.x & (size / 2)) != 0;

      for (uint stride = size / 2; stride > 0; stride >>= 1) {
        cg::sync(cta);
        uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
        Comparator(shmem[pos + 0], shmem[pos + stride], ddd);
      }
    }

    uint ddd = blockIdx.x & 1;
    {
      for (uint stride = SHARED_MEMORY_SIZE / 2; stride > 0; stride >>= 1) {
        cg::sync(cta);
        uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
        Comparator(shmem[pos + 0], shmem[pos + stride], ddd);
      }
    }

    cg::sync(cta);

    in[0] = shmem[threadIdx.x];
    in[(SHARED_MEMORY_SIZE / 2)] =
        shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)];
  }
}

__global__ void bitonicMergeGlobalMemory(float *in, uint arrayLen, uint *wsizes_d,
                                   params_t *params, uint size, uint stride,
                                   uint dir) {
  uint block_size_offset = (blockIdx.x) / (2 * params->k);
  uint nelem = wsizes_d[block_size_offset];

  if (nelem > 1) {
    uint global_comparatorI = blockIdx.x * blockDim.x + threadIdx.x;
    uint comparatorI = global_comparatorI & (arrayLen / 2 - 1);

    // Bitonic merge
    uint ddd = dir ^ ((comparatorI & (size / 2)) != 0);
    uint pos = 2 * global_comparatorI - (global_comparatorI & (stride - 1));

    float A = in[pos];
    float B = in[pos + stride];

    Comparator(A, B, ddd);

    in[pos] = A;
    in[pos + stride] = B;
  }
}

__global__ void bitonicMergeSharedMemory(float *in, uint arrayLength, uint *wsizes_d,
                                   params_t *params, uint size, uint dir) {
  uint block_size_offset = blockIdx.x / (params->k);
  uint nelem = wsizes_d[block_size_offset];

  if (nelem > 1) {
    cg::thread_block cta = cg::this_thread_block();
    __shared__ float shmem[SHARED_MEMORY_SIZE];

    in += blockIdx.x * SHARED_MEMORY_SIZE + threadIdx.x;

    shmem[threadIdx.x] = in[0];
    shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)] = in[SHARED_MEMORY_SIZE / 2];

    uint comparatorI =
        UMAD(blockIdx.x, blockDim.x, threadIdx.x) & ((arrayLength / 2) - 1);
    uint ddd = dir ^ ((comparatorI & (size / 2)) != 0);

    for (uint stride = SHARED_MEMORY_SIZE / 2; stride > 0; stride >>= 1) {
      cg::sync(cta);
      uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
      Comparator(shmem[pos], shmem[pos + stride], ddd);
    }

    cg::sync(cta);
    in[0] = shmem[threadIdx.x];
    in[SHARED_MEMORY_SIZE / 2] = shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)];
  }
}

inline void bitonic_sort_network_stage(float *windows_d, uint real_wsize, uint size,
                            uint *wsizes_d, params_t *params_d, uint dir,
                            uint blockCount, uint threadCount,
                            cudaStream_t *stream) {
  for (unsigned stride = size / 2; stride > 0; stride >>= 1) {
    if (stride >= SHARED_MEMORY_SIZE) {
      bitonicMergeGlobalMemory<<<blockCount * 2, threadCount / 2, 0, *stream>>>(
          windows_d, real_wsize, wsizes_d, params_d, size, stride, dir);
    } else {
      bitonicMergeSharedMemory<<<blockCount, threadCount, 0, *stream>>>(
          windows_d, real_wsize, wsizes_d, params_d, size, dir);
      break;
    }
  }
}

inline void bitonic_sort(devicemem_t *device, params_t *params,
                         cudaStream_t *stream, uint blockCount,
                         uint threadCount, uint dir) {
  bitonicSortKernelLarge<<<blockCount, threadCount, 0, *stream>>>(
      device->windows_d, device->wsizes_d, device->params_d);

  for (uint size = 2 * SHARED_MEMORY_SIZE; size <= params->real_wsize;
       size <<= 1) {
    bitonic_sort_network_stage(device->windows_d, params->real_wsize, size,
                    device->wsizes_d, device->params_d, dir, blockCount,
                    threadCount, stream);
  }
}

inline void bitonic_merge(devicemem_t *device, params_t *params,
                              cudaStream_t *stream, uint blockCount,
                              uint threadCount, uint dir) {
  bitonic_sort_network_stage(device->windows_d, params->real_wsize, params->real_wsize,
                  device->wsizes_d, device->params_d, dir, blockCount,
                  threadCount, stream);
}
