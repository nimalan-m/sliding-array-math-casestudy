# Default benchmark
declare -a THREADS=(1 2 4 6 8)
declare -a SIZE=(256 512 1024 2048)
declare -a VALIDPIX=(0.75 0.8)
