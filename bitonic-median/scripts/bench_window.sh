#!/usr/bin/env bash
set -e
set -x

coach_dir=$(dirname "${BASH_SOURCE[0]}")
coach=$coach_dir/coach
build_dir=build-window

declare -a THREADS=(1)
declare -a SIZE=(1024)
declare -a VALIDPIX=(0.75)
declare -a WSIZE=(63 127 255 511)
declare OPT=CUDA

function run_bench_mask_array() {
  dir=$1
  dim=$2
  valpix=$3
  wsize=$4

  shift 4

  echo "coach bench -b $dir -- --sizex $dim --sizey $dim --masked --validpix $valpix --wsize $wsize $@"
  $coach bench -b $dir -- --sizex $dim --sizey $dim --masked --validpix $valpix --wsize $wsize $@
}

do_build=true

if [ $# -ge 1 ]
then
    do_build=$1
    shift
fi

if $do_build ; then
  $coach build -b $build_dir -o $OPT
fi

# casacore MaskedArray
for size in "${SIZE[@]}"
do
  for sparsity in "${VALIDPIX[@]}"
  do
    for wsize in "${WSIZE[@]}"
    do
      run_bench_mask_array $build_dir $size $sparsity $wsize --block 32 $@
    done
  done
done
