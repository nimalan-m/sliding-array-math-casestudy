#!/usr/bin/env bash
set -e
set -x

coach_dir=$(dirname "${BASH_SOURCE[0]}")
coach=$coach_dir/coach
build_dir=build-cuda

source $coach_dir/params.sh

function run_bench_array() {
  dir=$1
  dim=$2

  shift 2

  echo "coach bench -b $dir -- --sizex $dim --sizey $dim $@"
  $coach bench -b $dir -- --sizex $dim --sizey $dim $@
}

function run_bench_mask_array() {
  dir=$1
  dim=$2
  valpix=$3

  shift 3

  echo "coach bench -b $dir -- --sizex $dim --sizey $dim --masked --validpix $valpix $@"
  $coach bench -b $dir -- --sizex $dim --sizey $dim --masked --validpix $valpix $@
}

do_build=true

if [ $# -ge 1 ]
then
    do_build=$1
    shift
fi

if $do_build ; then
  $coach build -b $build_dir -o CUDA
fi

# casacore Array
for size in "${SIZE[@]}"
do
  run_bench_array $build_dir $size $@
done

# casacore MaskedArray
for size in "${SIZE[@]}"
do
  for sparsity in "${VALIDPIX[@]}"
  do
    run_bench_mask_array $build_dir $size $sparsity $@
  done
done

