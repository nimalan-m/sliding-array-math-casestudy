#!/usr/bin/env bash
set -e
set -x

coach_dir=$(dirname "${BASH_SOURCE[0]}")
coach=$coach_dir/coach
build_dir=build-omp

declare -a THREADS=(1 2 4 6 8 12 16 24 32 48 64)
declare -a SIZE=(256 512 1024 2048)
declare -a VALIDPIX=(0.75 0.8)

function run_bench_array() {
  dir=$1
  dim=$2

  shift 2

  echo "coach bench -b $dir -- --sizex $dim --sizey $dim $@"
  $coach bench -b $dir -- --sizex $dim --sizey $dim $@
}

function run_bench_mask_array() {
  dir=$1
  dim=$2
  valpix=$3

  shift 3

  echo "coach bench -b $dir -- --sizex $dim --sizey $dim --masked --validpix $valpix $@"
  $coach bench -b $dir -- --sizex $dim --sizey $dim --masked --validpix $valpix $@
}

do_build=true

if [ $# -ge 1 ]
then
    do_build=$1
    shift
fi

if $do_build ; then
  $coach build -b $build_dir
fi

# casacore Array
for th in "${THREADS[@]}"
do
  echo "export OMP_NUM_THREADS=$th"
  export OMP_NUM_THREADS=$th
  for size in "${SIZE[@]}"
  do
    run_bench_array $build_dir $size $@
  done
done

# casacore MaskedArray
for th in "${THREADS[@]}"
do
  echo "export OMP_NUM_THREADS=$th"
  export OMP_NUM_THREADS=$th
  for size in "${SIZE[@]}"
  do
    for sparsity in "${VALIDPIX[@]}"
    do
      run_bench_mask_array $build_dir $size $sparsity $@
    done
  done
done

