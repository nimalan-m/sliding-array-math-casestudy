#include <casacore/casa/Arrays.h>
#include <cassert>
#include <chrono>
#include <cstdio>
#include <iostream>

#include "helpers.hh"
#include "ArrayMathOpt.h"

namespace casa = casacore;

static const float correctionFactor = 0.6744888;

int main(int argc, char **argv) {
  // size_t sizex = 16;
  // size_t sizey = 16;
  // size_t wsize = 1;
  // uint block_size = 4;

  args::parse_args("tbitonicMedianCASA", argc, argv);

  size_t sizex = args::sizex;
  size_t sizey = args::sizey;
  size_t wsize = args::wsize;
  uint block_size = args::block;
  bool enable_assertion = args::assertion;
  bool test_masked = args::masked;
  float validpix = args::validpix;

  casa::IPosition shape(2, sizex, sizey);

  casa::Array<casa::Float> matrix(shape);
  float range = 5.0f;
  // float threshold = -1.0f;
  float threshold = range - (range * validpix);

  std::srand(time(0));
  for (int j = 0; j < shape[1]; j++) {
    for (int i = 0; i < shape[0]; i++) {
      auto val = (double)std::rand() / (double)(RAND_MAX / range);
      matrix(casacore::IPosition(2, i, j)) = val;
    }
  }
  casa::MaskedArray<casa::Float> masked_matrix(matrix, (matrix > threshold));

  if (test_masked) {
    auto mask = masked_matrix.getMask();
    bool *maskData = mask.data();
    auto cnt = 0;
    auto sz = masked_matrix.size();
    for (size_t i = 0; i < masked_matrix.size(); i++) {
      if (maskData[i])
        cnt++;
    }
    std::printf("Current array validpix: %.2f\n", (float)cnt / sz);
  }

  // std::cout << shape << " " << shape[0] << " " << shape[1] << std::endl;
  // std::cout << matrix << std::endl;
  // std::cout << masked_matrix << std::endl;

  casa::Array<casa::Float> medianCASA, rmsCASA;
  auto hboxsz = casa::IPosition(2, wsize, wsize);

  if (enable_assertion) {
    helpers::WallClock clock;
    clock.tick();

    if (test_masked) {
      medianCASA = casa::slidingArrayMath(
          masked_matrix, hboxsz, casa::MaskedMedianFunc<casa::Float>());

      rmsCASA = casa::slidingArrayMath(masked_matrix, hboxsz,
                                       casa::MaskedMadfmFunc<casa::Float>()) /
                correctionFactor;
    } else {
      medianCASA = casa::slidingArrayMath(matrix, hboxsz,
                                          casa::MedianFunc<casa::Float>());

      rmsCASA = casa::slidingArrayMath(matrix, hboxsz,
                                       casa::MadfmFunc<casa::Float>()) /
                correctionFactor;
    }

    std::cout << "CASA wall clock time taken: " << clock.elapsedTime() << "ms"
              << std::endl;
  }

#ifdef DEBUG
  std::cout << medianCASA << std::endl;
#endif

  casa::Array<casa::Float> medianRewrite, rmsRewrite;
  {

    // Note Wall clock time is not accurate for GPUs internally the driver
    // will print the time taken based on Cuda Events, use that for measuring
    // performance

    helpers::WallClock clock;
    clock.tick();

    if (test_masked) {
      arraymath::slidingArrayMedianMADFM(medianRewrite, rmsRewrite, masked_matrix,
                                     hboxsz, block_size);
    } else {
      arraymath::slidingArrayMedianMADFM(medianRewrite, rmsRewrite, matrix, hboxsz,
                                     block_size);
    }

    std::cout << "Rewrite version wall clock time taken: "
              << clock.elapsedTime() << "ms" << std::endl;

#ifdef DEBUG
    std::cout << medianRewrite << std::endl;
#endif
  }

  if (enable_assertion) {
    std::cout << "Asserting Median" << std::endl;
    helpers::assertArray(medianCASA, medianRewrite);
    std::cout << "Asserting RMS" << std::endl;
    helpers::assertArray(rmsCASA, rmsRewrite);
  }
}
