#pragma once

#include <casacore/casa/Arrays.h>
#include <chrono>
#include <cstdlib>
#include <getopt.h>
#include <iostream>
#include <unistd.h>

#define TOLERANCE 0.01f

namespace args {

static int sizex = 1024;
static int sizey = 1024;
static int wsize = 50;
static int block = 64;
static int masked = false;
static int assertion = false;
static float validpix = 0.75f;
static int disp_help = false;

static struct option long_option[] = {
    {.name = "sizex", .has_arg = required_argument, .val = 'x'},
    {.name = "sizey", .has_arg = required_argument, .val = 'y'},
    {.name = "wsize", .has_arg = required_argument, .val = 'w'},
    {.name = "block", .has_arg = required_argument, .val = 'b'},
    {.name = "masked", .has_arg = no_argument, .flag = &masked, .val = 'm'},
    {.name = "assertion",
     .has_arg = no_argument,
     .flag = &assertion,
     .val = 'a'},
    {.name = "validpix", .has_arg = required_argument, .val = 's'},
    {.name = "help", .has_arg = no_argument, .flag = &disp_help, .val = 'h'},
    {0, 0, 0, 0}};

void display_help_exit(const char *progname) {
  printf("Usage: %s [options]\n"
         "Options:\n"
         "\t--sizex <int>        - Size of array in x dimension\n"
         "\t--sizey <int>        - Size of array in y dimension\n"
         "\t--wsize <int>        - Size of window(half box size)\n"
         "\t--block <int>        - Size of block(GPU code only)\n"
         "\t--assertion          - If set will compare result against CASA\n"
         "\t--masked             - Use casacore MaskedArray(there is a "
         "performance difference between Array and MaskedArray)\n"
         "\t--validpix <float> - Spareness of MaskedArray(only when masked "
         "is enabled)\n",
         progname);
  exit(0);
}

void parse_args(char *progname, int argc, char **argv) {
  int opt_idx = 0;
  int c;
  while ((c = getopt_long(argc, argv, "x:y:w:mas:h", long_option, &opt_idx)) !=
         -1) {
    switch (c) {
    case 0:
      break;
    case 'x':
      sizex = std::atoi(optarg);
      break;
    case 'y':
      sizey = std::atoi(optarg);
      break;
    case 'w':
      wsize = std::atoi(optarg);
      break;
    case 'b':
      block = std::atoi(optarg);
      break;
    case 's':
      validpix = std::atof(optarg);
      break;
    case 'h':
    case '?': // Pass through
    default:
      display_help_exit(progname);
    }
  }

  if (optind < argc) {
    display_help_exit(progname);
  }

  printf("Configuration for benchmark\n"
         "Sizex: %d\n"
         "Sizey: %d\n"
         "Wsize: %d\n"
         "Asserting values: %s\n"
         "Masked values: %s\n"
         "Sparseness: %f\n"
         "",
         sizex, sizey, wsize, assertion ? "true" : "false",
         masked ? "true" : "false", validpix);
}

} // namespace args

namespace helpers {
namespace casa = casacore;

void assertArray(casa::Array<casa::Float> &m1, casa::Array<casa::Float> &m2) {

  assert(m1.ndim() == m2.ndim());
  for (int i = 0; i < m1.ndim(); i++) {
    assert(m1.shape()[i] == m2.shape()[i]);
  }

  for (ssize_t i = 0; i < m1.shape()[0]; i++) {
    for (ssize_t j = 0; j < m1.shape()[1]; j++) {

      float val =
          fabs(m1(casa::IPosition(2, i, j)) - m2(casa::IPosition(2, i, j)));

      if (val > TOLERANCE) {
        std::cerr << "Assertion failed at " << std::endl;
        std::cout << "i : " << i << " j: " << j << " diff: " << val
                  << std::endl;
        std::cout << "expected: " << m1(casa::IPosition(2, i, j)) << " vs "
                  << "actual: " << m2(casa::IPosition(2, i, j)) << std::endl;
        assert(val < TOLERANCE);
      }
    }
  }
}

class WallClock {
  std::chrono::steady_clock::time_point begin;

public:
  void tick() { begin = std::chrono::steady_clock::now(); }
  double elapsedTime() {
    std::chrono::steady_clock::time_point end =
        std::chrono::steady_clock::now();
    return std::chrono::duration_cast<std::chrono::milliseconds>(end - begin)
        .count();
  }
};
} // namespace helpers
