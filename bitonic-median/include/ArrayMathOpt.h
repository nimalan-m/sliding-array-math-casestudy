#ifndef ARRAY_MATH_OPT_H
#define ARRAY_MATH_OPT_H

#include <casacore/casa/Arrays.h>

namespace arraymath {
void slidingArrayMedianMADFM(casacore::Array<float> &median,
                         casacore::Array<float> &madfm,
                         casacore::Array<float> &array,
                         const casacore::IPosition &halfBoxSize,
                         const unsigned int block_size);

void slidingArrayMedianMADFM(casacore::Array<float> &median,
                         casacore::Array<float> &madfm,
                         casacore::MaskedArray<float> &array,
                         const casacore::IPosition &halfBoxSize,
                         const unsigned int block_size);

// Implemented with either OpenMP, Cuda, HIP
void slidingMedianMadfmOptimized(float *median, float *rms, float *arr,
                            unsigned int arrdimx, unsigned int arrdimy,
                            unsigned int wdim, unsigned int block_dim);

} // namespace arraymath

#endif
