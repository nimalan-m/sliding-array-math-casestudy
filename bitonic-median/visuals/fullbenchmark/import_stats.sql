create table stat_cuda (threads,dim,time,sparseness);
create table stat_omp (threads,dim,time,sparseness);
create table stat_radeon (threads,dim,time,sparseness);
create table stat_rtx3060ti (threads,dim,time,sparseness);
create table stat_casa (threads,dim,time,sparseness);
create table stat_casa_milanx (threads,dim,time,sparseness);
create table stat_1650 (threads,dim,time,sparseness);
create table stat_a100 (threads,dim,time,sparseness);
create table window_radeon(dim, wsize, time, sparseness);
create table window_a100(dim, wsize, time, sparseness);

.mode csv

.import gondor_cuda_run.csv stat_cuda
.import gondor_casa_run.csv stat_casa
.import milanx-casa-run.csv stat_casa_milanx
.import gondor_omp_run.csv stat_omp
.import radeon_run.csv stat_radeon
.import rtx3060ti_run.csv stat_rtx3060ti
.import p1650_run.csv stat_1650
.import a100_stats.csv stat_a100
.import a100_window_2048.csv window_a100
.import radeon_window_2048.csv window_radeon
