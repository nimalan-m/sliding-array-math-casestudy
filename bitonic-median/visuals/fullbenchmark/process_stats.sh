#!/usr/bin/env bash

set -e
set -x

declare -a FILES=("p1650_run" "radeon_run" "gondor_cuda_run" "gondor_omp_run" "a100_stats" "milanx-casa-run" "rtx3060ti_run")
declare -a CASA_FILES=("gondor_casa_run")
declare -a WINDOW_FILES=("a100_window_2048" "radeon_window_2048")

# Process files
rm *_filtered.log
for file in "${FILES[@]}"
do
  ./filter_stats.sh $file
  ./parse.sh ${file}_filtered.log > $file.csv
done

for file in "${CASA_FILES[@]}"
do
  ./filter_stats_casa.sh $file
  ./parse_casa.sh ${file}_filtered.log > $file.csv
done

for file in "${WINDOW_FILES[@]}"
do
  ./filter_stats.sh $file
  ./parse_stats_window.sh ${file}_filtered.log > $file.csv
done

# Import stats, generate csv
rm stats.db
sqlite3 stats.db < import_stats.sql
m4 -DVALIDPIX=75 query_stat.sql.m4 | sqlite3 stats.db
m4 -DVALIDPIX=80 query_stat.sql.m4 | sqlite3 stats.db

source ./plot_graphs.sh



