.mode csv
.separator ", "

-- GPU
.output format(`stats/gpu_perf_%spc.csv', VALIDPIX)
select  p40.dim as matrix_size,
        p1650.time as p1650_time,
        p40.time as p40_time,
        radeon.time as radeon_time,
        a100.time as a100_time
  from stat_cuda as p40
  join stat_radeon as radeon on p40.dim = radeon.dim and p40.sparseness = radeon.sparseness
  join stat_1650 as p1650 on p40.dim = p1650.dim and p40.sparseness = p1650.sparseness
  join stat_a100 as a100 on p40.dim = a100.dim and p40.sparseness = a100.sparseness
  where p40.sparseness = "0.VALIDPIX";

-- All stat
.output format(`stats/perf_all_%spc.csv', VALIDPIX)
select  casa.dim as matrix_size,
        casa.time as casa_time,
        v_omp_1.time as opt_time,
        v_omp_2.time as th_2_time,
        v_omp_4.time as th_4_time,
        v_omp_8.time as th_8_time,
        p1650.time as p1650_time,
        rtx3060ti.time as rtx3060ti_time,
        p40.time as p40_time,
        radeon.time as radeon_time,
        a100.time as a100_time
  from stat_casa as casa
  join (select * from stat_omp as omp where omp.threads = "1") as v_omp_1 on casa.dim = v_omp_1.dim and casa.sparseness = v_omp_1.sparseness
  join (select * from stat_omp as omp where omp.threads = "2") as v_omp_2 on casa.dim = v_omp_2.dim and casa.sparseness = v_omp_2.sparseness
  join (select * from stat_omp as omp where omp.threads = "4") as v_omp_4 on casa.dim = v_omp_4.dim and casa.sparseness = v_omp_4.sparseness
  join (select * from stat_omp as omp where omp.threads = "8") as v_omp_8 on casa.dim = v_omp_8.dim and casa.sparseness = v_omp_8.sparseness
  join (select * from stat_omp as omp where omp.threads = "12") as v_omp_12 on casa.dim = v_omp_12.dim and casa.sparseness = v_omp_12.sparseness
  join (select * from stat_omp as omp where omp.threads = "16") as v_omp_16 on casa.dim = v_omp_16.dim and casa.sparseness = v_omp_16.sparseness
  join (select * from stat_omp as omp where omp.threads = "24") as v_omp_24 on casa.dim = v_omp_24.dim and casa.sparseness = v_omp_24.sparseness
  join (select * from stat_omp as omp where omp.threads = "32") as v_omp_32 on casa.dim = v_omp_32.dim and casa.sparseness = v_omp_32.sparseness
  join (select * from stat_omp as omp where omp.threads = "48") as v_omp_48 on casa.dim = v_omp_48.dim and casa.sparseness = v_omp_48.sparseness
  join (select * from stat_omp as omp where omp.threads = "64") as v_omp_64 on casa.dim = v_omp_64.dim and casa.sparseness = v_omp_64.sparseness
  join stat_cuda as p40 on casa.dim = p40.dim and casa.sparseness = p40.sparseness
  join stat_radeon as radeon on casa.dim = radeon.dim and casa.sparseness = radeon.sparseness
  join stat_1650 as p1650 on casa.dim = p1650.dim and casa.sparseness = p1650.sparseness
  join stat_rtx3060ti as rtx3060ti on casa.dim = rtx3060ti.dim and casa.sparseness = rtx3060ti.sparseness
  join stat_a100 as a100 on casa.dim = a100.dim and casa.sparseness = a100.sparseness
  where casa.sparseness = "0.VALIDPIX";

-- Threads only
.output format(`stats/perf_threads_%spc.csv', VALIDPIX)
select  casa.dim as matrix_size,
        casa.time as casa_time,
        v_omp_1.time as opt_time,
        v_omp_2.time as th_2_time,
        v_omp_4.time as th_4_time,
        v_omp_8.time as th_8_time,
        v_omp_12.time as th_12_time,
        v_omp_16.time as th_16_time,
        v_omp_24.time as th_24_time,
        v_omp_32.time as th_32_time,
        v_omp_48.time as th_48_time,
        v_omp_64.time as th_64_time
  from stat_casa as casa
  join (select * from stat_omp as omp where omp.threads = "1") as v_omp_1 on casa.dim = v_omp_1.dim and casa.sparseness = v_omp_1.sparseness
  join (select * from stat_omp as omp where omp.threads = "2") as v_omp_2 on casa.dim = v_omp_2.dim and casa.sparseness = v_omp_2.sparseness
  join (select * from stat_omp as omp where omp.threads = "4") as v_omp_4 on casa.dim = v_omp_4.dim and casa.sparseness = v_omp_4.sparseness
  join (select * from stat_omp as omp where omp.threads = "8") as v_omp_8 on casa.dim = v_omp_8.dim and casa.sparseness = v_omp_8.sparseness
  join (select * from stat_omp as omp where omp.threads = "12") as v_omp_12 on casa.dim = v_omp_12.dim and casa.sparseness = v_omp_12.sparseness
  join (select * from stat_omp as omp where omp.threads = "16") as v_omp_16 on casa.dim = v_omp_16.dim and casa.sparseness = v_omp_16.sparseness
  join (select * from stat_omp as omp where omp.threads = "24") as v_omp_24 on casa.dim = v_omp_24.dim and casa.sparseness = v_omp_24.sparseness
  join (select * from stat_omp as omp where omp.threads = "32") as v_omp_32 on casa.dim = v_omp_32.dim and casa.sparseness = v_omp_32.sparseness
  join (select * from stat_omp as omp where omp.threads = "48") as v_omp_48 on casa.dim = v_omp_48.dim and casa.sparseness = v_omp_48.sparseness
  join (select * from stat_omp as omp where omp.threads = "64") as v_omp_64 on casa.dim = v_omp_64.dim and casa.sparseness = v_omp_64.sparseness
  where casa.sparseness = "0.VALIDPIX";

-- Thread scaling only
.output format(`stats/perf_threads_scaling_%spc.csv', VALIDPIX)
select zn1.threads, zn1.time, zn3.time from stat_omp zn1
  join stat_casa_milanx zn3 on zn1.threads = zn3.threads and zn1.dim = zn3.dim and zn1.sparseness = zn3.sparseness where zn1.sparseness="0.VALIDPIX" and zn1.dim="2048";

-- Combined thread scaling
.output format(`stats/perf_threads_scaling_combined_%spc.csv', VALIDPIX)
select dim, threads, time from stat_omp where sparseness="0.VALIDPIX";


--- Window Scaling
.output format(`stats/perf_window_scaling_%spc.csv', VALIDPIX)
select (2*radeon.wsize+1), radeon.time, a100.time from window_radeon radeon
  join window_a100 a100 on radeon.wsize = a100.wsize and radeon.dim = a100.dim
  where radeon.sparseness = "0.VALIDPIX";
