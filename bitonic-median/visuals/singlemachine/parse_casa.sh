#!/usr/bin/env bash

# print "threads,dim,time,sparseness";                        \
cat $1 | awk -FS=" " '                                          \
  BEGIN{                                                        \
    FS=" ";                                                     \
    sparseness = 1.0                                            \
  }                                                             \
  /Benchmark args passed/{                                      \
    dim=$5;                                                     \
  }                                                             \
  /--masked/ {                                                  \
    sparseness=$(NF-1);                                         \
  }                                                             \
  /CASA wall clock/ {                                           \
    split($NF, time, "ms");                                     \
    printf("%d,%d,%s,%.2f\n", th, dim, time[1], sparseness);    \
  }'
  