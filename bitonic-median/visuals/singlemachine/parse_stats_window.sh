#!/usr/bin/env bash

# print "threads,dim,time,sparseness";                      \
cat $1 | awk -FS=" " '                                        \
  BEGIN{                                                      \
    FS=" ";                                                   \
    sparseness = 1.0                                          \
  }                                                           \
  /Benchmark args passed/{                                    \
    dim=$5;                                                   \
  }                                                           \
  /--validpix/ {                                                \
    sparseness=$10;                                           \
  }                                                           \
  /--wsize/ {                                                \
    wsize=$12;                                           \
  }                                                           \
  /Rewrite version wall clock time/ {                         \
    split($NF, time, "ms");                                   \
    printf("%d,%d,%s,%.2f\n", dim, wsize, time[1], sparseness);  \
  }'
  