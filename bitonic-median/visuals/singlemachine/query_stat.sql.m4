.mode csv
.separator ", "

-- All stat
.output format(`stats/perf_all_%spc.csv', VALIDPIX)
select  casa.dim as matrix_size,
        casa.time as casa_time,
        v_omp_1.time as opt_time,
        v_omp_2.time as th_2_time,
        v_omp_4.time as th_4_time,
        v_omp_8.time as th_8_time,
        gpu.time as gpu_time
  from stat_baseline as casa
  join (select * from stat_omp as omp where omp.threads = "1") as v_omp_1 on casa.dim = v_omp_1.dim and casa.sparseness = v_omp_1.sparseness
  join (select * from stat_omp as omp where omp.threads = "2") as v_omp_2 on casa.dim = v_omp_2.dim and casa.sparseness = v_omp_2.sparseness
  join (select * from stat_omp as omp where omp.threads = "4") as v_omp_4 on casa.dim = v_omp_4.dim and casa.sparseness = v_omp_4.sparseness
  join (select * from stat_omp as omp where omp.threads = "8") as v_omp_8 on casa.dim = v_omp_8.dim and casa.sparseness = v_omp_8.sparseness
  join (select * from stat_omp as omp where omp.threads = "12") as v_omp_12 on casa.dim = v_omp_12.dim and casa.sparseness = v_omp_12.sparseness
  join (select * from stat_omp as omp where omp.threads = "16") as v_omp_16 on casa.dim = v_omp_16.dim and casa.sparseness = v_omp_16.sparseness
  join (select * from stat_omp as omp where omp.threads = "24") as v_omp_24 on casa.dim = v_omp_24.dim and casa.sparseness = v_omp_24.sparseness
  join (select * from stat_omp as omp where omp.threads = "32") as v_omp_32 on casa.dim = v_omp_32.dim and casa.sparseness = v_omp_32.sparseness
  join (select * from stat_omp as omp where omp.threads = "48") as v_omp_48 on casa.dim = v_omp_48.dim and casa.sparseness = v_omp_48.sparseness
  join (select * from stat_omp as omp where omp.threads = "64") as v_omp_64 on casa.dim = v_omp_64.dim and casa.sparseness = v_omp_64.sparseness
  join stat_gpu as gpu on casa.dim = gpu.dim and casa.sparseness = gpu.sparseness
  where casa.sparseness = "0.VALIDPIX";

-- Threads only
.output format(`stats/perf_threads_%spc.csv', VALIDPIX)
select  casa.dim as matrix_size,
        casa.time as casa_time,
        v_omp_1.time as opt_time,
        v_omp_2.time as th_2_time,
        v_omp_4.time as th_4_time,
        v_omp_8.time as th_8_time,
        v_omp_12.time as th_12_time,
        v_omp_16.time as th_16_time,
        v_omp_24.time as th_24_time,
        v_omp_32.time as th_32_time,
        v_omp_48.time as th_48_time,
        v_omp_64.time as th_64_time
  from stat_baseline as casa
  join (select * from stat_omp as omp where omp.threads = "1") as v_omp_1 on casa.dim = v_omp_1.dim and casa.sparseness = v_omp_1.sparseness
  join (select * from stat_omp as omp where omp.threads = "2") as v_omp_2 on casa.dim = v_omp_2.dim and casa.sparseness = v_omp_2.sparseness
  join (select * from stat_omp as omp where omp.threads = "4") as v_omp_4 on casa.dim = v_omp_4.dim and casa.sparseness = v_omp_4.sparseness
  join (select * from stat_omp as omp where omp.threads = "8") as v_omp_8 on casa.dim = v_omp_8.dim and casa.sparseness = v_omp_8.sparseness
  join (select * from stat_omp as omp where omp.threads = "12") as v_omp_12 on casa.dim = v_omp_12.dim and casa.sparseness = v_omp_12.sparseness
  join (select * from stat_omp as omp where omp.threads = "16") as v_omp_16 on casa.dim = v_omp_16.dim and casa.sparseness = v_omp_16.sparseness
  join (select * from stat_omp as omp where omp.threads = "24") as v_omp_24 on casa.dim = v_omp_24.dim and casa.sparseness = v_omp_24.sparseness
  join (select * from stat_omp as omp where omp.threads = "32") as v_omp_32 on casa.dim = v_omp_32.dim and casa.sparseness = v_omp_32.sparseness
  join (select * from stat_omp as omp where omp.threads = "48") as v_omp_48 on casa.dim = v_omp_48.dim and casa.sparseness = v_omp_48.sparseness
  join (select * from stat_omp as omp where omp.threads = "64") as v_omp_64 on casa.dim = v_omp_64.dim and casa.sparseness = v_omp_64.sparseness
  where casa.sparseness = "0.VALIDPIX";

-- Thread scaling only
.output format(`stats/perf_threads_scaling_%spc.csv', VALIDPIX)
select omp.threads, omp.time from stat_omp omp
  where omp.sparseness="0.VALIDPIX" and omp.dim="2048";


--- Window Scaling
.output format(`stats/perf_window_scaling_%spc.csv', VALIDPIX)
select (2*gpu.wsize+1), gpu.time from stat_window_gpu gpu
  where gpu.sparseness = "0.VALIDPIX";
