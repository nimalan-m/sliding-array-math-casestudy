#!/usr/bin/env bash

set -e
set -x

declare -a GRAPHS=("plot_threads_scaling" "plot_overall_comparison" "plot_window_scaling")

# Create graph
for plot in "${GRAPHS[@]}"
do
  gnuplot plots/${plot}.p
done
