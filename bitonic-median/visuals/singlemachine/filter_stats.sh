#!/usr/bin/env bash

cat $1.log | awk '                              \
  /Benchmark args passed/{print $0}             \
  /export OMP_NUM_THREADS/{print $0}            \
  /Rewrite version wall clock time/ {print $0}  \
  /CASA wall clock/ {print $0}                  \
' > $1_filtered.log
