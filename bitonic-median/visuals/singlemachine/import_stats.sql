create table stat_baseline (threads,dim,time,sparseness);
create table stat_omp (threads,dim,time,sparseness);
create table stat_gpu (threads,dim,time,sparseness);

create table stat_window_gpu(dim, wsize, time, sparseness);

create table stat_thread_scaling (threads,dim,time,sparseness);

.mode csv

.import baseline_run.csv stat_baseline
.import omp_run.csv stat_omp
.import gpu_run.csv stat_gpu

.import gpu_window_2048.csv stat_window_gpu

.import omp_thread_scaling_run.csv stat_thread_scaling
