#include "ArrayMathOpt.h"

#include <algorithm>
#include <casacore/casa/BasicMath/Math.h>
#include <cmath>
#include <limits>
#include <vector>

// Divide by the following correction factor to convert from MADFM to sigma
// (rms) estimator.
static const float correctionFactor = 0.6744888;

namespace arraymath {

inline void sliding_window(float &median, float &rms, float *arr, int blc[2],
                           int trc[2], int arrdimx, int arrdimy, int wdim) {
  const int sz = wdim * wdim;
  float *v = new float[sz];

  // Account for masking
  int tmpSz = 0;
  for (int y = blc[1]; y < trc[1]; y++) {
    for (int x = blc[0]; x < trc[0]; x++) {
      int idx = y * arrdimx + x;
      if (arr[idx] != std::numeric_limits<float>::infinity()) {
        v[tmpSz++] = arr[idx];
      }
    }
  }

  std::nth_element(v, v + tmpSz / 2, v + tmpSz);
  median = v[tmpSz / 2];

  if (!(tmpSz & 1)) {
    std::nth_element(v, v + tmpSz / 2 - 1, v + tmpSz);
    median += v[tmpSz / 2 - 1];
    median /= 2;
  }

  for (int i = 0; i < tmpSz; i++) {
    v[i] = std::fabs(v[i] - median);
  }

  std::nth_element(v, v + tmpSz / 2, v + tmpSz);
  float madfm = v[tmpSz / 2];

  if (!(tmpSz & 1)) {
    std::nth_element(v, v + tmpSz / 2 - 1, v + tmpSz);
    madfm += v[tmpSz / 2 - 1];
    madfm /= 2;
  }

  rms = madfm / correctionFactor;

  delete[] v;
}

void slidingMedianMadfmOptimized(float *medianArr, float *rmsArr, float *arr,
                            unsigned int arrdimx, unsigned int arrdimy,
                            unsigned int bxdim, unsigned int block_dim) {

  int wdim = bxdim + 1;
  int resdimx = arrdimx - wdim + 1;
  int resdimy = arrdimy - wdim + 1;

#pragma omp parallel for
  for (int y = 0; y < resdimy; y++) {
    for (int x = 0; x < resdimx; x++) {
      int pos[2];
      int blc[2];
      int trc[2];

      pos[0] = x;
      pos[1] = y;

      blc[0] = pos[0];
      blc[1] = pos[1];

      trc[0] = blc[0] + wdim;
      trc[1] = blc[1] + wdim;

      float median;
      float rms;
      sliding_window(median, rms, arr, blc, trc, arrdimx, arrdimy, wdim);

      medianArr[y * resdimx + x] = median;
      rmsArr[y * resdimx + x] = rms;
    }
  }
}

} // namespace arraymath
