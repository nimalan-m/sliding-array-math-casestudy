#pragma once

#include <cooperative_groups.h>

#ifndef __HIP_PLATFORM_HCC__
#include "compute_helpers.cuh"
#else
#include "compute_helpers.cch"
#endif

namespace cg = cooperative_groups;

#ifdef __HIP_PLATFORM_HCC__
#define SHFL_DOWN(val, offset) __shfl_down(val, offset)
#else
#define SHFL_DOWN(val, offset) __shfl_down_sync(0xffffffff, val, offset)
#endif

// Divide by the following correction factor to convert from MADFM to sigma
// (rms) estimator.
static const float correctionFactor = 0.6744888;

template <typename T> __inline__ __device__ T wrap_reduce(T val) {
  for (uint i = warpSize/2; i >= 1; i >>= 1)
    val += SHFL_DOWN(val, i);

  return val;
}

template <typename T> __inline__ __device__ T block_reduce(T val) {
  static __shared__ T shared[128];

  uint tid = threadIdx.x;
  uint wid = tid % warpSize;    // tid % 32
  uint tblockid = tid / warpSize; // tid / 32

  val = wrap_reduce(val);

  // Store reduction result of warp
  if (wid == 0) {
    shared[tblockid] = val;
  }

  __syncthreads();

  // Read only from existing warps
  // Note: this will not work if number of warps is less than 1
  val = (threadIdx.x < blockDim.x / warpSize) ? shared[wid] : 0;

  val = wrap_reduce(val);

  return val;
}

__global__ void load_2d_kernel(float *in, float *out, params_t *params,
                               uint batchX, uint batchY) {

  uint dimx = params->arrdimx;
  uint dimy = params->arrdimy;
  uint block_dim = params->block_dim;
  uint wdim = params->wdim;
  uint real_wdim = params->real_wdim;
  uint real_wsize = params->real_wsize;
  uint resdimx = params->resdimx;
  uint resdimy = params->resdimy;

  uint batchOffsetX = batchX * block_dim;
  uint batchOffsetY = batchY * block_dim;

  __shared__ float shmem[SHARED_MEM_WINDOW + 1][SHARED_MEM_WINDOW + 1];

  out += (blockIdx.y * block_dim + blockIdx.x) * real_wsize;

  uint x = threadIdx.x;
  uint y = threadIdx.y;

  uint ax = blockIdx.x + batchOffsetX;
  uint ay = blockIdx.y + batchOffsetY;

  for (uint i = 0; i < real_wdim; i += SHARED_MEM_WINDOW) {
    for (uint j = 0; j < real_wdim; j += BLOCK_ROWS) {
      uint oidx = (y + j) * real_wdim + (x + i);
      uint iidx = (ay + y + j) * dimx + (ax + x + i);

      if ((x + i) >= wdim || (y + j) >= wdim || (x + ax + i) >= dimx ||
          (y + ay + j) >= dimy || ay >= resdimy || ax >= resdimx) {
        shmem[threadIdx.y][threadIdx.x] = CUDART_INF_F;
      } else {
        shmem[threadIdx.y][threadIdx.x] = in[iidx];
      }

      out[oidx] = shmem[threadIdx.y][threadIdx.x];
    }
  }
}

__global__ void compute_wsizes(float *in, uint *out, params_t *params) {
  uint real_wsize = params->real_wsize;
  uint block_dim = params->block_dim;

  uint offset = (blockIdx.y * block_dim + blockIdx.x);
  in += offset * real_wsize;

  uint size = 0;
  for (int stride = 0; stride < real_wsize; stride += SHARED_MEMORY_SIZE) {
    size += in[threadIdx.x + stride] != CUDART_INF_F;
    size += in[threadIdx.x + stride + (SHARED_MEMORY_SIZE / 2)] != CUDART_INF_F;
  }
  size = block_reduce<int>(size);

  if (threadIdx.x == 0) {
    out[offset] = size;
  }
}

__global__ void extract_median_transform(float *in, float *out, uint *sz,
                                         params_t *params, uint batchX,
                                         uint batchY, bool calculateRMS) {
  uint real_wsize = params->real_wsize;
  uint block_dim = params->block_dim;
  uint resdimx = params->resdimx;
  uint resdimy = params->resdimy;

  uint batchOffsetX = batchX * block_dim;
  uint batchOffsetY = batchY * block_dim;
  uint wsize = params->wsize;

  uint ay = blockIdx.y + batchOffsetY;
  uint ax = blockIdx.x + batchOffsetX;

  // Do not transform rows of infinity
  if (ay < resdimy && ax < resdimx) {

    uint offset = (blockIdx.y * block_dim + blockIdx.x);
    in += (offset * real_wsize);

    __shared__ float shmem[SHARED_MEMORY_SIZE];

    float value = 0.0f;
    if (sz[offset] > 0U) {
      // uint mid = wsize / 2;
      uint mid = sz[offset] / 2;
      value = in[mid];
      value += (sz[offset] & 1) ? in[mid] : in[mid - 1];
      value /= 2;
      value = calculateRMS ? value / correctionFactor : value;
    }

    in += threadIdx.x;

    for (int stride = 0; stride < real_wsize; stride += SHARED_MEMORY_SIZE) {
      shmem[threadIdx.x] = in[stride];
      shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)] =
          in[stride + (SHARED_MEMORY_SIZE / 2)];

      in[stride] = fabs(value - shmem[threadIdx.x]);
      in[stride + (SHARED_MEMORY_SIZE / 2)] =
          fabs(value - shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)]);
    }

    if (threadIdx.x == 0) {
      out[ay * resdimx + ax] = value;
    }
  }
}
