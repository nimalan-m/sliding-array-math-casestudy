#include <assert.h>
#include <casacore/casa/Arrays.h>
#include <casacore/casa/Arrays/IPosition.h>

#include <cmath>

#include "ArrayMathOpt.h"

namespace casa = casacore;

// TODO: This is redundant
inline unsigned int highest_power_2(unsigned int n) {
  unsigned int t = n - 1;
  t |= t >> 1;
  t |= t >> 2;
  t |= t >> 4;
  t |= t >> 8;
  t |= t >> 16;
  return t + 1;
}

namespace arraymath {

inline void calculateSlidingMedianMadfm(casacore::Array<float> &median,
                        casacore::Array<float> &madfm,
                        casacore::Array<float> &array,
                        const casa::IPosition &halfBoxSize,
                        const unsigned int block_size) {

  casacore::IPosition hboxsz(2 * halfBoxSize);
  if (hboxsz.size() != array.ndim()) {
    size_t sz = hboxsz.size();
    hboxsz.resize(array.ndim());
    for (size_t i = sz; i < hboxsz.size(); ++i) {
      hboxsz[i] = 0;
    }
  }
  unsigned int wdim = hboxsz[0];
  unsigned int real_wdim = highest_power_2(wdim);

  // Assert present of contiguousStorage
  assert(median.contiguousStorage());
  assert(madfm.contiguousStorage());
  assert(array.contiguousStorage());

  const casacore::IPosition &shape = array.shape();

  size_t ndim = array.ndim();
  casacore::IPosition resShape(ndim);
  for (size_t i = 0; i < ndim; ++i) {
    resShape[i] = shape[i] - hboxsz[i];
    if (resShape[i] <= 0) {
      casa::Array<float> fullMedianRes(shape);
      casa::Array<float> fullMadfmRes(shape);
      fullMedianRes = float();
      fullMadfmRes = float();

      median = std::move(fullMedianRes);
      madfm = std::move(fullMadfmRes);
      return;
    }
  }

  casacore::Array<float> medianRes(resShape);
  casacore::Array<float> madfmRes(resShape);

  float *medianData = medianRes.data();
  float *madfmData = madfmRes.data();
  float *arrData = array.data();

  slidingMedianMadfmOptimized(medianData, madfmData, arrData, shape[0], shape[1],
                         wdim, block_size);

  casacore::Array<float> fullMedianRes(shape);
  casacore::Array<float> fullMadfmRes(shape);

  fullMedianRes = float();
  fullMadfmRes = float();
  hboxsz /= 2;
  fullMedianRes(hboxsz, resShape + hboxsz - 1) = medianRes;
  fullMadfmRes(hboxsz, resShape + hboxsz - 1) = madfmRes;

  median = std::move(fullMedianRes);
  madfm = std::move(fullMadfmRes);
}

inline void assert_array(const casacore::Array<float> &array,
                         const casa::IPosition &halfBoxSize) {
  casa::IPosition mat_shape = array.shape();

  assert(mat_shape.size() >= 2);

  // Assert that we are working on a continuum image, any higher dimesion has to
  // be 1
  if (mat_shape.size() > 2) {
    for (size_t i = 2; i < mat_shape.size(); i++) {
      assert(mat_shape[i] == 1);
    }
  }

  // Assert that we have a 2D square box
  assert(halfBoxSize.size() == 2);
  assert(halfBoxSize[0] == halfBoxSize[1]);

  assert(mat_shape[1] - halfBoxSize[1] > 0);
  assert(mat_shape[0] - halfBoxSize[0] > 0);
}

// The fillEdge done by CASA has been removed
void slidingArrayMedianMADFM(casacore::Array<float> &median,
                         casacore::Array<float> &madfm,
                         casacore::Array<float> &array,
                         const casa::IPosition &halfBoxSize,
                         const unsigned int block_size) {
  assert_array(array, halfBoxSize);
  calculateSlidingMedianMadfm(median, madfm, array, halfBoxSize, block_size);
}

void slidingArrayMedianMADFM(casacore::Array<float> &median,
                         casacore::Array<float> &madfm,
                         casacore::MaskedArray<float> &masked_array,
                         const casacore::IPosition &halfBoxSize,
                         const unsigned int block_size) {

  assert_array(masked_array.getArray(), halfBoxSize);

  // Need to make shallow copy because operator() is non-const.

  casa::Array<float> array = masked_array.getArray().copy();
  casa::LogicalArray mask = masked_array.getMask();

  casa::IPosition mat_shape = array.shape();

  float *arrData = array.data();
  bool *maskData = mask.data();

  for (int j = 0; j < mat_shape[1]; j++) {
    for (int i = 0; i < mat_shape[0]; i++) {
      if (!maskData[j * mat_shape[0] + i]) {
        arrData[j * mat_shape[0] + i] = std::numeric_limits<float>::infinity();
      }
      if (std::isnan(arrData[j * mat_shape[0] + i])) {
        arrData[j * mat_shape[0] + i] = std::numeric_limits<float>::infinity();
      }
    }
  }

  calculateSlidingMedianMadfm(median, madfm, array, halfBoxSize, block_size);
}

} // namespace arraymath
