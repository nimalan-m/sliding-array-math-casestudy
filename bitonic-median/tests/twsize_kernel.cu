#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/generate.h>
#include <thrust/host_vector.h>
#include <thrust/random.h>

#include "compute_helpers.cuh"
#include "transform_kernels.cuh"

#include "ttest_helpers.cuh"

#include <limits>
#include <stdio.h>

int main(int argc, char **argv) {

  uint arrdimx = 32;
  uint arrdimy = 32;
  uint wdim = 16;
  uint block_dim = 32;
  if (argc == 5) {
    arrdimx = atoi(argv[1]);
    arrdimy = atoi(argv[2]);
    wdim = atoi(argv[3]);
    block_dim = atoi(argv[4]);
  } else {
    fprintf(stderr,
            "Usage: twsize_kernel <arrdimx> <arrdimx> <wdim> <block_dim> \n"
            "Example: twsize_kernel 128 128 50 32\n");
    return 1;
  }

  BitonicSlidingMedian cont(arrdimx, arrdimy, wdim, block_dim);

  checkCudaErrors(cudaMalloc(&cont.dmem.params_d, sizeof(params_t)));
  checkCudaErrors(cudaMemcpyAsync(cont.dmem.params_d, &cont.params_h,
                                  sizeof(params_t), cudaMemcpyHostToDevice, 0));

  thrust::default_random_engine rng(1337);
  thrust::uniform_real_distribution<double> dist(0.0, 50.0);

  thrust::host_vector<float> windows(cont.params_h.buffer_size);
  thrust::generate(windows.begin(), windows.end(), [&] { return dist(rng); });

  for (int i = 0; i < windows.size(); i++) {
    if (windows[i] > 45.0) {
      windows[i] = std::numeric_limits<float>::infinity();
    }
  }

  thrust::host_vector<uint> exp_wsizes(cont.params_h.block_size);
  for (int window = 0; window < cont.params_h.block_size; window++) {
    int count = 0;
    for (int pos = 0; pos < cont.params_h.real_wsize; pos++) {
      count += windows[window * cont.params_h.real_wsize + pos] !=
                       std::numeric_limits<float>::infinity()
                   ? 1
                   : 0;
    }
    exp_wsizes[window] = count;
  }

  thrust::device_vector<float> windows_d(cont.params_h.buffer_size);

  thrust::host_vector<uint> wsizes(cont.params_h.block_size, 0);
  thrust::device_vector<uint> wsizes_d(cont.params_h.block_size, 0);

  windows_d = windows;

  dim3 blocks2d(cont.params_h.block_dim, cont.params_h.block_dim);
  uint threadCount = SHMEM_HALF;

  compute_wsizes<<<blocks2d, threadCount, 0, cont.stream>>>(
      thrust::raw_pointer_cast(windows_d.data()),
      thrust::raw_pointer_cast(wsizes_d.data()), cont.dmem.params_d);

  wsizes = wsizes_d;

  assertArray(exp_wsizes, wsizes);
  printf("Assertion passed\n");

  // printArray(wsizes, cont.params_h.block_dim);
}
