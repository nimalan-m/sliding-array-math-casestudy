#pragma once

#include <thrust/host_vector.h>

#define EPHISILON 0.001f

void printArray(const thrust::host_vector<float> &exp_windows, int wsize) {
  for (uint i = 0; i < exp_windows.size(); i++) {
    printf("%.1f ", exp_windows[i]);
    if ((i + 1) % wsize == 0) {
      printf("\n");
    }
  }
}

void assertArray(const thrust::host_vector<uint> &expected,
                 const thrust::host_vector<uint> &actual) {
  for (int i = 0; i < expected.size(); i++) {
    if (expected[i] != actual[i]) {
      fprintf(stderr,
              "Assertion error for window: %d - expected: %u actual: %u\n", i,
              expected[i], actual[i]);
      assert(expected[i] == actual[i]);
    }
  }
}

void assertArray(const thrust::host_vector<float> &expected,
                 const thrust::host_vector<float> &actual) {
  for (int i = 0; i < expected.size(); i++) {
    if (fabs(expected[i] - actual[i]) > EPHISILON) {
      fprintf(stderr,
              "Assertion error for window: %d - expected: %.1f actual: %.1f\n",
              i, expected[i], actual[i]);
      assert(fabs(expected[i] - actual[i]) < EPHISILON);
    }
  }
}

void assertArray(const thrust::host_vector<float> &expected,
                 const thrust::host_vector<float> &actual, int batchsize,
                 int real_wsize) {
  for (int window = 0; window < batchsize; window++) {
    for (int pos = 0; pos < real_wsize; pos++) {
      int idx = window * real_wsize + pos;
      if (fabs(expected[idx] - actual[idx]) > EPHISILON) {
        fprintf(
            stderr,
            "Assertion error for window: %d pos: %d - expected: %.1f actual: %.1f\n", window, pos,
            expected[idx], actual[idx]);
        assert(fabs(expected[idx] - actual[idx]) < EPHISILON);
      }
    }
  }
}

void assertWindows(const thrust::host_vector<float> &exp_windows,
                   const thrust::host_vector<float> &windows, int batchsize,
                   int real_wsize) {
  for (int window = 0; window < batchsize; window++) {
    for (int pos = 0; pos < real_wsize; pos++) {
      int idx = window * real_wsize + pos;

      if (idx > windows.size()) {
        fprintf(stderr,
                "Assertion failure out of bounds at window: %d pos: %d array "
                "size: %lu \n",
                window, pos, windows.size());
        assert(idx < windows.size());
      }

      if (abs(exp_windows[idx] - windows[idx]) > EPHISILON) {
        fprintf(stderr,
                "Assertion failure at window: %d pos: %d "
                " exp: %f actual: %f\n",
                window, pos, exp_windows[idx], windows[idx]);
        assert(abs(exp_windows[idx] - windows[idx]) < EPHISILON);
      }
    }
  }
}
