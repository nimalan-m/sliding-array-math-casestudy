#include "compute_helpers.cuh"
#include "utest.h"

UTEST(devicemem, assert_generated_values) {
  BitonicSlidingMedian cont(32, 32, 16, 32);

  ASSERT_EQ(1024U, cont.params_h.arr_size);

  ASSERT_EQ(32U, cont.params_h.real_wdim);
  ASSERT_EQ(1024U, cont.params_h.real_wsize);
  ASSERT_EQ(16U, cont.params_h.max_resdim);
  ASSERT_EQ(16U, cont.params_h.real_resdim);

  ASSERT_EQ((16U * 16U), cont.params_h.no_windows);
  ASSERT_EQ(1U, cont.params_h.k);

  ASSERT_EQ(262144U, cont.params_h.buffer_size);

  ASSERT_EQ(16U, cont.params_h.block_dim);
  ASSERT_EQ(1U, cont.params_h.batchdim);
}

UTEST(devicemem, assert_odd_window_sizes) {
  BitonicSlidingMedian cont(32, 32, 19, 32);

  ASSERT_EQ(1024U, cont.params_h.arr_size);

  ASSERT_EQ(32U, cont.params_h.real_wdim);
  ASSERT_EQ(1024U, cont.params_h.real_wsize);
  ASSERT_EQ(13U, cont.params_h.max_resdim);
  ASSERT_EQ(16U, cont.params_h.real_resdim);

  ASSERT_EQ(169U, cont.params_h.no_windows);

  ASSERT_EQ(262144U, cont.params_h.buffer_size);

  ASSERT_EQ(16U, cont.params_h.block_dim);
  ASSERT_EQ(1U, cont.params_h.batchdim);
}

UTEST(devicemem, assert_uneven_window_sizes) {
  BitonicSlidingMedian cont(50, 31, 29, 32);

  ASSERT_EQ(1550U, cont.params_h.arr_size);

  ASSERT_EQ(32U, cont.params_h.real_wdim);
  ASSERT_EQ(1024U, cont.params_h.real_wsize);
  ASSERT_EQ(21U, cont.params_h.max_resdim);
  ASSERT_EQ(21U, cont.params_h.resdimx);
  ASSERT_EQ(2U, cont.params_h.resdimy);
  ASSERT_EQ(32U, cont.params_h.real_resdim);

  ASSERT_EQ(1U, cont.params_h.k);

  ASSERT_EQ(42U, cont.params_h.no_windows);

  ASSERT_EQ(1048576U, cont.params_h.buffer_size);

  ASSERT_EQ(32U, cont.params_h.block_dim);
  ASSERT_EQ(1U, cont.params_h.batchdim);
}

UTEST(devicemem, assert_large_k) {
  BitonicSlidingMedian cont(50, 62, 50, 32);

  ASSERT_EQ(4U, cont.params_h.k);
}

UTEST_MAIN();
