#include <limits>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/generate.h>

#include "compute_helpers.cuh"
#include "transform_kernels.cuh"

#include "ttest_helpers.cuh"

void generateExpected(const thrust::host_vector<float> &arr,
                      thrust::host_vector<float> &exp, uint batchX, uint batchY,
                      params_t &params) {
  uint dimx = params.arrdimx;
  uint dimy = params.arrdimy;
  uint block_dim = params.block_dim;
  uint wdim = params.wdim;
  uint real_wdim = params.real_wdim;
  uint real_wsize = params.real_wsize;
  uint resdimx = params.resdimx;
  uint resdimy = params.resdimy;

  for (uint bidxy = 0; bidxy < params.block_dim; bidxy++) {
    for (uint bidxx = 0; bidxx < params.block_dim; bidxx++) {
      uint batchOffsetX = batchX * block_dim;
      uint batchOffsetY = batchY * block_dim;

      uint ax = bidxx + batchOffsetX;
      uint ay = bidxy + batchOffsetY;

      uint offset = (bidxy * block_dim + bidxx) * real_wsize;

      for (uint i = 0; i < real_wdim; i++) {
        for (uint j = 0; j < real_wdim; j++) {
          uint oidx = j * real_wdim + i;
          uint iidx = (ay + j) * dimx + (ax + i);

          if (i >= wdim || j >= wdim || (ax + i) >= dimx || (ay + j) >= dimy ||
              ay >= resdimy || ax >= resdimx) {
            exp[offset + oidx] = std::numeric_limits<float>::infinity();
          } else {
            exp[offset + oidx] = arr[iidx];
          }
        }
      }
    }
  }
}

int randInt() {
  int range = 5.0;
  return rand() / (double)(RAND_MAX / range);
}

int main(int argc, char **argv) {
  uint arrdimx = 128;
  uint arrdimy = 128;
  uint wdim = 50;
  uint block_dim = 32;
  if (argc == 5) {
    arrdimx = atoi(argv[1]);
    arrdimy = atoi(argv[2]);
    wdim = atoi(argv[3]);
    block_dim = atoi(argv[4]);
  } else {
    fprintf(stderr,
            "Usage: tload_kernel <arrdimx> <arrdimx> <wdim> <block_dim> \n"
            "Example: tload_kernel 128 128 50 32\n");
    return 1;
  }

  BitonicSlidingMedian cont(arrdimx, arrdimy, wdim, block_dim);

  checkCudaErrors(cudaMalloc(&cont.dmem.params_d, sizeof(params_t)));
  checkCudaErrors(cudaMemcpyAsync(cont.dmem.params_d, &cont.params_h,
                                  sizeof(params_t), cudaMemcpyHostToDevice, 0));

  thrust::host_vector<float> mat(cont.params_h.arr_size);
  thrust::generate(mat.begin(), mat.end(), randInt);

  thrust::host_vector<float> windows(cont.params_h.buffer_size);

  thrust::device_vector<float> mat_d(cont.params_h.arr_size);
  thrust::device_vector<float> windows_d(cont.params_h.buffer_size);

  thrust::host_vector<float> exp_windows(cont.params_h.buffer_size);

  mat_d = mat;

  dim3 blocks2d(cont.params_h.block_dim, cont.params_h.block_dim);
  dim3 threads2d(SHARED_MEM_WINDOW, BLOCK_ROWS);

  float *mat_ptr = thrust::raw_pointer_cast(mat_d.data());
  float *windows_ptr = thrust::raw_pointer_cast(windows_d.data());

  // printArray(mat, cont.params_h.arrdimx);
  printf("Windows: %d real_wsize: %d\n", cont.params_h.no_windows, cont.params_h.real_wsize);

  // cont.params_h.batchdim = 1;
  for (uint batchY = 0; batchY < cont.params_h.batchdim; batchY++) {
    for (uint batchX = 0; batchX < cont.params_h.batchdim; batchX++) {
      load_2d_kernel<<<blocks2d, threads2d, 0, cont.stream>>>(
          mat_ptr, windows_ptr, cont.dmem.params_d, batchX, batchY);

      generateExpected(mat, exp_windows, batchX, batchY, cont.params_h);

      // printf("Batch exp: \n");
      // printArray(exp_windows, cont.params_h.real_wsize);
      // cudaDeviceSynchronize();

      windows = windows_d;

      cudaDeviceSynchronize();
      // printf("Actual: \n");
      // printArray(windows, cont.params_h.real_wsize);

      assertWindows(exp_windows, windows, cont.params_h.block_size,
                    cont.params_h.real_wsize);
    }
  }

  checkCudaErrors(cudaFree(cont.dmem.params_d));
  printf("Assertion passed\n");
}