#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/generate.h>
#include <thrust/host_vector.h>
#include <thrust/random.h>

#include "compute_helpers.cuh"
#include "sort_kernels.cuh"

#include "ttest_helpers.cuh"

int main(int argc, char **argv) {
  uint arrdimx = 32;
  uint arrdimy = 32;
  uint wdim = 16;
  uint block_dim = 32;
  if (argc == 5) {
    arrdimx = atoi(argv[1]);
    arrdimy = atoi(argv[2]);
    wdim = atoi(argv[3]);
    block_dim = atoi(argv[4]);
  } else {
    fprintf(stderr,
            "Usage: twsize_kernel <arrdimx> <arrdimx> <wdim> <block_dim> \n"
            "Example: twsize_kernel 128 128 50 32\n");
    return 1;
  }
  int dir = 1;
  uint threadCount = SHMEM_HALF;

  BitonicSlidingMedian cont(arrdimx, arrdimy, wdim, block_dim);

  thrust::default_random_engine rng(1337);
  thrust::uniform_real_distribution<double> dist(0.0, 50.0);

  thrust::host_vector<float> windows(cont.params_h.buffer_size);
  thrust::device_vector<float> windows_d(cont.params_h.buffer_size);
  thrust::generate(windows.begin(), windows.end(), [&] { return dist(rng); });
  for (int i = 0; i < windows.size(); i++) {
    if (windows[i] > 45.0) {
      windows[i] = std::numeric_limits<float>::infinity();
    }
  }
  windows_d = windows;

  thrust::host_vector<float> exp_windows(windows);

  thrust::host_vector<uint> wsizes(cont.params_h.block_size);
  for (int window = 0; window < cont.params_h.block_size; window++) {
    int count = 0;
    for (int pos = 0; pos < cont.params_h.real_wsize; pos++) {
      count += windows[window * cont.params_h.real_wsize + pos] !=
                       std::numeric_limits<float>::infinity()
                   ? 1
                   : 0;
    }
    wsizes[window] = count;
  }
  thrust::device_vector<uint> wsizes_d(cont.params_h.block_size);
  // thrust::fill(wsizes.begin(), wsizes.end(), cont.params_h.wsize);
  wsizes_d = wsizes;

  uint sortBlockCount = cont.params_h.buffer_size / SHARED_MEMORY_SIZE;

  cont.dmem.windows_d = thrust::raw_pointer_cast(windows_d.data());
  cont.dmem.wsizes_d = thrust::raw_pointer_cast(wsizes_d.data());
  checkCudaErrors(cudaMalloc(&cont.dmem.params_d, sizeof(params_t)));
  checkCudaErrors(cudaMemcpy(cont.dmem.params_d, &cont.params_h,
                                  sizeof(params_t), cudaMemcpyHostToDevice));

  bitonic_sort(&cont.dmem, &cont.params_h, &cont.stream, sortBlockCount, threadCount, dir);

  windows = windows_d;

  for (int window = 0; window < cont.params_h.block_size; window++) {
    thrust::sort(exp_windows.begin() + (window * cont.params_h.real_wsize),
                 exp_windows.begin() + (window * cont.params_h.real_wsize) +
                     cont.params_h.real_wsize);
  }

  // printArray(windows, cont.params_h.real_wsize);
  // printArray(exp_windows, cont.params_h.real_wsize);

  assertArray(exp_windows, windows, cont.params_h.block_size,
              cont.params_h.real_wsize);

  checkCudaErrors(cudaFree(cont.dmem.params_d));

  printf("Assertion passed\n");
}
