#include <algorithm>
#include <cooperative_groups.h>
#include <string>

#ifndef __HIP_PLATFORM_HCC__
#include "compute_helpers.cuh"
#include "sort_kernels.cuh"
#include "transform_kernels.cuh"
#else
#include "compute_helpers.cch"
#include "sort_kernels.cch"
#include "transform_kernels.cch"
#endif

#include "ArrayMathOpt.h"

namespace arraymath {

void slidingMedianMadfmOptimized(float *median, float *rms, float *arr,
                            unsigned int arrdimx, unsigned int arrdimy,
                            unsigned int bxdim, unsigned int block_dim) {

  // Sort ascending
  uint dir = 1;

  BitonicSlidingMedian cont(arrdimx, arrdimy, bxdim, block_dim);

  cont.allocDeviceMemory();

  // Transfer
  cont.transferHostToDevice(arr);

#ifdef PRINT_INFO
  cudaEvent_t start, stop;
  checkCudaErrors(cudaEventCreate(&start));
  checkCudaErrors(cudaEventCreate(&stop));

  checkCudaErrors(cudaEventRecord(start, cont.stream));
#endif

  // Scaling: In latest generation GPUs we can have a higher SHARED_MEM_WINDOW
  // size but the primary bottleneck of this algorithm is not the load kernel
  // and transform kernel, the sort kernel is the critical step
  dim3 blocks2d(cont.params_h.block_dim, cont.params_h.block_dim);
  dim3 threads2d(SHARED_MEM_WINDOW, BLOCK_ROWS);

  // Scaling: Our scaling is dependent on these two variables
  uint threadCount = SHMEM_HALF;
  uint sortBlockCount = cont.params_h.buffer_size / SHARED_MEMORY_SIZE;

#ifdef PRINT_INFO
  printf("block2d: %d %d threads2d: %d %d\n", blocks2d.x, blocks2d.y,
         threads2d.x, threads2d.y);
  printf("shmem: %d shmem_w: %d\n", SHARED_MEMORY_SIZE, SHARED_MEM_WINDOW);
#endif

  // cont.params_h.batchdim = 1;
  for (uint batchY = 0; batchY < cont.params_h.batchdim; batchY++) {
    for (uint batchX = 0; batchX < cont.params_h.batchdim; batchX++) {

      // Load kernel
      load_2d_kernel<<<blocks2d, threads2d, 0, cont.stream>>>(
          cont.dmem.arr_d, cont.dmem.windows_d, cont.dmem.params_d, batchX,
          batchY);

      compute_wsizes<<<blocks2d, threadCount, 0, cont.stream>>>(
          cont.dmem.windows_d, cont.dmem.wsizes_d, cont.dmem.params_d);

      // Sort for median
      bitonic_sort(&cont.dmem, &cont.params_h, &cont.stream, sortBlockCount,
                   threadCount, dir);

      extract_median_transform<<<blocks2d, threadCount, 0, cont.stream>>>(
          cont.dmem.windows_d, cont.dmem.median_d, cont.dmem.wsizes_d,
          cont.dmem.params_d, batchX, batchY, false);

      // Sort for madfm
      bitonic_merge(&cont.dmem, &cont.params_h, &cont.stream,
                        sortBlockCount, threadCount, dir);

      // Extract rms
      extract_median_transform<<<blocks2d, threadCount, 0, cont.stream>>>(
          cont.dmem.windows_d, cont.dmem.madfm_d, cont.dmem.wsizes_d,
          cont.dmem.params_d, batchX, batchY, true);
    }
  }

  cont.transferDeviceToHost(median, rms);

  cudaStreamSynchronize(cont.stream);

#ifdef PRINT_INFO
  checkCudaErrors(cudaEventRecord(stop, cont.stream));
  checkCudaErrors(cudaEventSynchronize(stop));

  float elapsedTime;
  checkCudaErrors(cudaEventElapsedTime(&elapsedTime, start, stop));

  uint batches = cont.params_h.batchdim * cont.params_h.batchdim;
  printf(
      "Elasped GPU time %f ms Throughput: %f MElem/s\n", elapsedTime,
      (cont.params_h.no_windows * cont.params_h.buffer_size * batches * 1e-6) /
          (elapsedTime * 1e-3));
  checkCudaErrors(cudaEventDestroy(start));
  checkCudaErrors(cudaEventDestroy(stop));
#endif

  cont.deallocDeviceMemory();
}

} // namespace arraymath
