#pragma once

#include <cassert>
#include <stdio.h>
#include <string>

#include "config.h"

#ifndef SHARED_MEM_WINDOW
#define SHARED_MEM_WINDOW 32U
#endif

#ifndef SHARED_MEMORY_SIZE
#define SHARED_MEMORY_SIZE 2048U
#endif

#define SHMEM_HALF (SHARED_MEMORY_SIZE >> 1)
#define BLOCK_ROWS (SHARED_MEM_WINDOW >> 2)

#define CUDART_INF_F __int_as_float(0x7f800000U)

#define UMUL(a, b) __umul24((a), (b))
#define UMAD(a, b, c) (UMUL((a), (b)) + (c))

inline void checkGPU(cudaError_t result, std::string file, int const line) {
  if (result != cudaSuccess) {
    fprintf(stderr, "Cuda Runtime Error at %s:%d : %s\n", file.c_str(), line,
            cudaGetErrorString(result));
    exit(EXIT_FAILURE);
  }
}

#define checkCudaErrors(val) checkGPU(val, __FILE__, __LINE__)

inline uint highest_power_2(uint n) {
  uint t = n - 1;
  t |= t >> 1;
  t |= t >> 2;
  t |= t >> 4;
  t |= t >> 8;
  t |= t >> 16;
  return t + 1;
}

typedef struct params_t {
  uint arrdimx;
  uint arrdimy;
  uint arr_size;

  uint wdim;
  uint wsize;

  uint real_wdim;
  uint real_wsize;
  uint k;

  uint resdimx;
  uint resdimy;
  uint max_resdim;
  uint real_resdim;
  uint no_windows; // ressize

  uint block_dim;
  uint block_size;

  uint batchdim;

  uint buffer_size;
} params_t;

typedef struct {
  float *arr_d;
  float *median_d;
  float *madfm_d;
  uint *wsizes_d;
  float *windows_d;
  params_t *params_d;
} devicemem_t;

class BitonicSlidingMedian {
public:
  params_t params_h;
  devicemem_t dmem;
  cudaStream_t stream;

#ifdef PRINT_INFO
  void printInfo() {
    printf("arr dim: %d x %d wdim: %d rwdim: %d res dim: %d x %d max_resdim: "
           "%d rresdim: %d\n",
           params_h.arrdimx, params_h.arrdimy, params_h.wdim,
           params_h.real_wdim, params_h.resdimx, params_h.resdimy,
           params_h.max_resdim, params_h.real_resdim);
    printf("no windows %d \n", params_h.no_windows);
    printf("block dim: %d elem in batch %d bufferSize: %d\n",
           params_h.block_dim, params_h.block_size, params_h.buffer_size);
    printf("batch dim: %d \n", params_h.batchdim);
  }
#endif

  BitonicSlidingMedian(uint arrdimx, uint arrdimy, uint bxdim, uint block_dim) {
    // Full box adjustment
    uint wdim = bxdim + 1;

    // Window adjustment for sort
    uint real_wdim = highest_power_2(wdim);
    uint real_wsize = real_wdim * real_wdim;
    uint k = real_wsize / SHARED_MEMORY_SIZE;

    // Matrix adjustment for non 2^N
    uint max_arrdim = std::max(arrdimx, arrdimy);
    uint resdimx = arrdimx - wdim + 1;
    uint resdimy = arrdimy - wdim + 1;
    uint max_resdim = max_arrdim - wdim + 1;
    uint real_resdim = highest_power_2(max_resdim);

    block_dim = std::min(block_dim, real_resdim);

    uint no_windows = resdimx * resdimy;
    uint block_size = block_dim * block_dim;
    uint batchdim = real_resdim / block_dim;
    uint buffer_size = block_size * real_wsize;

    if (real_wdim < SHARED_MEM_WINDOW) {
      fprintf(stderr,
              "Window size is too low to use the GPU implementation, minimum "
              "size is %d\n",
              SHARED_MEM_WINDOW >> 1);
      assert(real_wdim >= SHARED_MEM_WINDOW);
    }
    // assert(block_dim <= real_resdim);
    // assert(real_wdim % block_dim == 0);

    params_h = {.arrdimx = arrdimx,
                .arrdimy = arrdimy,
                .arr_size = arrdimx * arrdimy,
                .wdim = wdim,
                .wsize = wdim * wdim,
                .real_wdim = real_wdim,
                .real_wsize = real_wsize,
                .k = k,
                .resdimx = resdimx,
                .resdimy = resdimy,
                .max_resdim = max_resdim,
                .real_resdim = real_resdim,
                .no_windows = no_windows,
                .block_dim = block_dim,
                .block_size = block_size,
                .batchdim = batchdim,
                .buffer_size = buffer_size};

#ifdef PRINT_INFO
    printInfo();
#endif

    // Create stream
    checkCudaErrors(cudaStreamCreate(&stream));
  }

  ~BitonicSlidingMedian() {
    checkCudaErrors(cudaStreamDestroy(stream));
  }

  void allocDeviceMemory() {
    checkCudaErrors(cudaMalloc(&dmem.arr_d, params_h.arr_size * sizeof(float)));
    checkCudaErrors(
        cudaMalloc(&dmem.median_d, params_h.no_windows * sizeof(float)));
    checkCudaErrors(
        cudaMalloc(&dmem.wsizes_d, params_h.block_size * sizeof(uint)));
    checkCudaErrors(
        cudaMalloc(&dmem.madfm_d, params_h.no_windows * sizeof(float)));
    checkCudaErrors(
        cudaMalloc(&dmem.windows_d, params_h.buffer_size * sizeof(float)));
    checkCudaErrors(cudaMalloc(&dmem.params_d, sizeof(params_t)));
  }

  void deallocDeviceMemory() {
    checkCudaErrors(cudaFree(dmem.arr_d));
    checkCudaErrors(cudaFree(dmem.median_d));
    checkCudaErrors(cudaFree(dmem.madfm_d));
    checkCudaErrors(cudaFree(dmem.wsizes_d));
    checkCudaErrors(cudaFree(dmem.windows_d));
  }

  void transferHostToDevice(float *arr) {
    checkCudaErrors(cudaMemcpyAsync(dmem.arr_d, arr,
                                    params_h.arr_size * sizeof(float),
                                    cudaMemcpyHostToDevice, stream));

    checkCudaErrors(cudaMemcpyAsync(dmem.params_d, &params_h, sizeof(params_t),
                                    cudaMemcpyHostToDevice, stream));
  }

  void transferDeviceToHost(float *median, float *madfm) {
    checkCudaErrors(cudaMemcpyAsync(median, dmem.median_d,
                                    params_h.no_windows * sizeof(float),
                                    cudaMemcpyDeviceToHost, stream));

    checkCudaErrors(cudaMemcpyAsync(madfm, dmem.madfm_d,
                                    params_h.no_windows * sizeof(float),
                                    cudaMemcpyDeviceToHost, stream));
  }
};
