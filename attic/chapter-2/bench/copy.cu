#include <assert.h>
#include <cooperative_groups.h>
#include <stdio.h>
#include <stdlib.h>

namespace cg = cooperative_groups;

#define SHARED_SIZE_LIMIT 1024U
#define WARP 512U
#define WINDOW_SIZE SHARED_SIZE_LIMIT

inline cudaError_t checkCudaErrors(cudaError_t result) {
  if (result != cudaSuccess) {
    fprintf(stderr, "Cuda Runtime Error : %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
  return result;
}

__global__ void copy(float *out, float *in, uint size) {
  uint tid = blockIdx.x * blockDim.x + threadIdx.x;
  uint stride = blockDim.x * gridDim.x;

  for (; tid < size; tid += stride) {
    // printf("%d\n", tid);
    out[tid] = in[tid];
  }
}

__global__ void copy_unroll(float *out, float *in, uint size) {
  uint tid = blockIdx.x * blockDim.x + threadIdx.x;
  uint stride = blockDim.x * gridDim.x;

  #pragma unroll 4
  for (; tid < size; tid += stride) {
    // printf("%d\n", tid);
    out[tid] = in[tid];
  }
}

__global__ void copy2(float *out, float *in, uint size) {
  uint tid = blockIdx.x * blockDim.x + threadIdx.x;
  uint stride = blockDim.x * gridDim.x;

  for (; tid < size/2; tid += stride) {
    // printf("%d\n", tid);
    reinterpret_cast<float2*>(out)[tid] = reinterpret_cast<float2*>(in)[tid];
  }
}

__global__ void copy4(float *out, float *in, uint size) {
  uint tid = blockIdx.x * blockDim.x + threadIdx.x;
  uint stride = blockDim.x * gridDim.x;

  for (; tid < size/4; tid += stride) {
    // printf("%d\n", tid);
    reinterpret_cast<float4*>(out)[tid] = reinterpret_cast<float4*>(in)[tid];
  }
}

__global__ void copy4_unroll(float *out, float *in, uint size) {
  uint tid = blockIdx.x * blockDim.x + threadIdx.x;
  uint stride = blockDim.x * gridDim.x;

  #pragma unroll 4
  for (; tid < size/4; tid += stride) {
    // printf("%d\n", tid);
    reinterpret_cast<float4*>(out)[tid] = reinterpret_cast<float4*>(in)[tid];
  }
}

void driver(uint size, int blocks, int threads,
            void (*kernel)(float *, float *, uint)) {

  float *in;
  float *out;
  cudaMallocManaged(&in, sizeof(float) * size);
  cudaMallocManaged(&out, sizeof(float) * size);

  for (uint i = 0; i < size; i++) {
    in[i] = (size - i) * 1.0;
  }

  cudaEvent_t start, stop;

  checkCudaErrors(cudaEventCreate(&start));
  checkCudaErrors(cudaEventCreate(&stop));

  checkCudaErrors(cudaEventRecord(start));

  kernel<<<blocks, threads>>>(out, in, size);

  cudaDeviceSynchronize();

#ifdef DEBUG
  for (int i = 0; i < size; i++) {
    printf("%f ", in[i]);
  }
  printf("\n");
#endif

  checkCudaErrors(cudaEventRecord(stop));
  checkCudaErrors(cudaEventSynchronize(stop));

  float elapsedTime;
  checkCudaErrors(cudaEventElapsedTime(&elapsedTime, start, stop));
  printf("Elapsed GPU time %f ms\n", elapsedTime);

#ifdef ASSERT
  for (int i = 0; i < size; i++) {
    if (out[i] != in[i]) {
      printf("Assertion failed at pos: %d\n", i);
      assert(out[i] == in[i]);
    }
  }
#endif

#ifdef DEBUG
  for (int i = 0; i < size; i++) {
    printf("%f ", out[i]);
  }
  printf("\n");
#endif

  checkCudaErrors(cudaEventDestroy(start));
  checkCudaErrors(cudaEventDestroy(stop));

  cudaFree(out);
  cudaFree(in);
}

int main(int argc, char **argv) {
  int N;
  int blocks;
  int threads;

  if (argc < 4) {
    N = 1024;
    blocks = 256;
    threads = 256;
  } else {
    uint pow = atoi(argv[1]);
    N = 1 << pow;
    blocks = atoi(argv[2]);
    threads = atoi(argv[3]);
  }

  printf("Testing copy for %d elements\n", N);

  printf("Scalar copy\n");
  driver(N, blocks, threads, copy);

  printf("Scalar copy unroll\n");
  driver(N, blocks, threads, copy_unroll);

  printf("Vector float2 copy\n");
  driver(N, blocks, threads, copy2);

  printf("Vector float4 copy\n");
  driver(N, blocks, threads, copy4);

  printf("Vector float4 copy unroll\n");
  driver(N, blocks, threads, copy4_unroll);
}
