#include <assert.h>
#include <cooperative_groups.h>
#include <stdio.h>
#include <stdlib.h>

namespace cg = cooperative_groups;

#define SHARED_SIZE_LIMIT 1024U
#define WARP 512U
#define WINDOW_SIZE SHARED_SIZE_LIMIT

inline cudaError_t checkCudaErrors(cudaError_t result) {
  if (result != cudaSuccess) {
    fprintf(stderr, "Cuda Runtime Error : %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
  return result;
}

__global__ void populateWindows(float *arr, float *windows, uint noOfWindows) {
  uint tid = blockIdx.x * blockDim.x + threadIdx.x;
  uint stride = blockDim.x * gridDim.x;

  for (; tid < noOfWindows; tid += stride) {
    for (uint i = 0; i < WINDOW_SIZE; i++) {
      windows[tid * WINDOW_SIZE + i] = arr[tid + i];
    }
  }
}

/*
  This kernel does not work. Given that the nature of global access is
  unaligned, we cannot use vectors here reinterpret_cast<float2*>(arr+1)[tid +
  i]; does not work as it is unaligned illegal memory access
*/
__global__ void populateWindowsVector(float *arr, float *windows,
                                      uint noOfWindows) {
  uint tid = blockIdx.x * blockDim.x + threadIdx.x;
  uint stride = blockDim.x * gridDim.x;

  for (; tid < noOfWindows / 2; tid += stride) {
    int idx = 0;

    // Window 1
    for (uint i = 0; i < WINDOW_SIZE / 2; i++) {
      reinterpret_cast<float2 *>(windows)[tid * WINDOW_SIZE + idx] =
          reinterpret_cast<float2 *>(arr)[tid + i];
      idx++;
    }

    float *arr2 = reinterpret_cast<float *>(arr) + 1;

    // Only lower half of the kernel is populated
    // Window 2
    for (uint i = 0; i < WINDOW_SIZE / 2; i++) {
      windows[tid * WINDOW_SIZE + idx] = arr2[tid + i];
      idx++;
    }
  }
}

__global__ void populateWindowsCollase(float *arr, float *windows,
                                       uint noOfWindows) {
  windows += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;

  windows[0] = arr[blockIdx.x + threadIdx.x + 0];
  windows[WARP] = arr[blockIdx.x + threadIdx.x + WARP];
}

__global__ void populateWindowsCollaseShmem(float *arr, float *windows,
                                            uint noOfWindows) {
  windows += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;

  __shared__ float shmem[SHARED_SIZE_LIMIT];

  shmem[threadIdx.x] = arr[blockIdx.x + threadIdx.x + 0];
  shmem[threadIdx.x + WARP] = arr[blockIdx.x + threadIdx.x + WARP];

  windows[0] = shmem[threadIdx.x];
  windows[WARP] = shmem[threadIdx.x + WARP];
}

__global__ void populateWindowsCollaseShmemOptLoad(float *arr, float *windows,
                                                   uint noOfWindows) {
  cg::thread_block cta = cg::this_thread_block();
  windows += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;

  __shared__ float shmem[SHARED_SIZE_LIMIT];

  if (cta.thread_rank() == 0) {
    for (uint i = 0; i < SHARED_SIZE_LIMIT / 4; i++) {
      reinterpret_cast<float4 *>(shmem)[threadIdx.x] =
          reinterpret_cast<float4 *>(arr)[blockIdx.x + threadIdx.x];
    }
  }

  cg::sync(cta);

  windows[0] = shmem[threadIdx.x];
  windows[WARP] = shmem[threadIdx.x + WARP];
}

__global__ void populateWindowsCollaseShmemOptLoadParallel(float *arr,
                                                           float *windows,
                                                           uint noOfWindows) {
  cg::thread_block cta = cg::this_thread_block();
  windows += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;

  __shared__ float shmem[SHARED_SIZE_LIMIT];

  reinterpret_cast<float2 *>(shmem)[threadIdx.x] =
      reinterpret_cast<float2 *>(arr)[blockIdx.x + threadIdx.x];

  cg::sync(cta);

  windows[0] = shmem[threadIdx.x];
  windows[WARP] = shmem[threadIdx.x + WARP];
}

__global__ void populateWindowsCollaseVector2(float *arr, float *windows,
                                              uint noOfWindows) {
  windows += blockIdx.x * SHARED_SIZE_LIMIT;

  reinterpret_cast<float2 *>(windows)[threadIdx.x] =
      reinterpret_cast<float2 *>(arr)[blockIdx.x + threadIdx.x];
}

__global__ void populateWindowsCollaseShmemVector2(float *arr, float *windows,
                                                   uint noOfWindows) {
  windows += blockIdx.x * SHARED_SIZE_LIMIT;

  __shared__ float shmem[SHARED_SIZE_LIMIT];

  reinterpret_cast<float2 *>(shmem)[threadIdx.x] =
      reinterpret_cast<float2 *>(arr)[blockIdx.x + threadIdx.x];

  reinterpret_cast<float2 *>(windows)[threadIdx.x] =
      reinterpret_cast<float2 *>(shmem)[threadIdx.x];
}

__global__ void populateWindowsCollaseShmemOptVector2(float *arr,
                                                      float *windows,
                                                      uint noOfWindows) {
  cg::thread_block cta = cg::this_thread_block();

  windows += blockIdx.x * SHARED_SIZE_LIMIT;

  if (blockIdx.x % 2 == 0) {
    reinterpret_cast<float2 *>(windows)[threadIdx.x] =
        reinterpret_cast<float2 *>(arr)[blockIdx.x + threadIdx.x];
  } else {
    __shared__ float shmem_even[SHARED_SIZE_LIMIT];
    __shared__ float shmem_odd[SHARED_SIZE_LIMIT];

    reinterpret_cast<float2 *>(shmem_even)[threadIdx.x] =
        reinterpret_cast<float2 *>(arr)[blockIdx.x + threadIdx.x];
    reinterpret_cast<float2 *>(shmem_odd)[threadIdx.x] =
        reinterpret_cast<float2 *>(arr)[blockIdx.x + threadIdx.x];

    cg::sync(cta);

    if (cta.thread_rank() < cta.size() - 1) {
      shmem_odd[threadIdx.x] = shmem_even[threadIdx.x + 1];
      shmem_odd[threadIdx.x + WARP] = shmem_even[threadIdx.x + WARP + 1];
    } else {
      shmem_odd[threadIdx.x] = shmem_odd[threadIdx.x] =
          shmem_even[threadIdx.x + 1];
      shmem_odd[threadIdx.x + WARP] = shmem_odd[threadIdx.x + WARP];
    }

    reinterpret_cast<float2 *>(windows)[threadIdx.x] =
        reinterpret_cast<float2 *>(shmem_odd)[threadIdx.x];
  }
}

__global__ void populateWindowsShmem(float *arr, float *windows,
                                     uint noOfWindows) {
  cg::thread_block cta = cg::this_thread_block();

  uint tid = blockIdx.x * blockDim.x + threadIdx.x;
  uint stride = blockDim.x * gridDim.x;

  // uint aid = blockIdx.x * (WARP) + threadIdx.x;

  __shared__ float shmem[2 * SHARED_SIZE_LIMIT];

  shmem[threadIdx.x + 0] = arr[tid + 0];
  shmem[threadIdx.x + WARP] = arr[tid + WARP];
  shmem[threadIdx.x + 2 * WARP] = arr[tid + 2 * WARP];
  shmem[threadIdx.x + 3 * WARP] = arr[tid + 3 * WARP];

  cg::sync(cta);

  for (uint i = 0; i < WINDOW_SIZE; i++) {
    windows[tid * WINDOW_SIZE + i] = shmem[threadIdx.x + i];
  }
}

void driver(uint size, int blocks, int threads,
            void (*kernel)(float *, float *, uint)) {
  int noOfWindows = size - WINDOW_SIZE;
  int bufferSize = noOfWindows * SHARED_SIZE_LIMIT;

  float *arr;
  float *windows;
  cudaMallocManaged(&arr, sizeof(float) * size);
  cudaMallocManaged(&windows, sizeof(float) * bufferSize);

  for (uint i = 0; i < size; i++) {
    arr[i] = (size - i) * 1.0;
  }

  cudaEvent_t start, stop;

  checkCudaErrors(cudaEventCreate(&start));
  checkCudaErrors(cudaEventCreate(&stop));

  checkCudaErrors(cudaEventRecord(start));

  kernel<<<blocks, threads>>>(arr, windows, noOfWindows);

  cudaDeviceSynchronize();

#ifdef DEBUG
  for (int i = 0; i < bufferSize; i++) {
    printf("%f ", windows[i]);
    if ((i + 1) % WINDOW_SIZE == 0) {
      printf("\n");
    }
  }
  printf("\n");
#endif

  checkCudaErrors(cudaEventRecord(stop));
  checkCudaErrors(cudaEventSynchronize(stop));

  float elapsedTime;
  checkCudaErrors(cudaEventElapsedTime(&elapsedTime, start, stop));
  printf("Elapsed GPU time %f ms\n", elapsedTime);

#ifdef ASSERT
  int widx = 0;
  for (int window = 0; window < noOfWindows; window++) {
    for (uint idx = 0; idx < SHARED_SIZE_LIMIT; idx++) {
      if (windows[widx] != arr[window + idx]) {
        printf(
            "Assertion failed at pos: %d window: %d idx: %d exp: %f act: %f\n",
            widx, window, idx, arr[window + idx], windows[widx]);
        assert(windows[widx] == arr[window + idx]);
      }
      widx++;
    }
  }
  printf("Assertions passed\n");
#endif

  checkCudaErrors(cudaEventDestroy(start));
  checkCudaErrors(cudaEventDestroy(stop));

  cudaFree(windows);
  cudaFree(arr);
}

int main(int argc, char **argv) {
  int N;
  int blocks;
  int threads;

  if (argc < 4) {
    N = 1024;
    blocks = 256;
    threads = 256;
  } else {
    N = atoi(argv[1]);
    blocks = atoi(argv[2]);
    threads = atoi(argv[3]);
  }

  int size = N + SHARED_SIZE_LIMIT;
  uint bufferSize;
  uint noOfWindows;

  printf("Base implementation\n");
  driver(size, blocks, threads, populateWindows);

  // Kernel does not work
  // printf("Base implementation with float2 vector\n");
  // driver(size, blocks, threads, populateWindowsVector);
  // driver(size, 1, 1, populateWindowsVector);

    printf("Pure shared memory\n");
    threads = 512;
    noOfWindows = size - 1024;
    bufferSize = noOfWindows * SHARED_SIZE_LIMIT;
    blocks = bufferSize / (SHARED_SIZE_LIMIT * threads);
    printf("blocks: %d\n", blocks);
    driver(size, blocks, threads, populateWindowsShmem);

    printf("Collasce implementation 1\n");
    threads = 512;
    noOfWindows = size - 1024;
    bufferSize = noOfWindows * SHARED_SIZE_LIMIT;
    blocks = bufferSize / SHARED_SIZE_LIMIT;
    driver(size, blocks, threads, populateWindowsCollase);

    printf("Collasce with shmem implementation 1\n");
    threads = 512;
    noOfWindows = size - 1024;
    bufferSize = noOfWindows * SHARED_SIZE_LIMIT;
    blocks = bufferSize / SHARED_SIZE_LIMIT;
    driver(size, blocks, threads, populateWindowsCollaseShmem);

  // These don't pass assertions
  #ifndef ASSERT
    printf("Collasce with shmem implementation with vector\n");
    threads = 512;
    noOfWindows = size - 1024;
    bufferSize = noOfWindows * SHARED_SIZE_LIMIT;
    blocks = bufferSize / SHARED_SIZE_LIMIT;
    driver(size, blocks, threads, populateWindowsCollaseShmemOptLoad);

    printf("Collasce with shmem implementation with parallel vector load\n");
    threads = 512;
    noOfWindows = size - 1024;
    bufferSize = noOfWindows * SHARED_SIZE_LIMIT;
    blocks = bufferSize / SHARED_SIZE_LIMIT;
    driver(size, blocks, threads,
    populateWindowsCollaseShmemOptLoadParallel);

    printf("Collasce with vector all\n");
    threads = 512;
    noOfWindows = size - 1024;
    bufferSize = noOfWindows * SHARED_SIZE_LIMIT;
    blocks = bufferSize / SHARED_SIZE_LIMIT;
    driver(size, blocks, threads, populateWindowsCollaseVector2);

    printf("Collasce with shmem implementation vector all\n");
    threads = 512;
    noOfWindows = size - 1024;
    bufferSize = noOfWindows * SHARED_SIZE_LIMIT;
    blocks = bufferSize / SHARED_SIZE_LIMIT;
    driver(size, blocks, threads, populateWindowsCollaseShmemVector2);

    printf("Collasce with shmem implementation vector all opt write\n");
    threads = 512;
    noOfWindows = size - 1024;
    bufferSize = noOfWindows * SHARED_SIZE_LIMIT;
    blocks = bufferSize / SHARED_SIZE_LIMIT;
    driver(size, blocks, threads, populateWindowsCollaseShmemOptVector2);
  #endif
}
