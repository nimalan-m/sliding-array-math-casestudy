#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#define SHARED_SIZE_LIMIT 1024u
#define WARP 512u

inline cudaError_t checkCudaErrors(cudaError_t result) {
  if (result != cudaSuccess) {
    fprintf(stderr, "Cuda Runtime Error : %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
  return result;
}

__global__ void extract(float *in, float *out, int batchSize) {
  uint tid = blockIdx.x * blockDim.x + threadIdx.x;
  uint stride = blockDim.x * gridDim.x;
  uint mid = batchSize / 2;

  in += tid * batchSize;

  float median = (in[mid] + in[mid - 1]) / 2;
  out[tid] = median;
}

// __global__ extract_coalesed() {

// }

// __global__ void transform(float *in, float *out, int batchSize) {
//   __shared__ float shmem[SHARED_SIZE_LIMIT];

//   shmem[threadIdx.x] = shmem[threadIdx.x + WARP] = 
// }

void driver(uint blocks, uint size, uint batch, uint threads,
            void kernel(float *, float *, int)) {
  float *in;
  float *out;
  float *d_out;

  out = (float *)malloc(sizeof(float) * blocks);

  checkCudaErrors(cudaMallocManaged(&in, sizeof(float) * size));
  checkCudaErrors(cudaMalloc(&d_out, sizeof(float) * blocks));

  checkCudaErrors(
      cudaMemcpy(d_out, out, sizeof(float) * blocks, cudaMemcpyHostToDevice));

  uint val = 0;
  for (uint bl = 0; bl < blocks; bl++) {
    for (uint idx = 0; idx < batch; idx++) {
      in[bl * batch + idx] = (val++) * 1.0;
    }
  }

#ifdef ASSERT
  float expected[blocks];
  val = 0;
  for (uint bl = 0; bl < blocks; bl++) {

    float mid = val + batch / 2;
    expected[bl] = (mid + mid - 1) / 2;
    val += batch;
  }
#endif

#ifdef DEBUG

  for (int i = 0; i < blocks; i++) {
    printf("%f ", out[i]);
  }
  printf("\n");

  for (uint bl = 0; bl < blocks; bl++) {
    for (uint idx = 0; idx < batch; idx++) {
      printf("%f ", in[bl * batch + idx]);
    }
    printf("\n");
  }
  printf("\n");

#endif

  uint noOfBlocks = blocks / threads;

  cudaEvent_t start, stop;

  checkCudaErrors(cudaEventCreate(&start));
  checkCudaErrors(cudaEventCreate(&stop));

  checkCudaErrors(cudaEventRecord(start));

  kernel<<<noOfBlocks, threads>>>(in, d_out, batch);

  checkCudaErrors(cudaEventRecord(stop));
  checkCudaErrors(cudaEventSynchronize(stop));

  float elapsedTime;
  checkCudaErrors(cudaEventElapsedTime(&elapsedTime, start, stop));
  printf("Elapsed GPU time %f ms\n", elapsedTime);

  checkCudaErrors(cudaEventDestroy(start));
  checkCudaErrors(cudaEventDestroy(stop));

#ifdef DEBUG
  for (int i = 0; i < blocks; i++) {
    printf("%f ", out[i]);
  }
  printf("\n");
#endif

#ifdef ASSERT
  cudaMemcpy(out, d_out, sizeof(float) * blocks, cudaMemcpyDeviceToHost);
  for (uint bl = 0; bl < blocks; bl++) {
    if (out[bl] != expected[bl]) {
      printf("Assertion Error median of block %d is incorrect expected: %f "
             "actual: %f\n",
             bl, expected[bl], out[bl]);
      assert(out[bl] == expected[bl]);
    }
  }
#endif

  cudaFree(in);
  cudaFree(d_out);
  free(out);
}

int main(int argc, char **argv) {

  uint batch;
  uint blocks;
  uint threads;

  if (argc < 4) {
    batch = 32;
    blocks = 10;
    threads = 1;
  } else {
    batch = atoi(argv[1]);
    blocks = atoi(argv[2]);
    threads = atoi(argv[3]);
  }

  blocks = 1 << blocks;

  int size = blocks * batch;

  printf("Uncoalesed extract\n");
  driver(blocks, size, batch, threads, extract);

  // printf("Coalesed extract\n");
  // driver(blocks, size, batch, threads, extract_coalesed);
}
