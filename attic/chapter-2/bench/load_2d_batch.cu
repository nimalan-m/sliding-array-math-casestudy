#include <assert.h>
#include <cooperative_groups.h>
#include <stdio.h>
#include <stdlib.h>

namespace cg = cooperative_groups;

#define WINDOW_SIZE 32U
#define BLOCK_ROWS (WINDOW_SIZE / 4)
#define TOTAL_WINDOW_SIZE (WINDOW_SIZE * WINDOW_SIZE)
#define WARP (TOTAL_WINDOW_SIZE / 2)

// Making a consious decision of not using a 2D kernel

inline cudaError_t checkCudaErrors(cudaError_t result) {
  if (result != cudaSuccess) {
    fprintf(stderr, "Cuda Runtime Error : %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
  return result;
}

__global__ void load_2d_kernel(float *windows, float *in, uint dim, uint wdim,
                               uint batchX, uint batchY, uint batchSize) {

  uint batchOffsetX = batchX * batchSize;
  uint batchOffsetY = batchY * batchSize;
  uint bx = blockIdx.x;
  uint by = blockIdx.y;
  int offset = (by * batchSize + bx) * TOTAL_WINDOW_SIZE;

  windows += offset;

  int x = threadIdx.x;
  int y = threadIdx.y;

  int ax = (bx) + batchOffsetX + threadIdx.x;
  int ay = (by) + batchOffsetY + threadIdx.y;

  for (uint j = 0; j < WINDOW_SIZE; j += BLOCK_ROWS) {
    windows[(y + j) * WINDOW_SIZE + x] = in[(ay + j) * dim + ax];
  }
}

void driver(uint dim, uint size, dim3 batchDim, dim3 blocks, dim3 threads,
            void (*kernel)(float *, float *, uint, uint, uint, uint, uint)) {
  uint wdim = dim - WINDOW_SIZE;
  uint noOfWindows = wdim * wdim;
  uint batchSize = batchDim.x * batchDim.y;
  uint bufferSize = batchSize * TOTAL_WINDOW_SIZE;
  // uint bufferSize = batchSize * TOTAL_WINDOW_SIZE;

  printf("Dim: %u %u Size: %u\n", dim, dim, size);
  printf("Window dim: %u %u Size: %u\n", wdim, wdim, noOfWindows);
  printf("buffer elem: %d buffer size: %.2f kb\n", bufferSize,
         (4.0f * bufferSize) / 1024.0f);

  float *arr;
  float *windows;

  checkCudaErrors(cudaMallocManaged(&arr, sizeof(float) * size));
  checkCudaErrors(cudaMallocManaged(&windows, sizeof(float) * (bufferSize)));

  for (uint i = 0; i < size; i++) {
    arr[i] = i * 1.0;
  }

#ifdef DEBUG
  printf("Input\n");
  for (uint j = 0; j < dim; j++) {
    for (uint i = 0; i < dim; i++) {
      printf("%.1f ", arr[j * dim + i]);
    }
    printf("\n");
  }
  printf("\n");
#endif

  uint bDim = wdim / batchDim.x;
  uint noOfBatches = noOfWindows / batchSize;
  printf("BatchDim: %d %d %d\n", batchDim.x, batchDim.y, batchDim.z);
  printf("Threads: %d %d %d\n", threads.x, threads.y, threads.z);
  printf("BatchSize: %u Windows: %u\n", batchSize, noOfWindows);
  printf("Populating for %d batches\n", noOfBatches);

  cudaEvent_t start, stop;

  checkCudaErrors(cudaEventCreate(&start));
  checkCudaErrors(cudaEventCreate(&stop));

  checkCudaErrors(cudaEventRecord(start));

  for (int batch = 0; batch < noOfBatches; batch++) {
    uint batchX = batch % bDim;
    uint batchY = batch / bDim;
    printf("Batch: %d batchx %d batchy %d\n", batch, batchX, batchY);
    kernel<<<batchDim, threads>>>(windows, arr, dim, wdim, batchX, batchY,
                                  batchDim.x);
    // kernel<<<dim3(16, 16), threads>>>(windows, arr, dim, wdim, batchX,
    // batchY,
    //                               batchDim.x);

#ifdef DEBUG
    {
      cudaDeviceSynchronize();
      int idx = 0;
      int r = 1;
      printf("Batch: %d.\n", batch);
      for (uint win = 0; win < batchSize; win++) {
        for (uint i = 0; i < TOTAL_WINDOW_SIZE; i++) {
          printf("%.1f ", windows[idx++]);
          if ((i + 1) % WINDOW_SIZE == 0) {
            printf("\n");
          }
        }
        printf("\nRow%d.\n", r);
        r++;
      }
      printf("\n");
    }
#endif

#ifdef ASSERT
    printf("Assertions\n");
    cudaDeviceSynchronize();
    {
      int idx = 0;
      uint st = batch * (batchDim.x * batchDim.x);
      uint en = st + (batchDim.x * batchDim.x);
      for (uint win = st; win < en; win++) {
        for (uint j = 0; j < WINDOW_SIZE; j++) {
          for (uint i = 0; i < WINDOW_SIZE; i++) {
            uint ax = (win % batchDim.x);
            uint ay = (win / batchDim.x);
            // uint ax = win % wdim;
            // uint ay = win / wdim;
            uint aidx = (ay + j) * dim + (ax + i);
            // uint widx = (win * TOTAL_WINDOW_SIZE) + (j * WINDOW_SIZE + i);
            if (windows[idx] != arr[aidx]) {
              printf("Assertion failure at batch %d window %d ay: %d ax: %d j: "
                     "%d i: %d  "
                     "actual: %.1f expected %.1f arrloc: %d\n",
                     batch, win, ay, ax, j, i, windows[idx], arr[aidx], aidx);
            }
            idx++;
          }
        }
      }
    }
#endif
  }

  cudaDeviceSynchronize();

  checkCudaErrors(cudaEventRecord(stop));
  checkCudaErrors(cudaEventSynchronize(stop));

  float elapsedTime;
  checkCudaErrors(cudaEventElapsedTime(&elapsedTime, start, stop));
  printf("Elapsed GPU time %f ms\n", elapsedTime);

  checkCudaErrors(cudaEventDestroy(start));
  checkCudaErrors(cudaEventDestroy(stop));

  cudaFree(windows);
  cudaFree(arr);
}

int main(int argc, char **argv) {
  uint N;
  dim3 batchSize;

  if (argc < 3) {
    N = 32;
    batchSize = dim3(32, 32);

  } else {
    N = atoi(argv[1]);
    uint t = atoi(argv[2]);
    batchSize = dim3(t, t);
  }

  uint dim = N + WINDOW_SIZE;
  uint size = dim * dim;
  dim3 dimBlock;
  dim3 dimGrid;

  uint wdim = dim - WINDOW_SIZE;
  uint noOfWindows = wdim * wdim;
  dimGrid = dim3(wdim, wdim, 1);
  // dimGrid = dim3(2, 2, 1);
  dimBlock = dim3(WINDOW_SIZE, BLOCK_ROWS, 1);

  printf("2D Coalesce Approach 1\n");
  printf("Grid dim: %d %d %d Block Dim: %d %d %d\n", dimGrid.x, dimGrid.y,
         dimGrid.z, dimBlock.x, dimBlock.y, dimBlock.z);
  driver(dim, size, batchSize, dimGrid, dimBlock, load_2d_kernel);
  printf("\n\n");

  // printf("2D Coalesce Approach 1 with shmem\n");
  // printf("Grid dim: %d %d %d Block Dim: %d %d %d\n", dimGrid.x, dimGrid.y,
  //        dimGrid.z, dimBlock.x, dimBlock.y, dimBlock.z);
  // driver(dim, size, batchSize, dimGrid, dimBlock, load_2d_kernel_shmem);
  // printf("\n\n");
}
