#include <assert.h>
#include <stdio.h>
#include <cooperative_groups.h>

namespace cg = cooperative_groups;

#ifndef SHARED_SIZE_LIMIT
#define SHARED_SIZE_LIMIT 128u
#endif
#ifndef WARP
#define WARP 64u
#endif
#ifndef BLOCKS
#define BLOCKS 256
#endif

__global__ void access_1(float* in, float* out) {
  cg::thread_block cta = cg::this_thread_block();

  __shared__ float shmem[SHARED_SIZE_LIMIT];

  in += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;

  shmem[threadIdx.x] = in[threadIdx.x];
  shmem[threadIdx.x + WARP] = in[threadIdx.x + WARP];

  cg::sync(cta);

  out[blockIdx.x] = shmem[WARP-1] + shmem[WARP];
}

__global__ void access_2(float* in, float* out) {
  cg::thread_block cta = cg::this_thread_block();

  __shared__ float shmem[SHARED_SIZE_LIMIT];

  in += blockIdx.x * SHARED_SIZE_LIMIT;

  reinterpret_cast<float2 *>(shmem)[threadIdx.x] = reinterpret_cast<float2 *>(in)[threadIdx.x];

  cg::sync(cta);

  out[blockIdx.x] = shmem[WARP-1] + shmem[WARP];
}

__global__ void access_3(float* in, float* out) {
  cg::thread_block cta = cg::this_thread_block();

  __shared__ float shmem[SHARED_SIZE_LIMIT];

  in += blockIdx.x * SHARED_SIZE_LIMIT;

  reinterpret_cast<float4 *>(shmem)[threadIdx.x] = reinterpret_cast<float4 *>(in)[threadIdx.x];

  cg::sync(cta);

  out[blockIdx.x] = shmem[WARP-1] + shmem[WARP];
}

int main() {

  float *in;
  float *out_1;
  float *out_2;
  float *out_3;

  cudaMallocManaged(&in, sizeof(float) * BLOCKS * SHARED_SIZE_LIMIT);
  cudaMallocManaged(&out_1, sizeof(float) * BLOCKS);
  cudaMallocManaged(&out_2, sizeof(float) * BLOCKS);
  cudaMallocManaged(&out_3, sizeof(float) * BLOCKS);

  int idx = 0;
  for (int i = 0; i < BLOCKS; i++) {
    for (int j = 0; j < SHARED_SIZE_LIMIT; j++) {
      in[idx++] = i * 1.0f;
    }
  }
  for (int i = 0; i < BLOCKS; i++) {
    out_1[i] = 0.0f;
    out_2[i] = 0.0f;
    out_3[i] = 0.0f;
  }

  access_1<<<BLOCKS, WARP>>>(in, out_1);
  access_2<<<BLOCKS, WARP>>>(in, out_2);
  access_3<<<BLOCKS, WARP/2>>>(in, out_3);

  cudaDeviceSynchronize();

  for (int i = 0; i < BLOCKS; i++) {
    if (out_1[i] != out_2[i]) {
      printf("Assertion failed for vec2 at block %d exp: %f act: %f\n", i, out_1[i], out_2[i]);
    }
    if (out_1[i] != out_3[i]) {
      printf("Assertion failed for vec4 at block %d exp: %f act: %f\n", i, out_1[i], out_3[i]);
    }
  }

  cudaFree(in);
  cudaFree(out_1);
  cudaFree(out_2);
  cudaFree(out_3);
}
