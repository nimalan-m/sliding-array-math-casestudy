# Benchmark kernels to optimize data load

## 1. Baseline copy

`nvcc -o copy copy.cu -O3`

## 2. Load and Transform Data kernel

TLDR: L2 cache is now a bottle neck


`nvcc -o load_kernels load_kernels.cu -O3`

```
(base) e4r-workstation-nimalan :: array-math-case-study/chapter-2/bench ‹bitonic*› » ./load_kernels 32768 256 256
Base implementation
Elapsed GPU time 34.290688 ms
Pure shared memory
blocks: 64
Elapsed GPU time 28.360704 ms
Collasce implementation 1
Elapsed GPU time 10.163168 ms
Collasce with shmem implementation 1
Elapsed GPU time 6.890560 ms
Collasce with shmem implementation with vector
Elapsed GPU time 11.079680 ms
Collasce with shmem implementation with parallel vector load
Elapsed GPU time 7.896320 ms

(base) e4r-workstation-nimalan :: array-math-case-study/chapter-2/bench ‹bitonic*› » ./load_kernels 16384 256 256
Base implementation
Elapsed GPU time 18.693695 ms
Pure shared memory
blocks: 32
Elapsed GPU time 12.174880 ms
Collasce implementation 1
Elapsed GPU time 5.810592 ms
Collasce with shmem implementation 1
Elapsed GPU time 6.149600 ms
Collasce with shmem implementation with vector
Elapsed GPU time 5.404992 ms
Collasce with shmem implementation with parallel vector load
Elapsed GPU time 5.723968 ms
```

## 3. Shmem Exp

Experiment to create coalesce access to global memory

## 4. Bitonic Kernels