#include <assert.h>
#include <cooperative_groups.h>
#include <stdio.h>
#include <stdlib.h>

namespace cg = cooperative_groups;

#define WINDOW_SIZE 32U
#define BLOCK_ROWS (WINDOW_SIZE / 4)
#define TOTAL_WINDOW_SIZE (WINDOW_SIZE * WINDOW_SIZE)
#define WARP (TOTAL_WINDOW_SIZE / 2)

// Making a consious decision of not using a 2D kernel

inline cudaError_t checkCudaErrors(cudaError_t result) {
  if (result != cudaSuccess) {
    fprintf(stderr, "Cuda Runtime Error : %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
  return result;
}

// RIP Coalesce
__global__ void load_2d(float *windows, float *in, uint dim, uint wdim,
                        uint noOfWindows) {
  uint tid = blockIdx.x * blockDim.x + threadIdx.x;
  uint stride = blockDim.x * gridDim.x;

  windows += tid * TOTAL_WINDOW_SIZE;

  for (; tid < noOfWindows; tid += stride) {

    for (uint j = 0; j < WINDOW_SIZE; j++) {
      for (uint i = 0; i < WINDOW_SIZE; i++) {
        uint widx = (j * WINDOW_SIZE + i);

        uint xoff = tid % wdim;
        uint yoff = tid / wdim;

        uint inOffset = yoff * dim + xoff;

        uint idx = (j * dim + i);
        windows[widx] = in[inOffset + idx];
      }
    }
  }
}

__global__ void load_2d_kernel(float *windows, float *in, uint dim, uint wdim,
                               uint noOfWindows) {
  int offset = (blockIdx.y * wdim + blockIdx.x) * TOTAL_WINDOW_SIZE;

  windows += offset;

  int x = threadIdx.x;
  int y = threadIdx.y;

  int ax = blockIdx.x + threadIdx.x;
  int ay = blockIdx.y + threadIdx.y;

  for (uint j = 0; j < WINDOW_SIZE; j += BLOCK_ROWS) {
    windows[(y + j) * WINDOW_SIZE + x] = in[(ay + j) * dim + ax];
  }
}

__global__ void load_2d_kernel_shmem(float *windows, float *in, uint dim, uint wdim,
                               uint noOfWindows) {
  int offset = (blockIdx.y * wdim + blockIdx.x) * TOTAL_WINDOW_SIZE;

  __shared__ float tile[WINDOW_SIZE][WINDOW_SIZE];

  windows += offset;

  int x = threadIdx.x;
  int y = threadIdx.y;

  int ax = blockIdx.x + threadIdx.x;
  int ay = blockIdx.y + threadIdx.y;

  for (uint j = 0; j < WINDOW_SIZE; j += BLOCK_ROWS) {
    tile[y+j][x] = in[(ay + j) * dim + ax];
  }

  for (uint j = 0; j < WINDOW_SIZE; j += BLOCK_ROWS) {
    windows[(y + j) * WINDOW_SIZE + x] = tile[y+j][x];
  }
}

void driver(uint dim, uint size, dim3 blocks, dim3 threads,
            void (*kernel)(float *, float *, uint, uint, uint)) {
  uint wdim = dim - WINDOW_SIZE;
  uint noOfWindows = wdim * wdim;
  uint bufferSize = noOfWindows * TOTAL_WINDOW_SIZE;

  printf("Dim: %u %u Size: %u\n", dim, dim, size);
  printf("Window dim: %u %u Size: %u\n", wdim, wdim, noOfWindows);
  printf("buffer size: %.2f kb\n", (4.0f * bufferSize) / 1024.0f);

  float *arr;
  float *windows;

  checkCudaErrors(cudaMallocManaged(&arr, sizeof(float) * size));
  checkCudaErrors(cudaMallocManaged(&windows, sizeof(float) * bufferSize));

  for (uint i = 0; i < size; i++) {
    arr[i] = i * 1.0;
  }

#ifdef DEBUG
  printf("Input\n");
  for (uint j = 0; j < dim; j++) {
    for (uint i = 0; i < dim; i++) {
      printf("%.1f ", arr[j * dim + i]);
    }
    printf("\n");
  }
  printf("\n");
#endif

  cudaEvent_t start, stop;

  checkCudaErrors(cudaEventCreate(&start));
  checkCudaErrors(cudaEventCreate(&stop));

  checkCudaErrors(cudaEventRecord(start));

  kernel<<<blocks, threads>>>(windows, arr, dim, wdim, noOfWindows);

  cudaDeviceSynchronize();

#ifdef DEBUG
  printf("Output: \n");
  uint idx = 0;
  for (uint win = 0; win < noOfWindows; win++) {
    for (uint j = 0; j < WINDOW_SIZE; j++) {
      for (uint j = 0; j < WINDOW_SIZE; j++) {
        printf("%.1f ", windows[idx]);
        idx++;
      }
      // printf("\n");
    }
    printf("\n");
  }
  printf("\n");
#endif

  checkCudaErrors(cudaEventRecord(stop));
  checkCudaErrors(cudaEventSynchronize(stop));

  float elapsedTime;
  checkCudaErrors(cudaEventElapsedTime(&elapsedTime, start, stop));
  printf("Elapsed GPU time %f ms\n", elapsedTime);

  checkCudaErrors(cudaEventDestroy(start));
  checkCudaErrors(cudaEventDestroy(stop));

  cudaFree(windows);
  cudaFree(arr);
}

int main(int argc, char **argv) {
  uint N;

  if (argc < 2) {
    N = 32;
  } else {
    N = atoi(argv[1]);
  }

  uint dim = N + WINDOW_SIZE;
  uint size = dim * dim;
  dim3 dimBlock = dim3(512, 1, 1);

  // Iteration 1
  // Naive approach
  dim3 dimGrid = dim3(256);
  printf("Basic Approach\n");
  driver(dim, size, dimGrid, dimBlock, load_2d);
  printf("\n\n");

  uint wdim = dim - WINDOW_SIZE;
  uint noOfWindows = wdim * wdim;
  dimGrid = dim3(wdim, wdim, 1);
  // dimGrid = dim3(2, 2, 1);
  dimBlock = dim3(WINDOW_SIZE, BLOCK_ROWS, 1);

  printf("2D Coalesce Approach 1\n");
  printf("Grid dim: %d %d %d Block Dim: %d %d %d\n", dimGrid.x, dimGrid.y,
         dimGrid.z, dimBlock.x, dimBlock.y, dimBlock.z);
  driver(dim, size, dimGrid, dimBlock, load_2d_kernel);
  printf("\n\n");

  printf("2D Coalesce Approach 1 with shmem\n");
  printf("Grid dim: %d %d %d Block Dim: %d %d %d\n", dimGrid.x, dimGrid.y,
         dimGrid.z, dimBlock.x, dimBlock.y, dimBlock.z);
  driver(dim, size, dimGrid, dimBlock, load_2d_kernel_shmem);
  printf("\n\n");
}
