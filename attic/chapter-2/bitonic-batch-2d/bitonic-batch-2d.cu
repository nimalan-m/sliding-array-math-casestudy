#include <chrono>
#include <stdio.h>
#include <stdlib.h>

#include "bitonicSortBatch2D.cu"

using namespace std::chrono;

int main(int argc, char** argv) {
  uint N;
  uint batchSize;

  if (argc < 3) {
    N = 32;
    batchSize = 32;
  } else {
    N = atoi(argv[1]);
    batchSize = atoi(argv[2]);
  }

  N = N + WINDOW_SIZE;

  printf("%s Starting...\n\n", argv[0]);

  float *h_in, *median, *madfm;

  const float maxVal = 65536.0;
  
  uint arraySize = N * N;
  uint windows = N - WINDOW_SIZE;
  uint resSize = windows * windows;

  printf("Allocating and initializing host arrays...\n\n");
  h_in = (float *)malloc(arraySize * sizeof(float));
  median = (float *)malloc(resSize * sizeof(float));
  madfm = (float *)malloc(resSize * sizeof(float));

  srand(2001);

  for (uint i = 0; i < arraySize; i++) {
    // h_in[i] = (float)std::rand() / (float)(RAND_MAX / maxVal);
    h_in[i] = (arraySize - i) * 1.0f;
    // h_in[i] = i * 1.0f;
  }

  bitonicSlidingMedian(median, madfm, h_in, N, batchSize);

  free(median);
  free(madfm);
  free(h_in);
}
