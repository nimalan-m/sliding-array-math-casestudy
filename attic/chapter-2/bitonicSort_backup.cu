#pragma once

#include <assert.h>
#include <cooperative_groups.h>

namespace cg = cooperative_groups;

// This can be 2048
#define SHARED_SIZE_LIMIT 1024U
#define WINDOW_SIZE SHARED_SIZE_LIMIT

__device__ inline void Comparator(float &A, float &B, uint dir) {
  float t;

  if ((A > B) == dir) {
    t = A;
    A = B;
    B = t;
  }
}

// Bitonic sort kernel based on Cuda's example for arrays fitting into shared
// memory
__global__ void bitonicSortShared(float *out, float *in, uint arrayLength,
                                  uint dir) {
  // Handle to thread block group
  cg::thread_block cta = cg::this_thread_block();
  // Shared memory storage for one or more short vectors
  __shared__ float shmem[SHARED_SIZE_LIMIT];

  // Offset to the beginning of subbatch and load data
  in += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;
  out += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;

  shmem[threadIdx.x + 0] = in[0];
  shmem[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = in[(SHARED_SIZE_LIMIT / 2)];

  for (uint size = 2; size < arrayLength; size <<= 1) {
    // Bitonic merge
    uint ddd = dir ^ ((threadIdx.x & (size / 2)) != 0);

    for (uint stride = size / 2; stride > 0; stride >>= 1) {
      cg::sync(cta);
      uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
      Comparator(shmem[pos + 0], shmem[pos + stride], ddd);
    }
  }

  // ddd == dir for the last bitonic merge step
  {
    for (uint stride = arrayLength / 2; stride > 0; stride >>= 1) {
      cg::sync(cta);
      uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
      Comparator(shmem[pos + 0], shmem[pos + stride], dir);
    }
  }

  cg::sync(cta);
  out[0] = shmem[threadIdx.x + 0];
  out[(SHARED_SIZE_LIMIT / 2)] = shmem[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];
}

__global__ void bitonicSortKernel(float *in, uint arrayLength, uint dir) {
  // Handle to thread block group
  cg::thread_block cta = cg::this_thread_block();
  // Shared memory storage for one or more short vectors
  __shared__ float shmem[SHARED_SIZE_LIMIT];

  // Offset to the beginning of subbatch and load data
  in += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;

  shmem[threadIdx.x + 0] = in[0];
  shmem[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = in[(SHARED_SIZE_LIMIT / 2)];

  for (uint size = 2; size < arrayLength; size <<= 1) {
    // Bitonic merge
    uint ddd = dir ^ ((threadIdx.x & (size / 2)) != 0);

    for (uint stride = size / 2; stride > 0; stride >>= 1) {
      cg::sync(cta);
      uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
      Comparator(shmem[pos + 0], shmem[pos + stride], ddd);
    }
  }

  // ddd == dir for the last bitonic merge step
  {
    for (uint stride = arrayLength / 2; stride > 0; stride >>= 1) {
      cg::sync(cta);
      uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
      Comparator(shmem[pos + 0], shmem[pos + stride], dir);
    }
  }

  cg::sync(cta);
  in[0] = shmem[threadIdx.x + 0];
  in[(SHARED_SIZE_LIMIT / 2)] = shmem[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];
}

__global__ void populateWindows(float *arr, float *windows, uint noOfWindows) {
  uint tid = blockIdx.x * blockDim.x + threadIdx.x;
  uint stride = blockDim.x * gridDim.x;

  for (; tid < noOfWindows; tid += stride) {

    // Populate window
    for (uint i = 0; i < WINDOW_SIZE; i++) {
      windows[tid * WINDOW_SIZE + i] = arr[tid + i];
    }

  }
}

// Helper function (also used by odd-even merge sort)
static uint factorRadix2(uint *log2L, uint L) {
  if (!L) {
    *log2L = 0;
    return 0;
  } else {
    for (*log2L = 0; (L & 1) == 0; L >>= 1, *log2L++)
      ;

    return L;
  }
}

extern "C" uint bitonicSort1(float *out, float *in, uint batchSize,
                            uint arrayLength, uint dir) {
  // Nothing to sort
  if (arrayLength < 2)
    return 0;

  // Only power-of-two array lengths are supported by this implementation
  uint log2L;
  uint factorizationRemainder = factorRadix2(&log2L, arrayLength);
  assert(factorizationRemainder == 1);

  dir = (dir != 0);

  uint blockCount = batchSize * arrayLength / SHARED_SIZE_LIMIT;
  uint threadCount = SHARED_SIZE_LIMIT / 2;

  assert(arrayLength <= SHARED_SIZE_LIMIT);
  assert((batchSize * arrayLength) % SHARED_SIZE_LIMIT == 0);

  bitonicSortShared<<<blockCount, threadCount>>>(out, in, arrayLength, dir);

  return threadCount;
}

extern "C" uint bitonicSort(float *in, uint batchSize,
                            uint arrayLength, uint dir) {
  // Nothing to sort
  if (arrayLength < 2)
    return 0;

  // Only power-of-two array lengths are supported by this implementation
  uint log2L;
  uint factorizationRemainder = factorRadix2(&log2L, arrayLength);
  assert(factorizationRemainder == 1);

  dir = (dir != 0);

  uint blockCount = batchSize * arrayLength / SHARED_SIZE_LIMIT;
  uint threadCount = SHARED_SIZE_LIMIT / 2;

  assert(arrayLength <= SHARED_SIZE_LIMIT);
  assert((batchSize * arrayLength) % SHARED_SIZE_LIMIT == 0);

  bitonicSortKernel<<<blockCount, threadCount>>>(in, arrayLength, dir);

  return threadCount;
}

// void bitonicSlidingMedian(float* res, float* arr, uint arrayLength) {
//   uint dir = 0;

//   uint noOfWindows = (arrayLength - WINDOW_SIZE);

//   float *d_arr;
//   float *d_windows;

//   cudaMalloc(&d_arr, arrayLength * sizeof(float));
//   cudaMemcpy(d_arr, arr, arrayLength * sizeof(float), cudaMemcpyHostToDevice);

//   cudaMalloc(&d_windows, noOfWindows * WINDOW_SIZE * sizeof(float));

//   populateWindows<<<1, 1>>>(d_arr, d_windows, noOfWindows);
//   bitonicSortShared(float *out, float *in, uint batchSize, uint arrayLength, uint dir)

//   cudaFree(d_windows);
//   cudaFree(d_arr);
// }
