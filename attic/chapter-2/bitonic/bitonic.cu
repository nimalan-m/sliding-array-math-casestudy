#include <chrono>
#include <stdio.h>
#include <stdlib.h>

#include "bitonicSort.cu"

using namespace std::chrono;

int main(int argc, char **argv) {

  uint N;

  if (argc < 2) {
    N = 2048;
  } else {
    N = atoi(argv[1]);
  }

  printf("%s Starting...\n\n", argv[0]);

  float *h_in, *median, *madfm;

  const float maxVal = 65536.0;

  printf("Allocating and initializing host arrays...\n\n");
  h_in = (float *)malloc(N * sizeof(float));
  median = (float *)malloc((N - 1024) * sizeof(float));
  madfm = (float *)malloc((N - 1024) * sizeof(float));

  srand(2001);

  for (uint i = 0; i < N; i++) {
    h_in[i] = (float)std::rand() / (float)(RAND_MAX / maxVal);
    // h_in[i] = (N - i) * 1.0f;
  }

  bitonicSlidingMedian(median, madfm, h_in, N);

  free(median);
  free(madfm);
  free(h_in);
}
