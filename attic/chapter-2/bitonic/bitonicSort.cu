#pragma once

#include <assert.h>
#include <cooperative_groups.h>
#include <stdio.h>
#include <stdlib.h>

namespace cg = cooperative_groups;

// This can be 2048
#define SHARED_SIZE_LIMIT 1024U
#define WARP 512U
#define WINDOW_SIZE SHARED_SIZE_LIMIT

inline cudaError_t checkCudaErrors(cudaError_t result) {
  if (result != cudaSuccess) {
    fprintf(stderr, "Cuda Runtime Error : %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
  return result;
}

__device__ inline void Comparator(float &A, float &B, uint dir) {
  float t;
  // dir = 1;

  if ((A > B) == dir) {
    t = A;
    A = B;
    B = t;
  }
}

// Bitonic sort kernel based on Cuda's example for arrays fitting into shared
// memory
__global__ void bitonicSortKernel(float *in, float *out, uint arrayLength,
                                  uint dir) {
  // Handle to thread block group
  cg::thread_block cta = cg::this_thread_block();
  // Shared memory storage for one or more short vectors
  __shared__ float shmem[SHARED_SIZE_LIMIT];

  // Offset to the beginning of subbatch and load data
  in += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;

  shmem[threadIdx.x + 0] = in[0];
  shmem[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = in[(SHARED_SIZE_LIMIT / 2)];

  for (uint size = 2; size < arrayLength; size <<= 1) {
    // Bitonic direction
    uint ddd = dir ^ ((threadIdx.x & (size / 2)) != 0);

    for (uint stride = size / 2; stride > 0; stride >>= 1) {
      cg::sync(cta);
      uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
      Comparator(shmem[pos + 0], shmem[pos + stride], ddd);
    }
  }

  // ddd == dir for the last bitonic merge step
  {
    for (uint stride = arrayLength / 2; stride > 0; stride >>= 1) {
      cg::sync(cta);
      uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
      Comparator(shmem[pos + 0], shmem[pos + stride], dir);
    }
  }

  cg::sync(cta);

  uint mid = SHARED_SIZE_LIMIT / 2;
  float median = (shmem[mid - 1] + shmem[mid]) / 2;

  out[blockIdx.x] = median;

  in[0] = fabs(median - shmem[threadIdx.x + 0]);
  in[(SHARED_SIZE_LIMIT / 2)] =
      fabs(median - shmem[threadIdx.x + (SHARED_SIZE_LIMIT / 2)]);
}

// __global__ void populateWindows(float *arr, float *windows) {
//   cg::thread_block cta = cg::this_thread_block();

//   uint tid = blockIdx.x * blockDim.x + threadIdx.x;

//   __shared__ float shmem[2 * SHARED_SIZE_LIMIT];

//   shmem[threadIdx.x + 0] = arr[tid + 0];
//   shmem[threadIdx.x + WARP] = arr[tid + WARP];
//   shmem[threadIdx.x + 2 * WARP] = arr[tid + 2 * WARP];
//   shmem[threadIdx.x + 3 * WARP] = arr[tid + 3 * WARP];

//   cg::sync(cta);

//   for (uint i = 0; i < WINDOW_SIZE; i++) {
//     windows[tid * WINDOW_SIZE + i] = shmem[threadIdx.x + i];
//   }
// }

__global__ void populateWindows(float *arr, float *windows) {
  windows += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;

  __shared__ float shmem[SHARED_SIZE_LIMIT];

  shmem[threadIdx.x] = arr[blockIdx.x + threadIdx.x + 0];
  shmem[threadIdx.x + WARP] = arr[blockIdx.x + threadIdx.x + WARP];

  windows[0] = (shmem[threadIdx.x]);
  windows[WARP] = shmem[threadIdx.x + WARP];
}

// Helper function (also used by odd-even merge sort)
static uint factorRadix2(uint *log2L, uint L) {
  if (!L) {
    *log2L = 0;
    return 0;
  } else {
    for (*log2L = 0; (L & 1) == 0; L >>= 1, *log2L++)
      ;

    return L;
  }
}

extern "C" uint bitonicSort(float *in, float *out, uint batchSize,
                            uint arrayLength, uint dir) {
  // Nothing to sort
  if (arrayLength < 2)
    return 0;

  // Only power-of-two array lengths are supported by this implementation
  uint log2L;
  uint factorizationRemainder = factorRadix2(&log2L, arrayLength);
  assert(factorizationRemainder == 1);

  dir = (dir != 0);

  uint blockCount = batchSize * arrayLength / SHARED_SIZE_LIMIT;
  uint threadCount = SHARED_SIZE_LIMIT / 2;

  assert(arrayLength <= SHARED_SIZE_LIMIT);
  assert((batchSize * arrayLength) % SHARED_SIZE_LIMIT == 0);

  bitonicSortKernel<<<blockCount, threadCount>>>(in, out, arrayLength, dir);

  return threadCount;
}

void bitonicSlidingMedian(float *median, float *madfm, float *arr,
                          uint arrayLength) {
  uint dir = 1;

  uint noOfWindows = (arrayLength - WINDOW_SIZE);
  uint bufferSize = noOfWindows * WINDOW_SIZE;

  float *d_arr;
  float *d_median;
  float *d_madfm;
  float *d_windows;
  float *h_windows;

#ifdef DEBUG
  printf("Input: \n");
  for (uint i = 0; i < arrayLength; i++) {
    printf("%f ", arr[i]);
  }
  printf("\n");
#endif

  h_windows = (float *)malloc(bufferSize * sizeof(float));

  checkCudaErrors(cudaMallocAsync(&d_arr, arrayLength * sizeof(float), 0));
  checkCudaErrors(cudaMallocAsync(&d_median, noOfWindows * sizeof(float), 0));
  checkCudaErrors(cudaMallocAsync(&d_madfm, noOfWindows * sizeof(float), 0));
  checkCudaErrors(cudaMemcpyAsync(d_arr, arr, arrayLength * sizeof(float),
                             cudaMemcpyHostToDevice));

  checkCudaErrors(cudaMallocAsync(&d_windows, bufferSize * sizeof(float), 0));

  cudaEvent_t start, stop;

  checkCudaErrors(cudaEventCreate(&start));
  checkCudaErrors(cudaEventCreate(&stop));

  checkCudaErrors(cudaEventRecord(start));

  uint blocks = noOfWindows;

  populateWindows<<<blocks, (SHARED_SIZE_LIMIT / 2)>>>(d_arr, d_windows);

#ifdef DEBUG
  checkCudaErrors(cudaMemcpy(h_windows, d_windows, bufferSize * sizeof(float),
                             cudaMemcpyDeviceToHost));
  printf("\nOutput Before Sort:\n");
  for (uint window = 0; window < noOfWindows; window++) {
    for (uint i = 0; i < WINDOW_SIZE; i++) {
      printf("%f ", h_windows[window * WINDOW_SIZE + i]);
    }
    printf("\n");
  }
#endif

  bitonicSort(d_windows, d_median, bufferSize / WINDOW_SIZE, WINDOW_SIZE, dir);

  checkCudaErrors(cudaMemcpyAsync(median, d_median, sizeof(float) * noOfWindows,
                                  cudaMemcpyDeviceToHost));

  bitonicSort(d_windows, d_madfm, bufferSize / WINDOW_SIZE, WINDOW_SIZE, dir);

  checkCudaErrors(cudaMemcpyAsync(madfm, d_madfm, sizeof(float) * noOfWindows,
                                  cudaMemcpyDeviceToHost));

  checkCudaErrors(cudaEventRecord(stop));
  checkCudaErrors(cudaEventSynchronize(stop));

  float elapsedTime;
  checkCudaErrors(cudaEventElapsedTime(&elapsedTime, start, stop));
  printf("Elapsed GPU time %f ms\n", elapsedTime);

#ifdef DEBUG
  checkCudaErrors(cudaMemcpy(h_windows, d_windows, bufferSize * sizeof(float),
                             cudaMemcpyDeviceToHost));
  printf("\nOutput:\n");
  for (uint window = 0; window < noOfWindows; window++) {
    for (uint i = 0; i < WINDOW_SIZE; i++) {
      printf("%f ", h_windows[window * WINDOW_SIZE + i]);
    }
    printf("\n");
  }
#endif

#ifdef ASSERT
  printf("\nMedians:\n");
  for (uint window = 0; window < noOfWindows; window++) {
    printf("%f ", median[window]);
  }
  printf("\n");
  printf("\nMADFM:\n");
  for (uint window = 0; window < noOfWindows; window++) {
    printf("%f ", madfm[window]);
  }
  printf("\n");
#endif

  checkCudaErrors(cudaEventDestroy(start));
  checkCudaErrors(cudaEventDestroy(stop));

  free(h_windows);
  checkCudaErrors(cudaFree(d_median));
  checkCudaErrors(cudaFree(d_madfm));
  checkCudaErrors(cudaFree(d_windows));
  checkCudaErrors(cudaFree(d_arr));
}
