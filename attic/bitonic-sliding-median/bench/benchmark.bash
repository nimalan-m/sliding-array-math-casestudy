#!/usr/bin/env bash
set -e

APP=$1
folder=$2
scenario=$3

# declare -a small_sizes=(288)
declare -a small_sizes=(288 544 1056)
declare -a large_sizes=(2080 4128)

function run {
  folder=$1
  sz=$2
  runs=$3
  shift 3

  mkdir -p $folder
  rm -f $folder/stat_$sz.txt

  for i in `seq 1 $runs`;
  do
    echo "$APP --size $sz $@ >> $folder/stat_$sz.txt"
    $APP --size $sz $constraint >> $folder/stat_$sz.txt
  done
}

# awk '
# /Elapsed/ {
#   printf(\"%f, ", $4);
# }
# /GPU version/ {
#   printf(\"%f\n", $7);
# } '

function benchmark {
  constraint=""

  echo "processing $folder $scenario"

  rm -f $folder/stat_processed.csv

  case "$scenario" in
    0) # GPU
      constraint="--noassert --nofullbx"

      for sz in ${small_sizes[@]}; do
        run $folder $sz 10 $constraint
        awk "
          /GPU version/ {
            printf(\"$sz, %f\n\", \$7);
          } " $folder/stat_$sz.txt >> $folder/stat_processed.csv
      done
      
      for sz in ${large_sizes[@]}; do
        run $folder $sz 2 $constraint
        awk "
          /GPU version/ {
            printf(\"$sz, %f\n\", \$7);
          } " $folder/stat_$sz.txt >> $folder/stat_processed.csv
      done

      ;;
    1) # One pass 2n

      constraint="--noassert --nogpu"

      for sz in ${small_sizes[@]}; do
        run $folder $sz 10 $constraint
        awk "/OMP version/ { printf(\"$sz, %f\n\", \$7); }" $folder/stat_$sz.txt >> $folder/stat_processed.csv
      done
      
      for sz in ${large_sizes[@]}; do
        run $folder $sz 2 $constraint
        awk "/OMP version/ { printf(\"$sz, %f\n\", \$7); }" $folder/stat_$sz.txt >> $folder/stat_processed.csv
      done

      ;;
    2) # One pass 2n-1

      constraint="--noassert --nogpu --nofullbx --halfbx --nocasa"

      for sz in ${small_sizes[@]}; do
        run $folder $sz 10 $constraint
        awk "/OMP version/ { printf(\"$sz, %f\n\", \$7); }" $folder/stat_$sz.txt >> $folder/stat_processed.csv
      done
      
      for sz in ${large_sizes[@]}; do
        run $folder $sz 2 $constraint
        awk "/OMP version/ { printf(\"$sz, %f\n\", \$7); }" $folder/stat_$sz.txt >> $folder/stat_processed.csv
      done
      ;;
    3) # CASA 2n-1
      constraint="--noassert --nogpu --nofullbx --halfbx --noomp"

      for sz in ${small_sizes[@]}; do
        run $folder $sz 10 $constraint
        awk "/CASA wall/ { printf(\"$sz, %f\n\", \$6); }" $folder/stat_$sz.txt >> $folder/stat_processed.csv
      done
      
      for sz in ${large_sizes[@]}; do
        run $folder $sz 2 $constraint
        awk "/CASA wall/ { printf(\"$sz, %f\n\", \$6); }" $folder/stat_$sz.txt >> $folder/stat_processed.csv
      done

      ;;
    
    4) # One pass 2n+1

      constraint="--noassert --nogpu --nofullbx --halfbx --plus1 --nocasa"

      for sz in ${small_sizes[@]}; do
        run $folder $sz 10 $constraint
        awk "/OMP version/ { printf(\"$sz, %f\n\", \$7); }" $folder/stat_$sz.txt >> $folder/stat_processed.csv
      done
      
      for sz in ${large_sizes[@]}; do
        run $folder $sz 2 $constraint
        awk "/OMP version/ { printf(\"$sz, %f\n\", \$7); }" $folder/stat_$sz.txt >> $folder/stat_processed.csv
      done
      ;;
    5) # CASA 2n+1
      constraint="--noassert --nogpu --nofullbx --halfbx --plus1 --noomp"

      for sz in ${small_sizes[@]}; do
        run $folder $sz 10 $constraint
        awk "/CASA wall/ { printf(\"$sz, %f\n\", \$6); }" $folder/stat_$sz.txt >> $folder/stat_processed.csv
      done
      
      for sz in ${large_sizes[@]}; do
        run $folder $sz 2 $constraint
        awk "/CASA wall/ { printf(\"$sz, %f\n\", \$6); }" $folder/stat_$sz.txt >> $folder/stat_processed.csv
      done

      ;;
    *)
      echo "Unknown option"
      exit 1
      ;;
  esac

  echo "processing done"
}

benchmark
