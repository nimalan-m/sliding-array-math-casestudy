#!/usr/bin/env bash
set -e

declare -a folders=(onepass)

APP=$1
FOLDER=./stats

mkdir -p stats

bash benchmark.bash $APP $FOLDER/gpu 0
bash benchmark.bash $APP $FOLDER/op_2 1
bash benchmark.bash $APP $FOLDER/op_16 1
bash benchmark.bash $APP $FOLDER/op_omin 2
bash benchmark.bash $APP $FOLDER/op_omin_casa 3

# for folder in ${folders[@]}; do
#   bash benchmark.bash $APP onepass o
# done
