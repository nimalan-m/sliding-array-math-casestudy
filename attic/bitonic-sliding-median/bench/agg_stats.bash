#!/usr/bin/env bash
set -e

ROOT_FOLDER=$1

declare -a folders=(gpu op_2 op_4 op_16 op_omin op_omin_casa)

function import_data {
  db=$1

  rm -f schema.sql

  cat >> schema.sql << EOF
-- Extensions to processing
.load libcsv
.load libext

EOF

  for folder in ${folders[@]}; do

    # Table for Mean and median stats
    cat >> schema.sql << EOF
-- Tables for aggregates
CREATE VIRTUAL TABLE ${folder}_stats USING csv(
  filename = "./${ROOT_FOLDER}/${folder}/stat_processed.csv",
  schema = "CREATE TABLE x(size integer, stat real)"
);

CREATE TABLE ${folder}_median_stats (
  size integer,
  stat real
);

INSERT INTO ${folder}_median_stats (size, stat)
  SELECT size, median(stat) FROM ${folder}_stats GROUP BY size;

CREATE TABLE ${folder}_mean_stats (
  size integer,
  stat real
);

INSERT INTO ${folder}_mean_stats (size, stat)
  SELECT size, avg(stat) FROM ${folder}_stats GROUP BY size;

EOF
  done

# original real,
cat >> schema.sql << EOF
-- Global aggregate
CREATE TABLE mean_stats (
  size integer,
  casa real,
  onepass real,
  op_2 real,
  op_4 real,
  op_16 real,
  gpu real
);

INSERT INTO mean_stats (size, casa, onepass, op_2, op_4, op_16, gpu)
  SELECT a.size, a.stat, b.stat, c.stat, f.stat, d.stat, e.stat FROM op_omin_casa_mean_stats AS a
    JOIN op_omin_mean_stats AS b ON a.size = b.size
    JOIN op_2_mean_stats AS c ON a.size = c.size
    JOIN op_16_mean_stats AS d ON a.size = d.size
    JOIN gpu_mean_stats AS e ON a.size = e.size
    JOIN op_4_mean_stats AS f ON a.size = f.size;

CREATE TABLE scaling_factor_stats (
  size integer,
  casa real,
  onepass real,
  op_2 real,
  op_4 real,
  op_16 real,
  gpu real
);

INSERT INTO scaling_factor_stats (size, casa, onepass, op_2, op_4, op_16, gpu)
  SELECT a.size, a.stat/a.stat, a.stat/b.stat, a.stat/c.stat, a.stat/f.stat, a.stat/d.stat, a.stat/e.stat FROM op_omin_casa_mean_stats AS a
    JOIN op_omin_mean_stats AS b ON a.size = b.size
    JOIN op_2_mean_stats AS c ON a.size = c.size
    JOIN op_16_mean_stats AS d ON a.size = d.size
    JOIN gpu_mean_stats AS e ON a.size = e.size
    JOIN op_4_mean_stats AS f ON a.size = f.size;

EOF

cat >> schema.sql << EOF
-- Export CSV
.headers on
.mode csv
.once $ROOT_FOLDER/stats.csv
select * from mean_stats order by size;
.once $ROOT_FOLDER/scaling.csv
select * from scaling_factor_stats order by size;
EOF

  sqlite3 $db < schema.sql
}

rm -f stats.db

import_data stats.db
