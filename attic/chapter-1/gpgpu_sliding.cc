#pragma once

/*
  This is 2x better than a serial implementation, this is less as the threading version gives 10x
  This faces a problem with distributing work to the cores in the GPU,
  ctxSynchronize takes 99% of the time
  HtoD, ctxCreate, median, DtoH sees expected performance
  A rewrite in Cuda would be good
*/

#include "common.hh"
#include <casacore/casa/Arrays/LogiArrayFwd.h>
#include <casacore/casa/Arrays/MaskedArray.h>
#include <cassert>

#include <casacore/casa/Arrays.h>
#include <casacore/casa/Arrays/IPosition.h>
#include <cstdio>

#include "omp.h"

namespace casa = casacore;

#pragma omp declare target
inline void swap(float &a, float &b) {
  float temp = a;
  a = b;
  b = temp;
}

float qselect(float *arr, int len, int nth) {
  int start = 0;
  for (int index = 0; index < len - 1; index++) {
    if (arr[index] > arr[len - 1])
      continue;
    swap(arr[index], arr[start]);
    start++;
  }
  swap(arr[len - 1], arr[start]);

  if (nth == start)
    return arr[start];

  return start > nth ? qselect(arr, start, nth)
                     : qselect(arr + start, len - start, nth - start);
}
#pragma omp end declare target

class GPUMedianFunc {
public:
  unsigned int xlen;
  unsigned int ylen;
  // unsigned int xlimit;
  // unsigned int ylimit;
  unsigned int bxlen;
  unsigned int bylen;

  explicit GPUMedianFunc(unsigned int xlen, unsigned int ylen,
                        //  unsigned int xlimit, unsigned int ylimit,
                         unsigned int bxlen, unsigned int bylen)
      : xlen(xlen), ylen(ylen),
        // xlimit(xlimit), ylimit(ylimit),
        bxlen(bxlen), bylen(bylen) {}

#pragma omp declare target
  float operator()(float *arr, bool *mask, int blc[2], int trc[2]) const {
    int len = 0;
    float tmp[(bxlen+1) * (bylen+1)];

    for (int y = blc[1]; y <= trc[1]; y++) {
      for (int x = blc[0]; x <= trc[0]; x++) {
        int idx = y * xlen + x;
        if (mask[idx]) {
          tmp[len] = arr[idx];
          len++;
        }
      }
    }

    unsigned int nth = len / 2;

    float mid = qselect(tmp, len, nth);

    if (len % 2 == 0) {
      mid += qselect(tmp, len, nth - 1);
      mid /= 2.0;
    }

    return mid;
  }
#pragma omp end declare target
};

casa::Array<float> gpgpuSlidingArrayMath(const casa::MaskedArray<float> &array,
                                         const casa::IPosition &halfBoxSize,
                                        //  const GPUMedianFunc &funcObj,
                                         bool fillEdge) {
  size_t ndim = array.ndim();
  const casa::IPosition &shape = array.shape();
  // Set full box size (-1) and resize/fill as needed.
  casa::IPosition hboxsz(2 * halfBoxSize);
  if (hboxsz.size() != array.ndim()) {
    size_t sz = hboxsz.size();
    hboxsz.resize(array.ndim());
    for (size_t i = sz; i < hboxsz.size(); ++i) {
      hboxsz[i] = 0;
    }
  }
  // Determine the output shape. See if anything has to be done.
  casa::IPosition resShape(ndim);
  for (size_t i = 0; i < ndim; ++i) {
    resShape[i] = shape[i] - hboxsz[i];
    if (resShape[i] <= 0) {
      if (!fillEdge) {
        return casa::Array<float>();
      }
      casa::Array<float> res(shape);
      res = float();
      return res;
    }
  }
  // Need to make shallow copy because operator() is non-const.
  // casa::MaskedArray<float> arr(array);
  casa::Array<float> result(resShape);


  assert(result.contiguousStorage());

  float *res = result.data();

  casa::Array<float> arr = array.getArray();
  float *arrData = arr.data();

  casa::LogicalArray mask = array.getMask();
  bool *maskData = mask.data();

  unsigned int resSize = result.size();
  unsigned int arrSize = arr.size();

  assert(arr.contiguousStorage());
  assert(mask.contiguousStorage());
  assert(arr.size() == mask.size());

  int ylimit = shape[1] - hboxsz[1];
  int xlimit = shape[0] - hboxsz[0];

  int ylen = shape[1];
  int xlen = shape[0];

  int bylen = hboxsz[1];
  int bxlen = hboxsz[0];

  GPUMedianFunc median(xlen, ylen, bxlen, bylen);

  // #define DEBUG
  #ifdef DEBUG
    for (int y = 0; y < ylen; y++) {
      for (int x = 0; x < xlen; x++) {
          printf("%f ", arrData[y * xlen + x]);
        }
        printf("\n");
    }
    printf("\nMask Data\n");

    for (int y = 0; y < ylen; y++) {
      for (int x = 0; x < xlen; x++) {
          printf("%d ", maskData[y * xlen + x]);
        }
        printf("\n");
    }
    printf("\n");
  #endif

  // Loop through all data and assemble as needed.

  #pragma omp target map(from:res[:resSize])\
                    map(to:arrSize) \
                    map(to:arrData[:arrSize]) \
                    map(to:maskData[:arrSize]) \
                    map(to:median.xlen, median.ylen, median.bxlen, median.bylen) \
                    map(to:ylimit, xlimit, bxlen, bylen)
  {
    #pragma omp teams
    {

      // int team_id = omp_get_team_num();
      // float* local_result;

      // Distribute work
      #pragma omp distribute
      for (int y = 0; y < ylimit; y++) {
        #pragma omp parallel for
        for (int x = 0; x < xlimit; x++) {
          int pos[ndim];
          int blc[ndim];
          int trc[ndim];

          pos[0] = x;
          pos[1] = y;

          blc[0] = pos[0];
          blc[1] = pos[1];

          trc[0] = blc[0] + bxlen;
          trc[1] = blc[1] + bylen;

          casa::Float val = median(arrData, maskData, blc, trc);

        //  #pragma omp critical
        //   {
            res[y * xlimit + x] = val;
          // }
        }
      }


    }
  }

  if (!fillEdge) {
    return result;
  }
  casa::Array<float> fullResult(shape);
  fullResult = float();
  hboxsz /= 2;
  fullResult(hboxsz, resShape + hboxsz - 1) = result;
  return fullResult;
}

casa::Array<casa::Float> omp_gpu_only_median(casa::Array<casa::Float> &matrix) {
  std::cout << "OpenMP offload optimized" << std::endl;
  casa::MaskedArray<casa::Float> maskedMatrix(matrix, (matrix > THRESHOLD));
  casa::Array<casa::Float> medians = gpgpuSlidingArrayMath(
      maskedMatrix, casa::IPosition(2, WINDOW_SIZE, WINDOW_SIZE), true);

#ifdef DEBUG
  std::cout << "Median: \n";
  for (auto median : medians) {
    std::cout << median << " ";
  }
  std::cout << std::endl;
#endif

  return medians;
}
