//
// Optimizing CASA Mased Sliding Window Math
// Compile with g++ -o main main.cc -lcasa_casa -O3 -fopenmp
//
// Optimization 1:
//      CASA Sliding Window is writing for ND arrays, since the use case in
//      ASKAP is 2D arrays(even in case of spectral it's one channel at a time),
//      the algorithm can be rewritten to be suit 2D arrays, this make it easier
//      for compiler optimizers
//
// Approach 1
//    - OpenMP: Each point window is independent of each other, OpenMP
//    directives can be
//      added on top of Optimization 1
//
// Version 1.......
// CASA call
// V1 Time taken = 21683[ms]
// Version 2.......
// OpenMP optimized call
// V2 Time taken = 1830[ms]
// Asserting.......
// Assertion Done
//
//
#include <casacore/casa/Arrays.h>
#include <casacore/casa/Arrays/IPosition.h>
#include <cassert>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <unistd.h>

#include "common.hh"
#include "cuda_gliding.cc"
#include "gpgpu_sliding.cc"
#include "omp_onepass.cc"
#include "omp_sliding.cc"
#include "reference.cc"

void runAndCompare(casa::Array<casa::Float> fn1(casa::Array<casa::Float> &),
                   casa::Array<casa::Float> fn2(casa::Array<casa::Float> &)) {

  std::cout << " Working on size " << SIZE << std::endl;

  casa::IPosition shape(4, SIZE, SIZE, 1, 1);
  casa::Array<casa::Float> matrix(shape);
  double a = 5.0;

  for (int j = 0; j < SIZE; j++) {
    for (int i = 0; i < SIZE; i++) {
      auto val = (double)std::rand() / (double)(RAND_MAX / a);
      matrix(casacore::IPosition(4, i, j, 0, 0)) = val;
    }
  }

  std::cerr << "Version 1......\n";

  std::chrono::steady_clock::time_point begin =
      std::chrono::steady_clock::now();
  casa::Array<casa::Float> m1 = fn1(matrix);
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  std::cout << "V1 Time taken = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                     begin)
                   .count()
            << "[ms]" << std::endl;

  std::cerr << "Version 2.......\n";

  begin = std::chrono::steady_clock::now();
  casa::Array<casa::Float> m2 = fn2(matrix);
  end = std::chrono::steady_clock::now();
  std::cout << "V2 Time taken = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                     begin)
                   .count()
            << "[ms]" << std::endl;

#ifdef ASSERT
  std::cerr << "Asserting.......\n";
  assertArray(m1, m2);
  std::cout << "Assertion Done" << std::endl;
#endif
}

void runAndCompare(void fn1(casa::Array<casa::Float> &,
                            casa::Array<casa::Float> &,
                            casa::Array<casa::Float> &),
                   void fn2(casa::Array<casa::Float> &,
                            casa::Array<casa::Float> &,
                            casa::Array<casa::Float> &)) {
  casa::IPosition shape(4, SIZE, SIZE, 1, 1);
  casa::Array<casa::Float> array(shape);
  double a = 5.0;

  for (int j = 0; j < SIZE; j++) {
    for (int i = 0; i < SIZE; i++) {
      auto val = (double)std::rand() / (double)(RAND_MAX / a);
      array(casacore::IPosition(4, i, j, 0, 0)) = val;
    }
  }

  std::cerr << "Version 1......\n";

  std::chrono::steady_clock::time_point begin =
      std::chrono::steady_clock::now();
  casa::Array<casa::Float> v1_st1, v1_st2;
  fn1(array, v1_st1, v1_st2);
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  std::cout << "V1 Time taken = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                     begin)
                   .count()
            << "[ms]" << std::endl;

  std::cerr << "Version 2.......\n";

  begin = std::chrono::steady_clock::now();
  casa::Array<casa::Float> v2_st1, v2_st2;
  fn2(array, v2_st1, v2_st2);
  end = std::chrono::steady_clock::now();
  std::cout << "V2 Time taken = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                     begin)
                   .count()
            << "[ms]" << std::endl;

#ifdef ASSERT
  std::cerr << "Asserting.......\n";
  std::cout << "Asserting result 1\n";
  assertArray(v1_st1, v2_st1);

  std::cout << "Asserting result 2\n";
  assertArray(v1_st2, v2_st2);

  std::cout << "Assertion Done" << std::endl;
#endif
}

void run(casa::Array<casa::Float> fn1(casa::Array<casa::Float> &)) {

  std::cout << " Working on size " << SIZE << std::endl;

  casa::IPosition shape(4, SIZE, SIZE, 1, 1);
  casa::Array<casa::Float> matrix(shape);
  double a = 5.0;

  for (int j = 0; j < SIZE; j++) {
    for (int i = 0; i < SIZE; i++) {
      auto val = (double)std::rand() / (double)(RAND_MAX / a);
      matrix(casacore::IPosition(4, i, j, 0, 0)) = val;
    }
  }

  std::cerr << "Running......\n";

  std::chrono::steady_clock::time_point begin =
      std::chrono::steady_clock::now();
  casa::Array<casa::Float> m1 = fn1(matrix);
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  std::cout << "V1 Time taken = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                     begin)
                   .count()
            << "[ms]" << std::endl;
}

void print_usage() {
  printf("Benchmarks\n");
  printf("Usage ./bench -t <test>\n");
  printf("--------------\n");
  printf("Options\n");
  printf("--------------\n");
  printf("1. CASA vs Reference Rewrite\n");
  printf("\tThis is just the same implementation\n");
  printf("2. CASA vs OpenMP Rewrite\n");
  printf("\tCASA vs Rewritten with OpenMP\n");
  printf("3. CASA Median MADFM vs One pass Median MADFM OpenMP Rewrite\n");
  printf("\tCASA vs One pass OpenMP rewrite\n");
  printf("GPU Preliminary Case Studies\n");
  printf("Note: These case studies may be unstable/inefficient, the optimized "
         "cuda implementation in a different section of this repo\n");
  printf("4. CASA vs Offloading \n");
  printf("\tCASA vs OpenMP offload to GPU\n");
  printf("The initial cuda implementation does not work, it is kept for "
         "reference purposes\n");
  printf("5. CASA vs Cuda \n");
  printf("\tCASA vs initial cuda implementation(not optimized)\n");
  printf("6. OMP rewrite vs initial cuda implementation \n");
  printf("\tOpenMP rewrite vs initial cuda implementation\n");
}

int main(int argc, char **argv) {
  if (argc < 2) {
    print_usage();
    return 1;
  }

  int test;

  int c;
  while ((c = getopt(argc, argv, "t:h")) != -1) {
    switch (c) {
    case 't':
      test = atoi(optarg);
      break;
    default:
      print_usage();
      return 1;
    }
  }

  switch (test) {
  case 1:
    runAndCompare(casa_sliding_only_median, rewrite_reference);
    break;
  case 2:
    runAndCompare(casa_sliding_only_median, omp_only_median);
    break;
  case 3:
    runAndCompare(casa_sliding_median_madfm, omp_onepass_median_madfm);
    break;
  // GPU Case studies
  case 4:
    runAndCompare(casa_sliding_only_median, omp_gpu_only_median);
    break;
  case 5:
    runAndCompare(casa_sliding_only_median, cuda_gliding_only_median);
    break;
  case 6:
    runAndCompare(omp_only_median, cuda_gliding_only_median);
    break;
  }

  // run(cuda_gliding_only_median);

  return 0;
}
