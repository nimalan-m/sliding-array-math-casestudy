#include <boost/mpi/collectives/broadcast.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/timer.hpp>
#include <casacore/casa/Arrays.h>
#include <casacore/casa/Arrays/IPosition.h>

#include "common.hh"
#include "omp_onepass.cc"
#include "omp_sliding.cc"
#include "reference.cc"

/*
4 Workers woring on size 250
Version 1......
Version 1......
Version 1......
Version 1......
V1 Time taken = 906[ms]
Version 2.......
OpenMP optimized call
V1 Time taken = 907[ms]
Version 2.......
OpenMP optimized call
V1 Time taken = 907[ms]
Version 2.......
OpenMP optimized call
V1 Time taken = 908[ms]
Version 2.......
OpenMP optimized call
V2 Time taken = 326[ms]
V2 Time taken = 341[ms]
V2 Time taken = 344[ms]
V2 Time taken = 351[ms]
Executing in Master for reference
Size 1000x1000
Master Complete Exec Time taken = 19817[ms]
OpenMP optimized call
Master Complete Exec Optimized Time taken = 1708[ms]

*/

namespace bmpi = boost::mpi;

class Distributor {
  const bmpi::environment env;

public:
  const bmpi::communicator world;
  const int rank;
  const int size;
  inline bool isMaster() const { return rank == 0; }
  inline bool isWorker() const { return !isMaster(); }
  Distributor(int argc, char **argv)
      : env(argc, argv, bmpi::threading::funneled), world(), rank(world.rank()),
        size(world.size()) {}
};

void runAndCompare(casa::Array<casa::Float> fn1(casa::Array<casa::Float> &),
                   casa::Array<casa::Float> fn2(casa::Array<casa::Float> &),
                   int size, Distributor &dist) {

  if (dist.isMaster()) {
    std::cout << dist.size - 1 << " Workers woring on size " << size
              << std::endl;
  }

  if (dist.isWorker()) {
    casa::IPosition shape(4, size, size, 1, 1);
    casa::Array<casa::Float> matrix(shape);
    double a = 5.0;

    for (int j = 0; j < size; j++) {
      for (int i = 0; i < size; i++) {
        auto val = (double)std::rand() / (double)(RAND_MAX / a);
        matrix(casacore::IPosition(4, i, j, 0, 0)) = val;
      }
    }

    std::cout << "Version 1......\n";

    std::chrono::steady_clock::time_point begin =
        std::chrono::steady_clock::now();
    casa::Array<casa::Float> m1 = fn1(matrix);
    std::chrono::steady_clock::time_point end =
        std::chrono::steady_clock::now();
    std::cout << "V1 Time taken = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                     .count()
              << "[ms]" << std::endl;

    std::cout << "Version 2.......\n";

    begin = std::chrono::steady_clock::now();
    casa::Array<casa::Float> m2 = fn2(matrix);
    end = std::chrono::steady_clock::now();
    std::cout << "V2 Time taken = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                     .count()
              << "[ms]" << std::endl;

    #ifdef ASSERT
      assertArray(m1, m2);
    #endif
  }

  dist.world.barrier();

  if (dist.isMaster()) {

    std::cout << "Executing in Master for reference" << std::endl;

    casa::IPosition shape(4, SIZE, SIZE, 1, 1);
    casa::Array<casa::Float> matrix(shape);
    double a = 5.0;

    for (int j = 0; j < SIZE; j++) {
      for (int i = 0; i < SIZE; i++) {
        auto val = (double)std::rand() / (double)(RAND_MAX / a);
        matrix(casacore::IPosition(4, i, j, 0, 0)) = val;
      }
    }

    std::cout << "Size " << SIZE << "x" << SIZE << std::endl;

    std::chrono::steady_clock::time_point begin =
        std::chrono::steady_clock::now();
    casa::Array<casa::Float> m1 = fn1(matrix);
    std::chrono::steady_clock::time_point end =
        std::chrono::steady_clock::now();
    std::cout << "Master Complete Exec Time taken = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                     .count()
              << "[ms]" << std::endl;

    begin = std::chrono::steady_clock::now();
    casa::Array<casa::Float> m2 = fn2(matrix);
    end = std::chrono::steady_clock::now();
    std::cout << "Master Complete Exec Optimized Time taken = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                     .count()
              << "[ms]" << std::endl;
  }
}

void runAndCompare(void fn1(casa::Array<casa::Float> &, casa::Array<casa::Float> &, casa::Array<casa::Float> &),
                   void  fn2(casa::Array<casa::Float> &, casa::Array<casa::Float> &, casa::Array<casa::Float> &),
                   int size, Distributor &dist) {

  if (dist.isMaster()) {
    std::cout << dist.size - 1 << " Workers woring on size " << size
              << std::endl;
  }

  if (dist.isWorker()) {
    casa::IPosition shape(4, size, size, 1, 1);
    casa::Array<casa::Float> array(shape);
    double a = 5.0;

    for (int j = 0; j < size; j++) {
      for (int i = 0; i < size; i++) {
        auto val = (double)std::rand() / (double)(RAND_MAX / a);
        array(casacore::IPosition(4, i, j, 0, 0)) = val;
      }
    }

    std::cout << "Version 1......\n";

    std::chrono::steady_clock::time_point begin =
        std::chrono::steady_clock::now();
    casa::Array<casa::Float> v1_st1, v1_st2;
    fn1(array, v1_st1, v1_st2);
    std::chrono::steady_clock::time_point end =
        std::chrono::steady_clock::now();
    std::cout << "V1 Time taken = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                     .count()
              << "[ms]" << std::endl;

    std::cout << "Version 2.......\n";

    begin = std::chrono::steady_clock::now();
    casa::Array<casa::Float> v2_st1, v2_st2;
    fn2(array, v2_st1, v2_st2);
    end = std::chrono::steady_clock::now();
    std::cout << "V2 Time taken = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                     .count()
              << "[ms]" << std::endl;

    #ifdef ASSERT
      assertArray(v1_st1, v2_st1);
      assertArray(v1_st2, v2_st2);
    #endif
  }

  dist.world.barrier();

  if (dist.isMaster()) {

    std::cout << "Executing in Master for reference" << std::endl;

    casa::IPosition shape(4, SIZE, SIZE, 1, 1);
    casa::Array<casa::Float> array(shape);
    double a = 5.0;

    for (int j = 0; j < SIZE; j++) {
      for (int i = 0; i < SIZE; i++) {
        auto val = (double)std::rand() / (double)(RAND_MAX / a);
        array(casacore::IPosition(4, i, j, 0, 0)) = val;
      }
    }

    std::cout << "Size " << SIZE << "x" << SIZE << std::endl;

    std::chrono::steady_clock::time_point begin =
        std::chrono::steady_clock::now();
    casa::Array<casa::Float> v1_st1, v1_st2;
    fn1(array, v1_st1, v1_st2);
    std::chrono::steady_clock::time_point end =
        std::chrono::steady_clock::now();
    std::cout << "Master Complete Exec Time taken = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                     .count()
              << "[ms]" << std::endl;

    begin = std::chrono::steady_clock::now();
    casa::Array<casa::Float> v2_st1, v2_st2;
    fn2(array, v2_st1, v2_st2);
    end = std::chrono::steady_clock::now();
    std::cout << "Master Complete Exec Optimized Time taken = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                       begin)
                     .count()
              << "[ms]" << std::endl;
  }
}

int main(int argc, char **argv) {

  Distributor dist(argc, argv);

  int size = SIZE / (dist.size - 1);

  #ifdef OVERLAP
  size += 2*WINDOW_SIZE; // Overlap to prevent missing calculations
  #endif

  // runAndCompare(casa_sliding_only_median, omp_only_median, size, dist);
  runAndCompare(casa_sliding_median_madfm, omp_onepass_median_madfm, size, dist);

  return 0;
}
