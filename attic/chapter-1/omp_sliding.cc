#pragma once

#include "common.hh"

#include <assert.h>
#include <casacore/casa/Arrays.h>
#include <casacore/casa/Arrays/IPosition.h>

namespace casa = casacore;

casa::Array<float>
optimalSlidingArrayMath(const casa::MaskedArray<float> &array,
                        const casa::IPosition &halfBoxSize,
                        const ModMedianFunc &funcObj, bool fillEdge) {
  size_t ndim = array.ndim();
  const casa::IPosition &shape = array.shape();
  // Set full box size (-1) and resize/fill as needed.
  casa::IPosition hboxsz(2 * halfBoxSize);
  if (hboxsz.size() != array.ndim()) {
    size_t sz = hboxsz.size();
    hboxsz.resize(array.ndim());
    for (size_t i = sz; i < hboxsz.size(); ++i) {
      hboxsz[i] = 0;
    }
  }
  // Determine the output shape. See if anything has to be done.
  casa::IPosition resShape(ndim);
  for (size_t i = 0; i < ndim; ++i) {
    resShape[i] = shape[i] - hboxsz[i];
    if (resShape[i] <= 0) {
      if (!fillEdge) {
        return casa::Array<float>();
      }
      casa::Array<float> res(shape);
      res = float();
      return res;
    }
  }
  // Need to make shallow copy because operator() is non-const.
  casa::MaskedArray<float> arr(array);
  casa::Array<float> result(resShape);
  assert(result.contiguousStorage());
  float *res = result.data();
  // Loop through all data and assemble as needed.
  // casa::IPosition blc(ndim, 0);
  // casa::IPosition trc(hboxsz);
  // casa::IPosition pos(ndim, 0);

  #pragma omp parallel for shared(res)
  for (int y = 0; y < shape[1] - hboxsz[1]; y++) {
    for (int x = 0; x < shape[0] - hboxsz[0]; x++) {
      casa::IPosition pos(ndim);
      casa::IPosition blc(ndim);
      casa::IPosition trc(ndim);

      pos[0] = x;
      pos[1] = y;
      pos[2] = pos[3] = 0;

      blc[0] = pos[0];
      blc[1] = pos[1];
      blc[2] = blc[3] = 0;

      trc[0] = blc[0] + hboxsz[0];
      trc[1] = blc[1] + hboxsz[1];
      trc[2] = trc[3] = 0;

      // printf("[%d, %d][%d, %d]pos: [%d, %d]\n", blc[0], blc[1], trc[0],
      // trc[1],
      //        pos[0], pos[1]);
      casa::Float val = funcObj(arr(blc, trc));

      result(pos) = val;
        // result[pos[0]][pos[1]] = val;
#ifdef DEBUG_POS
      std::cerr << blc << trc << " pos: " << pos << std::endl;
#endif
    }
  }

  if (!fillEdge) {
    return result;
  }
  casa::Array<float> fullResult(shape);
  fullResult = float();
  hboxsz /= 2;
  fullResult(hboxsz, resShape + hboxsz - 1) = result;
  return fullResult;
}

casa::Array<casa::Float> omp_only_median(casa::Array<casa::Float> &matrix) {
  std::cout << "OpenMP optimized call" << std::endl;
  casa::MaskedArray<casa::Float> maskedMatrix(matrix, (matrix > THRESHOLD));
  casa::Array<casa::Float> medians = optimalSlidingArrayMath(
      maskedMatrix, casa::IPosition(2, WINDOW_SIZE, WINDOW_SIZE),
      ModMedianFunc(), true);

#ifdef DEBUG
  std::cout << "Median: \n";
  for (auto median : medians) {
    std::cout << median << " ";
  }
  std::cout << std::endl;
#endif

  return medians;
}

