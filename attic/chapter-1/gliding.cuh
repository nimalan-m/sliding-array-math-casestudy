#pragma once

#ifndef NDIM
#define NDIM 2
#endif

#ifndef WINDOW_SIZE
#define WINDOW_SIZE 8
#endif

void gliding_window(float *arr, bool *mask, float *res, int shape[NDIM], int hboxsz[NDIM],
                    int resShape[NDIM]);
