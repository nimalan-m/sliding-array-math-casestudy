## Chapter - 1 First Benchmarks

### Benchmarks

```
Benchmarks
Usage ./bench -t <test>
--------------
Options
--------------
1. CASA vs Reference Rewrite
        This is just the same implementation
2. CASA vs OpenMP Rewrite
        CASA vs Rewritten with OpenMP
3. CASA Median MADFM vs One pass Median MADFM OpenMP Rewrite
        CASA vs One pass OpenMP rewrite
GPU Preliminary Case Studies
Note: These case studies may be unstable/inefficient, the optimized cuda implementation in a different section of this repo
4. CASA vs Offloading
        CASA vs OpenMP offload to GPU
5. CASA vs Cuda
        CASA vs initial cuda implementation(not optimized)
6. OMP rewrite vs initial cuda implementation
        OpenMP rewrite vs initial cuda implementation
```

Microbenchmarking via Google benchmark was not used as offloading and cuda implementation were present

### MPI Benchmarks

- `mpiexec -n <N> ./mpi_bench`
