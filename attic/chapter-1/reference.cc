#pragma once

#include <cassert>
#include <casacore/casa/Arrays.h>
#include <casacore/casa/Arrays/IPosition.h>

#include "common.hh"

namespace casa = casacore;

void sliding_medians(casa::Array<casa::Float> matrix) {
  casa::Array<casa::Float> medians = casa::slidingArrayMath(
      matrix, casa::IPosition(2, WINDOW_SIZE, WINDOW_SIZE),
      casa::MedianFunc<casa::Float>());

  std::cout << "Median: \n";
  for (auto median : medians) {
    std::cout << median << " ";
  }
  std::cout << std::endl;
}

casa::Array<casa::Float>
casa_sliding_only_median(casa::Array<casa::Float> &matrix) {
  casa::MaskedArray<casa::Float> maskedArray(matrix, (matrix > THRESHOLD));
  casa::Array<casa::Float> maskedMedians = casa::slidingArrayMath(
      maskedArray, casa::IPosition(2, WINDOW_SIZE, WINDOW_SIZE),
      ModMedianFunc());
      // casa::MaskedMedianFunc<casa::Float>());

#ifdef DEBUG
  std::cout << "Masked Median: \n";
  for (auto median : maskedMedians) {
    std::cout << median << " ";
  }
  std::cout << std::endl;
#endif
  return maskedMedians;
}

void casa_sliding_median_madfm(casa::Array<casa::Float> &matrix, casa::Array<casa::Float> &maskedMedians, casa::Array<casa::Float> &maskedMadfm) {
  std::cout << "Median and MADFM Calculated in two separate calls" << std::endl;
  casa::MaskedArray<casa::Float> maskedMatrix(matrix, (matrix > THRESHOLD));
  maskedMedians = casa::slidingArrayMath(
      maskedMatrix, casa::IPosition(2, WINDOW_SIZE, WINDOW_SIZE),
      casa::MaskedMedianFunc<casa::Float>());
  
  maskedMadfm = casa::slidingArrayMath(
      maskedMatrix, casa::IPosition(2, WINDOW_SIZE, WINDOW_SIZE),
      casa::MaskedMadfmFunc<casa::Float>());

#ifdef DEBUG
  std::cout << "Masked Median: \n";
  for (auto median : maskedMedians) {
    std::cout << median << " ";
  }
  std::cout << std::endl;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Reference is taken from CASA
casa::Array<float> origSlidingArrayMath(const casa::MaskedArray<float> &array,
                                        const casa::IPosition &halfBoxSize,
                                        const ModMedianFunc &funcObj,
                                        bool fillEdge) {
  size_t ndim = array.ndim();
  const casa::IPosition &shape = array.shape();
  // Set full box size (-1) and resize/fill as needed.
  casa::IPosition hboxsz(2 * halfBoxSize);
  if (hboxsz.size() != array.ndim()) {
    size_t sz = hboxsz.size();
    hboxsz.resize(array.ndim());
    for (size_t i = sz; i < hboxsz.size(); ++i) {
      hboxsz[i] = 0;
    }
  }
  // Determine the output shape. See if anything has to be done.
  casa::IPosition resShape(ndim);
  for (size_t i = 0; i < ndim; ++i) {
    resShape[i] = shape[i] - hboxsz[i];
    if (resShape[i] <= 0) {
      if (!fillEdge) {
        return casa::Array<float>();
      }
      casa::Array<float> res(shape);
      res = float();
      return res;
    }
  }
  // Need to make shallow copy because operator() is non-const.
  casa::MaskedArray<float> arr(array);
  casa::Array<float> result(resShape);
  assert(result.contiguousStorage());
  float *res = result.data();
  // Loop through all data and assemble as needed.
  casa::IPosition blc(ndim, 0);
  casa::IPosition trc(hboxsz);
  casa::IPosition pos(ndim, 0);
  while (true) {
    *res++ = funcObj(arr(blc, trc));
    size_t ax;
    for (ax = 0; ax < ndim; ax++) {
      if (++pos[ax] < resShape[ax]) {
        blc[ax]++;
        trc[ax]++;
#ifdef DEBUG_POS
        std::cerr << blc << trc << " pos: " << pos << std::endl;
#endif
        break;
      }
      pos(ax) = 0;
      blc[ax] = 0;
      trc[ax] = hboxsz[ax];
    }
    if (ax == ndim) {
      break;
    }
  }
  if (!fillEdge) {
    return result;
  }
  casa::Array<float> fullResult(shape);
  fullResult = float();
  hboxsz /= 2;
  fullResult(hboxsz, resShape + hboxsz - 1) = result;
  return fullResult;
}

casa::Array<casa::Float> rewrite_reference(casa::Array<casa::Float> &array) {
  std::cout << "CASA call" << std::endl;
  casa::MaskedArray<casa::Float> maskedMatrix(array, (array > THRESHOLD));
  casa::Array<casa::Float> medians = origSlidingArrayMath(
      maskedMatrix, casa::IPosition(2, WINDOW_SIZE, WINDOW_SIZE),
      ModMedianFunc(), true);

#ifdef DEBUG
  std::cout << "Median: \n";
  for (auto median : medians) {
    std::cout << median << " ";
  }
  std::cout << std::endl;
#endif

  return medians;
}
