#pragma once
#include <casacore/casa/Arrays.h>

#ifndef SIZE
#define SIZE 1000
#endif

#ifndef WINDOW_SIZE
#define WINDOW_SIZE 25
#endif

#ifndef THRESHOLD
#define THRESHOLD 1.0f
#endif

#ifndef NDIM
#define NDIM 2
#endif

#define TOLERANCE 0.0001

#define ASSERT

namespace casa = casacore;

void print_matrix(casacore::Matrix<casacore::Float> matrix) {
  for (int i = 0; i < SIZE; ++i) {
    for (int j = 0; j < SIZE; ++j) {
      std::cout << matrix(casacore::IPosition(2, i, j)) << " ";
    }
    std::cout << std::endl;
  }
}

void assertArray(casa::Array<casa::Float> &m1, casa::Array<casa::Float> &m2) {
  for (ssize_t i = 0; i < m1.shape()[0]; i++) {
    for (ssize_t j = 0; j < m1.shape()[1]; j++) {

      float val =
          fabs(m1(casa::IPosition(4, i, j, 0, 0)) - m2(casa::IPosition(4, i, j, 0, 0)));

      if (val > TOLERANCE) {
        std::cout << "i : " << i << " j: " << j << " val: " << val << std::endl;
        std::cout << m1(casa::IPosition(4, i, j, 0, 0)) << " vs "
                  << m2(casa::IPosition(4, i, j, 0, 0)) << std::endl;
        // std::cerr << "Assertion failed" << std::endl;
        exit(1);
      }
    }
  }
}

class ModMedianFunc {
public:
  explicit ModMedianFunc(bool sorted = false, bool takeEvenMean = true)
      : itsSorted(sorted), itsTakeEvenMean(takeEvenMean) {}
  float operator()(const casacore::MaskedArray<float> &arr) const {
    #ifdef DEBUG
    std::cout << arr << std::endl;
    #endif
    return median(arr, itsSorted, itsTakeEvenMean);
  }

private:
  bool itsSorted;
  bool itsTakeEvenMean;
  bool itsInPlace;
};
