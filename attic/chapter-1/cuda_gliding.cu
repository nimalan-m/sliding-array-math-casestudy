#pragma once

#include "gliding.cuh"
#include <float.h>
#include <stdio.h>
#include <assert.h>

/*
  This works and can be plugged into a CASA array
  But isn't yet flexible enough for actual use.
  Limitations:
    - SIZE - 2*WINDOW_SIZE has to be a multiple of 8
    - WINDOW_SIZE is fixed
    - Instead of malloc arrays are fixed sized inside array
    - Assertions are need on array sizes
*/

__device__ inline void swap(float &a, float &b) {
  float temp = a;
  a = b;
  b = temp;
}

__device__ float d_qselect(float *arr, int len, int nth) {
  int start = 0;
  for (int index = 0; index < len - 1; index++) {
    if (arr[index] > arr[len - 1])
      continue;
    swap(arr[index], arr[start]);
    start++;
  }
  swap(arr[len - 1], arr[start]);

  if (nth == start)
    return arr[start];

  return start > nth ? d_qselect(arr, start, nth)
                     : d_qselect(arr + start, len - start, nth - start);
}

__device__ float sliding_median(float *arr, bool *mask, int blc[2], int trc[2],
                                unsigned int xlen, unsigned int ylen,
                                unsigned int bxlen, unsigned int bylen) {

  // float *tmp = (float *)malloc(sizeof(float) * bxlen * bylen);
  float tmp[WINDOW_SIZE * WINDOW_SIZE];
  int len = 0;

  //printf("Val%d %d %d %d\n", xlen, ylen, bxlen, bylen);

  for (int y = blc[1]; y <= trc[1]; y++) {
    for (int x = blc[0]; x <= trc[0]; x++) {
      int idx = y * xlen + x;
      if (mask[idx]) {
        tmp[len] = arr[idx];
        len++;
      }
    }
  }

  unsigned long nth = len / 2;

  float mid = d_qselect(tmp, len, nth);

  if (len % 2 == 0) {
    mid += d_qselect(tmp, len, nth - 1);
    mid /= 2.0;
  }

  //printf("Before free%f %d %d\n", mid, bxlen, bylen);

  //free(tmp);

  //printf("After free%f\n", mid);

  return mid;
}

__global__ void
sliding_window_kernel(float *arr, bool *mask, float *result,
                      const unsigned int xlen, const unsigned int ylen,
                      const unsigned int xlimit, const unsigned int ylimit,
                      const unsigned int bxlen, const unsigned int bylen) {
  unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;

  int pos[NDIM];
  int blc[NDIM];
  int trc[NDIM];

  pos[0] = x;
  pos[1] = y;

  blc[0] = pos[0];
  blc[1] = pos[1];

  trc[0] = blc[0] + bxlen;
  trc[1] = blc[1] + bylen;

  // printf("%d %d %d %d %d %d\n", x, y, xlen, ylen, bxlen, bylen);

  float mid = sliding_median(arr, mask, blc, trc, xlen, ylen, bxlen, bylen);
  // printf("%d %d %f\n", x, y, mid);

  result[y * xlimit + x] = mid;
}

void gliding_window(float *arr, bool *mask, float *res, int shape[NDIM],
                    int hboxsz[NDIM], int resShape[NDIM]) {

  size_t arrSize = sizeof(float) * shape[0] * shape[1];
  size_t maskSize = sizeof(bool) * shape[0] * shape[1];
  size_t resSize = sizeof(float) * resShape[0] * resShape[1];

  float *d_arr;
  bool *d_mask;
  float *d_res;

  int ylimit = resShape[1];
  int xlimit = resShape[0];

  int ylen = shape[1];
  int xlen = shape[0];

  int bylen = hboxsz[1];
  int bxlen = hboxsz[0];

  cudaMalloc(&d_arr, arrSize);
  cudaMalloc(&d_mask, maskSize);
  cudaMalloc(&d_res, resSize);

  cudaMemcpy(d_arr, arr, arrSize, cudaMemcpyHostToDevice);
  cudaMemcpy(d_mask, mask, maskSize, cudaMemcpyHostToDevice);

  dim3 dimBlock(16, 16, 1);
  int gridx = xlimit / dimBlock.x;
  int gridy = ylimit / dimBlock.y;

  assert (gridx != 0 && gridy != 0);

  dim3 dimGrid(gridx, gridy, 1);

  sliding_window_kernel<<<dimGrid, dimBlock, 0>>>(
      d_arr, d_mask, d_res, xlen, ylen, xlimit, ylimit, bxlen, bylen);

  cudaError_t error = cudaGetLastError();
  if (error != cudaSuccess) {
    printf("Error executing kernel %s\n", cudaGetErrorString(error));
    exit(1);
  }

  cudaDeviceSynchronize();

  cudaMemcpy(res, d_res, resSize, cudaMemcpyDeviceToHost);

  #ifdef CDEBUG
  printf("Device: \n");
  for (int y = 0; y < resShape[1]; y++) {
    for (int x = 0; x < resShape[0]; x++) {
      printf("%f ", res[y * xlimit + x]);
    }
    printf("\n");
  }
  #endif

  cudaFree(d_mask);
  cudaFree(d_arr);
  cudaFree(d_res);
}
