#pragma once

#include "common.hh"
#include <casacore/casa/Arrays.h>
#include <casacore/casa/Arrays/IPosition.h>
#include <cassert>
#include <cstdio>
#include "gliding.cuh"

casa::Array<float> cudaGlidingArrayMath(const casa::MaskedArray<float> &array,
                                         const casa::IPosition &halfBoxSize,
                                         bool fillEdge) {
  size_t ndim = array.ndim();
  const casa::IPosition &shape = array.shape();
  // Set full box size (-1) and resize/fill as needed.
  casa::IPosition hboxsz(2 * halfBoxSize);
  if (hboxsz.size() != array.ndim()) {
    size_t sz = hboxsz.size();
    hboxsz.resize(array.ndim());
    for (size_t i = sz; i < hboxsz.size(); ++i) {
      hboxsz[i] = 0;
    }
  }
  // Determine the output shape. See if anything has to be done.
  casa::IPosition resShape(ndim);
  for (size_t i = 0; i < ndim; ++i) {
    resShape[i] = shape[i] - hboxsz[i];
    if (resShape[i] <= 0) {
      if (!fillEdge) {
        return casa::Array<float>();
      }
      casa::Array<float> res(shape);
      res = float();
      return res;
    }
  }
  // Need to make shallow copy because operator() is non-const.
  // casa::MaskedArray<float> arr(array);
  casa::Array<float> result(resShape);


  assert(result.contiguousStorage());

  float *resData = result.data();

  casa::Array<float> arr = array.getArray();
  float *arrData = arr.data();

  casa::LogicalArray mask = array.getMask();
  bool *maskData = mask.data();

  assert(arr.contiguousStorage());
  assert(mask.contiguousStorage());
  assert(arr.size() == mask.size());


  int arrSize[NDIM] = { (int) shape[0], (int) shape[1] };
  int boxSize[NDIM] = { (int) hboxsz[0], (int) hboxsz[1] };
  int resSize[NDIM] = { (int) (shape[0] - hboxsz[0]), (int) (shape[1] - hboxsz[1]) };

  std::cout << arrSize[0] << " " << arrSize[1] << std::endl;
  std::cout << boxSize[0] << " " << boxSize[1] << std::endl;
  std::cout << resSize[0] << " " << resSize[1] << std::endl;

  gliding_window(arrData, maskData, resData, arrSize, boxSize, resSize);

  #ifdef CDEBUG
  std::cout << "Res: \n";
  for (int y = 0; y < resSize[1]; y++) {
    for (int x = 0; x < resSize[0]; x++) {
      std::cout << result(casacore::IPosition(4, x, y, 0, 0)) << " ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
  #endif

  if (!fillEdge) {
    return result;
  }
  casa::Array<float> fullResult(shape);
  fullResult = float();
  hboxsz /= 2;
  fullResult(hboxsz, resShape + hboxsz - 1) = result;
  return fullResult;
}

casa::Array<casa::Float> cuda_gliding_only_median(casa::Array<casa::Float> &matrix) {
  std::cout << "Cuda offload optimized" << std::endl;
  casa::MaskedArray<casa::Float> maskedMatrix(matrix, (matrix > THRESHOLD));
  casa::Array<casa::Float> medians = cudaGlidingArrayMath(
      maskedMatrix, casa::IPosition(2, WINDOW_SIZE, WINDOW_SIZE), true);

#ifdef CDEBUG
  std::cout << "Median: \n";
  for (auto median : medians) {
    std::cout << median << " ";
  }
  std::cout << std::endl;
#endif

  return medians;
}

