# Benchmarks

## Section 3.1: Casacore MedianSlider vs slidingArrayMath

Base performance of running Casacore two scans of slidingArrayMath with MaskedArray to calculate median and MADFM.

Comparison of MedianSlider vs slidingArrayMath

## Section 3.2: Optimising Casacore slidingArrayMath

Performance comparison of Casacore vs optimized version with OpenMP

## Section 3.3: Wide Array Median is Memory Intensive

Performance comparison of
  1. Casacore Median vs Median OpenMP offloading
  2. Casacore Mean vs Mean OpenMP offloading

# Performance

Section 3.1 - Ryzen 7 5800X

512x512   - 47.71s
1024x1024 - 240.82s
2048x2048 - 1074.65s

