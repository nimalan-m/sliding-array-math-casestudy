#include <casacore/casa/Arrays.h>
#include <casacore/casa/Arrays/IPosition.h>
#include <chrono>
#include <cstdio>
#include <cstdlib>

#include "Wallclock.hh"

namespace casa = casacore;

#define THRESHOLD 2.0f
#define RANGE 100.0
#define HALF_BOX_SIZE 50

void casa_sliding_median_madfm(casa::Array<casa::Float> &matrix,
                               casa::Array<casa::Float> &maskedMedians,
                               casa::Array<casa::Float> &maskedMadfm) {
  std::cout << "Calculatint Median and MADFM in two separate calls"
            << std::endl;
  casa::MaskedArray<casa::Float> maskedMatrix(matrix, (matrix > THRESHOLD));
  maskedMedians = casa::slidingArrayMath(
      maskedMatrix, casa::IPosition(2, HALF_BOX_SIZE, HALF_BOX_SIZE),
      casa::MaskedMedianFunc<casa::Float>());

  maskedMadfm = casa::slidingArrayMath(
      maskedMatrix, casa::IPosition(2, HALF_BOX_SIZE, HALF_BOX_SIZE),
      casa::MaskedMadfmFunc<casa::Float>());
}

int main(int argc, char **argv) {
  size_t N = 1000;

  if (argc == 2) {
    N = atoi(argv[1]);
  }

  casa::IPosition shape(4, N, N, 1, 1);
  casa::Array<casa::Float> matrix(shape);

  for (size_t j = 0; j < N; j++) {
    for (size_t i = 0; i < N; i++) {
      auto val = (double)std::rand() / (double)(RAND_MAX / RANGE);
      matrix(casacore::IPosition(4, i, j, 0, 0)) = val;
    }
  }

  WallClock clk;

  casa::Array<casa::Float> median, madfm;

  clk.tick();
  casa_sliding_median_madfm(matrix, median, madfm);

  double elapsedTime = clk.elapsedTime();
  std::cout << "Time taken = " << elapsedTime << "[ms]" << std::endl;
}
