# 3.1 Performance of slidingArrayMath

Case study of time taken with CASACORE's `slidingArrayMath` to calculate masked sliding array Median and MADFM of a matrix

## Running the benchmark

```sh
# Run for the default benchmark size 1000x1000
./casa-perf

# Run for the default benchmark size 500x500
./casa-perf 500
```

## Performance

| Machine                  | Intel(R) Core(TM) i7-9750H CPU |
|                          |        @ 2.60GHz               |
|--------------------------|--------------------------------|
| Matrix Size 500x500      |            53 s                |
| Matrix Size 1024x1024    |           284 s                |

