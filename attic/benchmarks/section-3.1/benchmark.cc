#include <casacore/casa/Arrays.h>
#include <casacore/casa/Arrays/IPosition.h>
#include <casacore/scimath/Mathematics/MedianSlider.h>
#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <iostream>

#include "Wallclock.hh"
#include "helpers.hh"

namespace casa = casacore;

void median_slider_approach_1d(const casa::Array<casa::Float> &arr,
                               const casa::IPosition &half_box,
                               casa::Array<casa::Float> &out) {
  casa::IPosition box = 2 * half_box;
  casa::IPosition shape = arr.shape();

  casa::MedianSlider slider(half_box[0]);

  for (size_t idx = 0; idx < box[0]; idx++) {
    slider.add(arr[idx]);
  }

  out = 0.0f;
  for (size_t idx = box[0]; idx < shape[0]; idx++) {
    slider.add(arr[idx]);
    float median = slider.median();
    out[idx - half_box[0]] = median;
  }
}

void median_slider_approach_1d(const casa::MaskedArray<casa::Float> &maskedArr,
                               const casa::IPosition &half_box,
                               casa::Array<casa::Float> &out) {
  casa::IPosition box = 2 * half_box;
  casa::MedianSlider slider(half_box[0]);

  casa::Array<casa::Float> arr = maskedArr.getArray();
  casa::LogicalArray mask = maskedArr.getMask();
  casa::IPosition shape = arr.shape();

  for (size_t idx = 0; idx < box[0]; idx++) {
    slider.add(arr[idx], mask[idx]);
  }

  out = 0.0f;
  for (size_t idx = box[0]; idx < shape[0]; idx++) {
    slider.add(arr[idx], mask[idx]);
    float median = slider.median();
    out[idx - half_box[0]] = median;
  }
}

void bench_1d(size_t N, size_t half_bx_sz, bool masked) {
  std::cout << "Benchmarking sliding array" << std::endl;

  casa::IPosition shape = casa::IPosition(1, N);
  casa::IPosition half_bx = casa::IPosition(1, half_bx_sz);

  casa::Array<casa::Float> median_filter(shape);
  casa::Array<casa::Float> sliding_array(shape);
  casa::Array<casa::Float> arr(shape);

  int idx = 0;
  // Populate random numbers
  for (auto &elem : arr) {
    auto randfloat = (double)std::rand() / (double)(RAND_MAX / RANGE);
    elem = randfloat;
    // elem = idx++;
  }
  casa::MaskedArray<casa::Float> maskedArr(arr, (arr > THRESHOLD));

  {
    WallClock clk;
    clk.tick();

    if (masked) {
      median_slider_approach_1d(maskedArr, half_bx, median_filter);
    } else {
      median_slider_approach_1d(arr, half_bx, median_filter);
    }

    double elapsed = clk.elapsedTime();
    std::cout << "Elapsed time for 1D median slider approach " << elapsed
              << " [ms]" << std::endl;
  }

  {
    WallClock clk;
    clk.tick();

    if (masked) {
      sliding_array = casa::slidingArrayMath(maskedArr, half_bx,
                                             casa::MaskedMedianFunc<casa::Float>());
    } else {
      sliding_array =
          casa::slidingArrayMath(arr, half_bx, casa::MedianFunc<casa::Float>());
    }

    double elapsed = clk.elapsedTime();
    std::cout << "Elapsed time for 1D sliding array approach " << elapsed
              << " [ms]" << std::endl;
  }

  assertArray1D(median_filter, sliding_array);
  std::cout << "Assertions passed for 1D benchmark" << std::endl;
}

int main(int argc, char **argv) {
  size_t N = 1000;
  size_t dim = 1;
  size_t half_bx_sz = 5;
  bool masked = false;
  if (argc == 3) {
    N = atoi(argv[1]);
    half_bx_sz = atoi(argv[2]);
    // dim = atoi(argv[2]);
    // masked = atoi(argv[3]);
  }

  if (dim == 1) {
    bench_1d(N, half_bx_sz, masked);
  } else if (dim == 2) {
    bench_1d(N, half_bx_sz, masked);
  } else {
    std::cout << "This benchmark can be run only on 1D/2D arrays";
    return EXIT_FAILURE;
  }
}
