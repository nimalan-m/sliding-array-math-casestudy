#pragma once

#include <chrono>

class WallClock {
  std::chrono::steady_clock::time_point begin;

public:
  void tick() { begin = std::chrono::steady_clock::now(); }
  double elapsedTime() {
    std::chrono::steady_clock::time_point end =
        std::chrono::steady_clock::now();
    return std::chrono::duration_cast<std::chrono::milliseconds>(end - begin)
        .count();
  }
};
