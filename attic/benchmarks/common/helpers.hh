#pragma once

#include <casacore/casa/Arrays.h>
#include <cassert>

#define TOLERANCE 0.0001
#define THRESHOLD 2.0f
#define RANGE 100.0
#define HALF_BOX_SIZE 50

namespace casa = casacore;

void assertArray(casa::Array<casa::Float> &m1, casa::Array<casa::Float> &m2) {
  for (ssize_t i = 0; i < m1.shape()[0]; i++) {
    for (ssize_t j = 0; j < m1.shape()[1]; j++) {

      float val = fabs(m1(casa::IPosition(4, i, j, 0, 0)) -
                       m2(casa::IPosition(4, i, j, 0, 0)));

      if (val > TOLERANCE) {
        std::cout << "i : " << i << " j: " << j << " val: " << val << std::endl;
        std::cout << m1(casa::IPosition(4, i, j, 0, 0)) << " vs "
                  << m2(casa::IPosition(4, i, j, 0, 0)) << std::endl;
        // std::cerr << "Assertion failed" << std::endl;
        exit(1);
      }
    }
  }
}

void assertArray1D(const casa::Array<casa::Float> expected,
                     const casa::Array<casa::Float> actual) {
  assert(expected.ndim() == actual.ndim());
  assert(expected.ndim() == 1);
  assert(expected.shape()[0] == actual.shape()[0]);

  for (ssize_t idx = 0; idx < expected.shape()[0]; idx++) {
    float val = fabs(expected(casa::IPosition(1, idx)) -
                     actual(casa::IPosition(1, idx)));

    if (val > TOLERANCE) {
      std::cerr << "Assertion failed at " << std::endl;
      std::cout << "idx : " << idx << " diff: " << val << std::endl;
      std::cout << "expected: " << expected(casa::IPosition(1, idx)) << " vs "
                << "actual: " << actual(casa::IPosition(1, idx)) << std::endl;
      assert(val < TOLERANCE);
    }
  }
}
