#include <casacore/casa/Arrays.h>
#include <casacore/casa/Arrays/IPosition.h>

#include <omp.h>

#include "Wallclock.hh"
#include "helpers.hh"

namespace casa = casacore;

#pragma omp declare target
inline void swap(float &a, float &b) {
  float temp = a;
  a = b;
  b = temp;
}

float qselect(float *arr, int len, int nth) {
  int start = 0;
  for (int index = 0; index < len - 1; index++) {
    if (arr[index] > arr[len - 1])
      continue;
    swap(arr[index], arr[start]);
    start++;
  }
  swap(arr[len - 1], arr[start]);

  if (nth == start)
    return arr[start];

  return start > nth ? qselect(arr, start, nth)
                     : qselect(arr + start, len - start, nth - start);
}

#pragma omp end declare target

class OffloadMedianFunc {
public:
  unsigned int xlen;
  unsigned int ylen;
  unsigned int bxsz;

  explicit OffloadMedianFunc(unsigned int xlen, unsigned int ylen,
                             unsigned int bxlen, unsigned int bylen)
      : xlen(xlen), ylen(ylen), bxsz((bxlen+1) * (bylen+1)){};

#pragma omp declare target
  float operator()(float *arr, bool *mask, int blc[2], int trc[2]) const {
    int len = 0;
    float tmp[bxsz];

    for (int y = blc[1]; y <= trc[1]; y++) {
      for (int x = blc[0]; x <= trc[0]; x++) {
        int idx = y * xlen + x;
        if (mask[idx]) {
          tmp[len] = arr[idx];
          len++;
        }
      }
    }

    unsigned int nth = len / 2;

    float mid = qselect(tmp, len, nth);

    if (len % 2 == 0) {
      mid += qselect(tmp, len, nth - 1);
      mid /= 2.0;
    }

    return mid;
  }
#pragma omp end declare target
};

class OffloadMeanFunc {
public:
  unsigned int xlen;
  unsigned int ylen;
  unsigned int bxsz;

  explicit OffloadMeanFunc(unsigned int xlen, unsigned int ylen,
                           unsigned int bxlen, unsigned int bylen)
      : xlen(xlen), ylen(ylen), bxsz((bxlen+1) * (bylen+1)) {};

#pragma omp declare target
  float operator()(float *arr, bool *mask, int blc[2], int trc[2]) const {
    int len = 0;
    float sum = 0.0f;

    for (int y = blc[1]; y <= trc[1]; y++) {
      for (int x = blc[0]; x <= trc[0]; x++) {
        int idx = y * xlen + x;
        if (mask[idx]) {
          sum += arr[idx];
          len++;
        }
      }
    }

    return (sum / (float)len);
  }
#pragma omp end declare target
};

template <typename OffloadFunc>
casa::Array<float>
offloadSlidingArrayMath(const casa::MaskedArray<float> &array,
                        const casa::IPosition &halfBoxSize, bool fillEdge) {
  size_t ndim = array.ndim();
  const casa::IPosition &shape = array.shape();
  // Set full box size (-1) and resize/fill as needed.
  casa::IPosition hboxsz(2 * halfBoxSize);
  if (hboxsz.size() != array.ndim()) {
    size_t sz = hboxsz.size();
    hboxsz.resize(array.ndim());
    for (size_t i = sz; i < hboxsz.size(); ++i) {
      hboxsz[i] = 0;
    }
  }
  // Determine the output shape. See if anything has to be done.
  casa::IPosition resShape(ndim);
  for (size_t i = 0; i < ndim; ++i) {
    resShape[i] = shape[i] - hboxsz[i];
    if (resShape[i] <= 0) {
      if (!fillEdge) {
        return casa::Array<float>();
      }
      casa::Array<float> res(shape);
      res = float();
      return res;
    }
  }
  // Need to make shallow copy because operator() is non-const.
  casa::Array<float> result(resShape);

  assert(result.contiguousStorage());

  float *res = result.data();

  casa::Array<float> arr = array.getArray();
  float *arrData = arr.data();

  casa::LogicalArray mask = array.getMask();
  bool *maskData = mask.data();

  unsigned int resSize = result.size();
  unsigned int arrSize = arr.size();

  assert(arr.contiguousStorage());
  assert(mask.contiguousStorage());
  assert(arr.size() == mask.size());

  int ylimit = shape[1] - hboxsz[1];
  int xlimit = shape[0] - hboxsz[0];

  int ylen = shape[1];
  int xlen = shape[0];

  int bylen = hboxsz[1];
  int bxlen = hboxsz[0];

  // These variables are defined and offloaded
  OffloadFunc funcObj(xlen, ylen, bxlen, bylen);

  // Loop through all data and assemble as needed.

  #pragma omp target  map(from : res[:resSize])   \
                      map(to: arrSize)            \
                      map(to: arrData[:arrSize])  \
                      map(to: maskData[:arrSize]) \
                      map(to: funcObj.xlen, funcObj.ylen, funcObj.bxsz)        \
                      map(to: ylimit, xlimit, bxlen, bylen)
  {
    #pragma omp teams
    {
      // Distribute work
      #pragma omp distribute
      for (int y = 0; y < ylimit; y++) {
        #pragma omp parallel for
        for (int x = 0; x < xlimit; x++) {
          int pos[ndim];
          int blc[ndim];
          int trc[ndim];

          pos[0] = x;
          pos[1] = y;

          blc[0] = pos[0];
          blc[1] = pos[1];

          trc[0] = blc[0] + bxlen;
          trc[1] = blc[1] + bylen;

          casa::Float val = funcObj(arrData, maskData, blc, trc);

          res[y * xlimit + x] = val;
        }
      }
    }
  }

  if (!fillEdge) {
    return result;
  }
  casa::Array<float> fullResult(shape);
  fullResult = float();
  hboxsz /= 2;
  fullResult(hboxsz, resShape + hboxsz - 1) = result;
  return fullResult;
}

int main(int argc, char **argv) {
  size_t N = 1000;

  if (argc == 2) {
    N = atoi(argv[1]);
  }

  casa::IPosition shape(4, N, N, 1, 1);
  casa::Array<casa::Float> matrix(shape);

  for (size_t j = 0; j < N; j++) {
    for (size_t i = 0; i < N; i++) {
      auto val = (double)std::rand() / (double)(RAND_MAX / RANGE);
      matrix(casacore::IPosition(4, i, j, 0, 0)) = val;
    }
  }
  casa::MaskedArray<casa::Float> maskedMatrix(matrix, (matrix > THRESHOLD));

  casa::Array<casa::Float> casaMedian;
  {
    WallClock clk;
    clk.tick();

    casaMedian = casa::slidingArrayMath(
        maskedMatrix, casa::IPosition(2, HALF_BOX_SIZE, HALF_BOX_SIZE),
        casa::MaskedMedianFunc<casa::Float>());

    double elapsedTime = clk.elapsedTime();
    std::cout << "Time taken casa median approach = " << elapsedTime << "[ms]"
              << std::endl;
  }

  casa::Array<casa::Float> offloadMedian;
  {
    WallClock clk;
    clk.tick();

    offloadMedian = offloadSlidingArrayMath<OffloadMedianFunc>(
        maskedMatrix, casa::IPosition(2, HALF_BOX_SIZE, HALF_BOX_SIZE), true);

    double elapsedTime = clk.elapsedTime();
    std::cout << "Time taken offload median approach = " << elapsedTime
              << "[ms]" << std::endl;
  }

  assertArray(casaMedian, offloadMedian);
  std::cout << "Assertion passed median" << std::endl;

casa::Array<casa::Float> casaMean;
  {
    WallClock clk;
    clk.tick();

    casaMean = casa::slidingArrayMath(
        maskedMatrix, casa::IPosition(2, HALF_BOX_SIZE, HALF_BOX_SIZE),
        casa::MaskedMeanFunc<casa::Float>());

    double elapsedTime = clk.elapsedTime();
    std::cout << "Time taken casa mean approach = " << elapsedTime << "[ms]"
              << std::endl;
  }

  casa::Array<casa::Float> offloadMean;
  {
    WallClock clk;
    clk.tick();

    offloadMean = offloadSlidingArrayMath<OffloadMeanFunc>(
        maskedMatrix, casa::IPosition(2, HALF_BOX_SIZE, HALF_BOX_SIZE), true);

    double elapsedTime = clk.elapsedTime();
    std::cout << "Time taken offload mean approach = " << elapsedTime
              << "[ms]" << std::endl;
  }

  assertArray(casaMean, offloadMean);
  std::cout << "Assertion passed mean" << std::endl;
}
