#include "Wallclock.hh"
#include "helpers.hh"

#include <assert.h>
#include <casacore/casa/Arrays.h>
#include <casacore/casa/Arrays/IPosition.h>

namespace casa = casacore;

class MaskedStatFunc {
public:
  explicit MaskedStatFunc(bool sorted = false, bool takeEvenMean = true)
      : itsSorted(sorted), itsTakeEvenMean(takeEvenMean) {}
  void operator()(const casacore::MaskedArray<float> &arr, float &median,
                  float &madfm) const {
    median = casa::median(arr, itsSorted, itsTakeEvenMean);
    // Inlining the MADFM is the actual time save
    casa::MaskedArray<float> absdiff = abs(arr - median);
    madfm = casa::median(absdiff, false, itsTakeEvenMean);
  }

private:
  bool itsSorted;
  bool itsTakeEvenMean;
  bool itsInPlace;
};

template <typename DualStatFunc>
void onePassSlidingArrayMath(const casa::MaskedArray<float> &array,
                             const casa::IPosition &halfBoxSize,
                             casa::Array<float> &stat1,
                             casa::Array<float> &stat2,
                             const DualStatFunc &funcObj, bool fillEdge) {
  size_t ndim = array.ndim();
  const casa::IPosition &shape = array.shape();
  // Set full box size (-1) and resize/fill as needed.
  casa::IPosition hboxsz(2 * halfBoxSize);
  if (hboxsz.size() != array.ndim()) {
    size_t sz = hboxsz.size();
    hboxsz.resize(array.ndim());
    for (size_t i = sz; i < hboxsz.size(); ++i) {
      hboxsz[i] = 0;
    }
  }
  // Determine the output shape. See if anything has to be done.
  casa::IPosition resShape(ndim);
  for (size_t i = 0; i < ndim; ++i) {
    resShape[i] = shape[i] - hboxsz[i];
    if (resShape[i] <= 0) {
      if (!fillEdge) {
        return;
      }
      casa::Array<float> res1(shape);
      res1 = float();
      casa::Array<float> res2(shape);
      res2 = float();

      stat1 = std::move(res1);
      stat2 = std::move(res2);

      return;
    }
  }
  // Need to make shallow copy because operator() is non-const.
  casa::MaskedArray<float> arr(array);
  casa::Array<float> result1(resShape);
  casa::Array<float> result2(resShape);

  assert(result1.contiguousStorage());
  assert(result2.contiguousStorage());
  // Loop through all data and assemble as needed.

#pragma omp parallel for shared(result1, result2)
  for (int y = 0; y < shape[1] - hboxsz[1]; y++) {
    for (int x = 0; x < shape[0] - hboxsz[0]; x++) {
      casa::IPosition pos(ndim);
      casa::IPosition blc(ndim);
      casa::IPosition trc(ndim);

      pos[0] = x;
      pos[1] = y;
      pos[2] = pos[3] = 0;

      blc[0] = pos[0];
      blc[1] = pos[1];
      blc[2] = blc[3] = 0;

      trc[0] = blc[0] + hboxsz[0];
      trc[1] = blc[1] + hboxsz[1];
      trc[2] = trc[3] = 0;

      float val1, val2;
      funcObj(arr(blc, trc), val1, val2);

      result1(pos) = val1;
      result2(pos) = val2;
    }
  }

  if (!fillEdge) {
    stat1 = std::move(result1);
    stat2 = std::move(result2);
    return;
  }
  casa::Array<float> fullResult1(shape);
  casa::Array<float> fullResult2(shape);
  fullResult1 = float();
  fullResult2 = float();
  hboxsz /= 2;
  fullResult1(hboxsz, resShape + hboxsz - 1) = result1;
  fullResult2(hboxsz, resShape + hboxsz - 1) = result2;

  stat1 = std::move(fullResult1);
  stat2 = std::move(fullResult2);
}

int main(int argc, char **argv) {
  size_t N = 1000;

  if (argc == 2) {
    N = atoi(argv[1]);
  }

  casa::IPosition shape(4, N, N, 1, 1);
  casa::Array<casa::Float> matrix(shape);

  for (size_t j = 0; j < N; j++) {
    for (size_t i = 0; i < N; i++) {
      auto val = (double)std::rand() / (double)(RAND_MAX / RANGE);
      matrix(casacore::IPosition(4, i, j, 0, 0)) = val;
    }
  }
  casa::MaskedArray<casa::Float> maskedMatrix(matrix, (matrix > THRESHOLD));

  casa::Array<casa::Float> origMedian, origMadfm;
  {
    WallClock clk;
    clk.tick();
    origMedian = casa::slidingArrayMath(
        maskedMatrix, casa::IPosition(2, HALF_BOX_SIZE, HALF_BOX_SIZE),
        casa::MaskedMedianFunc<casa::Float>());

    origMadfm = casa::slidingArrayMath(
        maskedMatrix, casa::IPosition(2, HALF_BOX_SIZE, HALF_BOX_SIZE),
        casa::MaskedMadfmFunc<casa::Float>());

    double elapsedTime = clk.elapsedTime();
    std::cout << "Time taken original approach = " << elapsedTime << "[ms]"
              << std::endl;
  }

  casa::Array<casa::Float> ompMedian, ompMadfm;
  {
    WallClock clk;
    clk.tick();
    onePassSlidingArrayMath(maskedMatrix,
                            casa::IPosition(2, HALF_BOX_SIZE, HALF_BOX_SIZE),
                            ompMedian, ompMadfm, MaskedStatFunc(), true);

    double elapsedTime = clk.elapsedTime();
    std::cout << "Time taken omp rewrite = " << elapsedTime << "[ms]"
              << std::endl;
  }

  assertArray(origMedian, ompMedian);
  assertArray(origMadfm, ompMadfm);
  std::cout << "Assertion passed" << std::endl;
}
