#include <assert.h>
#include <cstdlib>
#include <limits>
#include <stdio.h>
#include <string>

#include "colors.h"

#define CUDART_INF_F            __int_as_float(0x7f800000U)
#define ASSERT

#define SHARED_MEM_WINDOW 32U
#define BLOCK_ROWS (SHARED_MEM_WINDOW >> 2)
#define SHARED_MEMORY_SIZE (SHARED_MEM_WINDOW * SHARED_MEM_WINDOW)
#define SHMEM_HALF (SHARED_MEMORY_SIZE >> 1)

inline void checkGPU(cudaError_t result, std::string file, int const line) {
  if (result != cudaSuccess) {
    fprintf(stderr, "Cuda Runtime Error at %s:%d : %s\n", file.c_str(), line,
            cudaGetErrorString(result));
    exit(EXIT_FAILURE);
  }
}

#define checkCudaErrors(val) checkGPU(val, __FILE__, __LINE__)

void printArray(float *arr, uint arrSize, uint wSize) {
  for (uint i = 0; i < arrSize; i++) {
    printf("%.1f ", arr[i]);
    if ((i + 1) % wSize == 0) {
      printf("\n");
    }
  }
}

uint highest_power_2(uint n) {
  uint t = n - 1;
  t |= t >> 1;
  t |= t >> 2;
  t |= t >> 4;
  t |= t >> 8;
  t |= t >> 16;
  return t + 1;
}

#define EPHISILON 0.001f

void assertArray(std::string str, float *exp, float *act, uint N) {
  int assert_failure = 0;
  for (uint i = 0; i < N; i++) {
    if (abs(exp[i] - act[i]) > EPHISILON) {
#ifdef ASSERT_VERBOSE
      printf("Assertion failure: %s " RED_TEXT(" idx: %d exp: %f actual: %f\n"),
             str.c_str(), i, exp[i], act[i]);
#endif
      assert_failure = 1;
    }
  }
  if (assert_failure)
    printf(RED_TEXT("Assertion failed %s\n"), str.c_str());
  if (!assert_failure)
    printf(GREEN_TEXT("Assertion passed %s\n"), str.c_str());
}

__global__ void load_2d_kernel(float *in, float *out, uint dim, uint batch_dim,
                               uint wdim, uint real_wdim, uint real_wsize) {
  int offset = (blockIdx.y * batch_dim + blockIdx.x) * real_wsize;

  out += offset;

  int x = threadIdx.x;
  int y = threadIdx.y;

  int ax = blockIdx.x + threadIdx.x;
  int ay = blockIdx.y + threadIdx.y;

  // TODO: Find out which order of loops works best
  for (uint j = 0; j < real_wdim; j += BLOCK_ROWS) {
    for (uint i = 0; i < real_wdim; i += SHARED_MEM_WINDOW) {
      uint oidx = (y + j) * real_wdim + (x + i);
      uint iidx = (ay + j) * dim + (ax + i);
      // printf("o: %d i: %d\n", oidx, iidx);
      if ((x+i) >= wdim || (y+j) >= wdim) {
        out[oidx] = CUDART_INF_F;
      } else {
        out[oidx] = in[iidx];
      }
    }
  }
}

__global__ void load_2d_kernel_2(float *in, float *out, uint dim,
                                 uint batch_dim, uint wdim, uint real_wdim,
                                 uint real_wsize) {
  int offset = (blockIdx.y * batch_dim + blockIdx.x) * real_wsize;

  out += offset;

  int x = threadIdx.x;
  int y = threadIdx.y;

  int ax = blockIdx.x + threadIdx.x;
  int ay = blockIdx.y + threadIdx.y;

  // TODO: Find out which order of loops works best
  for (uint i = 0; i < real_wdim; i += SHARED_MEM_WINDOW) {
    for (uint j = 0; j < real_wdim; j += BLOCK_ROWS) {
      uint oidx = (y + j) * real_wdim + (x + i);
      uint iidx = (ay + j) * dim + (ax + i);
      // printf("o: %d i: %d\n", oidx, iidx);

      if ((x+i) >= wdim || (y+j) >= wdim) {
        out[oidx] = CUDART_INF_F;
      } else {
        out[oidx] = in[iidx];
      }
    }
  }
}

__global__ void load_2d_kernel_shmem(float *in, float *out, uint dim,
                                     uint batch_dim, uint wdim, uint real_wdim,
                                     uint real_wsize) {
  int offset = (blockIdx.y * batch_dim + blockIdx.x) * real_wsize;

  __shared__ float shmem[SHARED_MEM_WINDOW + 1][SHARED_MEM_WINDOW + 1];

  out += offset;

  int x = threadIdx.x;
  int y = threadIdx.y;

  int ax = blockIdx.x + threadIdx.x;
  int ay = blockIdx.y + threadIdx.y;

  // TODO: Find out which order of loops works best
  for (uint j = 0; j < real_wdim; j += BLOCK_ROWS) {
    for (uint i = 0; i < real_wdim; i += SHARED_MEM_WINDOW) {
      uint oidx = (y + j) * real_wdim + (x + i);
      uint iidx = (ay + j) * dim + (ax + i);

      if ((x+i) >= wdim || (y+j) >= wdim) {
        shmem[threadIdx.y][threadIdx.x] = CUDART_INF_F;
      } else {
        shmem[threadIdx.y][threadIdx.x] = in[iidx];
      }

      out[oidx] = shmem[threadIdx.y][threadIdx.x];
    }
  }
}

__global__ void load_2d_kernel_shmem_2(float *in, float *out, uint dim,
                                       uint batch_dim, uint wdim,
                                       uint real_wdim, uint real_wsize) {
  int offset = (blockIdx.y * batch_dim + blockIdx.x) * real_wsize;

  __shared__ float shmem[SHARED_MEM_WINDOW + 1][SHARED_MEM_WINDOW + 1];

  out += offset;

  int x = threadIdx.x;
  int y = threadIdx.y;

  int ax = blockIdx.x + threadIdx.x;
  int ay = blockIdx.y + threadIdx.y;

  // TODO: Find out which order of loops works best
  for (uint i = 0; i < real_wdim; i += SHARED_MEM_WINDOW) {
    for (uint j = 0; j < real_wdim; j += BLOCK_ROWS) {
      uint oidx = (y + j) * real_wdim + (x + i);
      uint iidx = (ay + j) * dim + (ax + i);

      if ((x+i) >= wdim || (y+j) >= wdim) {
        shmem[threadIdx.y][threadIdx.x] = CUDART_INF_F;
      } else {
        shmem[threadIdx.y][threadIdx.x] = in[iidx];
      }

      out[oidx] = shmem[threadIdx.y][threadIdx.x];
    }
  }
}

void create_assertion_values(float *exp_arr, uint batchdim, uint wdim, uint real_wdim,
                             uint mdim) {
  uint real_wsize = real_wdim * real_wdim;
  for (uint batchY = 0; batchY < batchdim; batchY++) {
    for (uint batchX = 0; batchX < batchdim; batchX++) {
      // uint idx = 0;
      for (uint cellY = 0; cellY < real_wdim; cellY++) {
        for (uint cellX = 0; cellX < real_wdim; cellX++) {

          uint idx = cellY * real_wdim + cellX;

          if (cellX >= wdim || cellY >= wdim) {
            exp_arr[idx] = std::numeric_limits<float>::infinity();
          } else {
            exp_arr[idx] = ((cellY + batchY) * mdim) + (cellX + batchX);
          }
          
        }
      }
      exp_arr += real_wsize;
    }
  }
}

void populate_matrix(float *mat, uint mdim) {
  uint idx = 0;
  for (uint cellY = 0; cellY < mdim; cellY++) {
    for (uint cellX = 0; cellX < mdim; cellX++) {
      mat[cellY * mdim + cellX] = (idx++) * 1.0f;
    }
  }
}

void driver(uint matrix_size, uint matrix_dim, uint window_arr_size,
            uint window_dim, uint batchdim, uint real_wsize, uint real_wdim,
            void kernel(float *, float *, uint, uint, uint, uint, uint)) {

  float *mat = (float *)malloc(sizeof(float) * matrix_size);
  float *windows = (float *)malloc(sizeof(float) * window_arr_size);
  float *exp_windows = (float *)malloc(sizeof(float) * window_arr_size);

  populate_matrix(mat, matrix_dim);
  create_assertion_values(exp_windows, batchdim, window_dim, real_wdim, matrix_dim);

  // printArray(windows, window_arr_size, real_wsize);
  #ifdef DEBUG
  printf(COLOR_TEXT("Expected:\n", C_YELLOW));
  printArray(exp_windows, window_arr_size, real_wsize);
  #endif

  float *mat_d;
  float *windows_d;

  checkCudaErrors(cudaMalloc(&mat_d, sizeof(float) * matrix_size));
  checkCudaErrors(cudaMalloc(&windows_d, sizeof(float) * window_arr_size));

  checkCudaErrors(cudaMemcpy(mat_d, mat, sizeof(float) * matrix_size,
                             cudaMemcpyHostToDevice));
  // checkCudaErrors(cudaMemcpy(windows_d, windows,
  //                            sizeof(float) * window_arr_size,
  //                            cudaMemcpyHostToDevice));

  uint batchX = 0;
  uint batchY = 0;

  dim3 blocks(batchdim, batchdim);
  dim3 threads(SHARED_MEM_WINDOW, BLOCK_ROWS);
  // dim3 blocks(1, 1);
  // dim3 threads(1, BLOCK_ROWS);

  printf("blocks: %d %d threads: %d %d\n", blocks.x, blocks.y, threads.x,
         threads.y);

  cudaEvent_t start, stop;

  checkCudaErrors(cudaEventCreate(&start));
  checkCudaErrors(cudaEventCreate(&stop));

  checkCudaErrors(cudaEventRecord(start));

  kernel<<<blocks, threads>>>(mat_d, windows_d, matrix_dim, batchdim,
                              window_dim, real_wdim, real_wsize);

  checkCudaErrors(cudaMemcpy(windows, windows_d,
                             sizeof(float) * window_arr_size,
                             cudaMemcpyDeviceToHost));

  float elapsedTime;

  checkCudaErrors(cudaEventRecord(stop));
  checkCudaErrors(cudaEventSynchronize(stop));
  checkCudaErrors(cudaEventElapsedTime(&elapsedTime, start, stop));

  checkCudaErrors(cudaEventDestroy(start));
  checkCudaErrors(cudaEventDestroy(stop));

#ifdef DEBUG
  printf(COLOR_TEXT("Actual:\n", C_YELLOW));
  printArray(windows, window_arr_size, real_wsize);
#endif

#ifdef ASSERT
  for (uint row = 0; row < batchdim * batchdim; row++) {
    uint offset = row * real_wsize;
    assertArray("windows row " + std::to_string(row), exp_windows + offset,
                windows + offset, real_wsize);
  }
#endif

  printf("Elements: " BLUE_TEXT(" %d ") " Time: " BLUE_TEXT(
             "%fms") " Throughput: " BLUE_TEXT("%f MElem/sec") " BandWidth:"
                                                               " " BLUE_TEXT(
                                                                   "%fGB"
                                                                   "/s") "\n",
         window_arr_size, elapsedTime,
         (1e-6 * window_arr_size) / (1e-3 * elapsedTime),
         ((2 * window_arr_size * 4 * 1e-6) / elapsedTime));

  checkCudaErrors(cudaFree(mat_d));
  checkCudaErrors(cudaFree(windows_d));

  free(mat);
  free(windows);
}

// __global__ void load_kernel(float *in, float *out, uint dim, uint wdim, uint
// real_wdim) {
//   in += (blockIdx.y * wdim + blockIdx.x) *
// }

void check(bool condition, std::string file, int const line) {
  if (!condition) {
    fprintf(stderr, RED_TEXT("Runtime Error at %s:%d \n"), file.c_str(), line);
    exit(EXIT_FAILURE);
  }
}

#define checkErrors(val) check(val, __FILE__, __LINE__)

int main(int argc, char **argv) {
  // uint N = 4;
  uint matrix_dim = 8;
  uint window_dim = 4;
  uint batchdim = 2;
  if (argc > 3) {
    matrix_dim = atoi(argv[1]);
    window_dim = atoi(argv[2]);
    batchdim = atoi(argv[3]);
  }

  
  uint batchSize = batchdim * batchdim;

  uint window_size = window_dim * window_dim;
  uint real_wdim = highest_power_2(window_dim);
  uint real_wsize = real_wdim * real_wdim;

  uint resDim = matrix_dim - real_wdim;
  uint noOfWindows = resDim * resDim;

  printf(BLUE_TEXT("matrix dim: %d window_dim: %d batchdim: %d resdim: %d\n"),
         matrix_dim, window_dim, batchdim, resDim);

  checkErrors(matrix_dim > window_dim);
  checkErrors(real_wdim >= SHARED_MEM_WINDOW);
  checkErrors(noOfWindows >= batchSize);
  checkErrors(resDim % batchdim == 0);

  printf("Normal approach\n");

  

  uint window_arr_size = real_wsize * batchSize;

  uint matrix_size = matrix_dim * matrix_dim;

  printf("wsize: %d rwdim: %d rwsize %d msize: %d real_warr_size: %d\n",
         window_size, real_wdim, real_wsize, matrix_size, window_arr_size);

  printf(COLOR_TEXT("Normal load\n", C_YELLOW));
  driver(matrix_size, matrix_dim, window_arr_size, window_dim, batchdim,
         real_wsize, real_wdim, load_2d_kernel);

  printf(COLOR_TEXT("Shared mem\n", C_YELLOW));
  driver(matrix_size, matrix_dim, window_arr_size, window_dim, batchdim,
         real_wsize, real_wdim, load_2d_kernel_shmem);

  printf(COLOR_TEXT("Normal load order 2\n", C_YELLOW));
  driver(matrix_size, matrix_dim, window_arr_size, window_dim, batchdim,
         real_wsize, real_wdim, load_2d_kernel_2);
  
  printf(COLOR_TEXT("Shmem order 2\n", C_YELLOW));
  driver(matrix_size, matrix_dim, window_arr_size, window_dim, batchdim,
         real_wsize, real_wdim, load_2d_kernel_shmem_2);
}
