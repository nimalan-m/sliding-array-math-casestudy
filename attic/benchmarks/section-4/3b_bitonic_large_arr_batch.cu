// This can also be immutable if the outputs median and madfm are written out in chunks
#include <assert.h>
#include <cooperative_groups.h>
#include <cstdlib>
#include <limits>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "colors.h"

#define SHMEM_WINDOW_SIZE 32U
#define BLOCK_ROWS (SHMEM_WINDOW_SIZE >> 2)
#define SHARED_MEMORY_SIZE 2048U
#define SHMEM_HALF (SHARED_MEMORY_SIZE >> 1)

// #define SHARED_MEMORY_SIZE 1024U
// #define WARP (SHARED_MEMORY_SIZE >> 1)

#define UMUL(a, b) __umul24((a), (b))
#define UMAD(a, b, c) (UMUL((a), (b)) + (c))

#define BATCH_SIZE 128U
#define NUM_REPS 1

inline void checkGPU(cudaError_t result, std::string file, int const line) {
  if (result != cudaSuccess) {
    fprintf(stderr, "Cuda Runtime Error at %s:%d : %s\n", file.c_str(), line,
            cudaGetErrorString(result));
    exit(EXIT_FAILURE);
  }
}

#define checkCudaErrors(val) checkGPU(val, __FILE__, __LINE__)

void printArray(float *arr, uint arrSize, uint wSize) {
  for (uint i = 0; i < arrSize; i++) {
    printf("%.2f ", arr[i]);
    if ((i + 1) % wSize == 0) {
      printf("\n");
    }
  }
}

void printArrayMedian(float *arr, float *median, uint arrSize, uint wSize) {
  int mIdx = 0;

  int rows = arrSize / wSize;
  for (uint row = 0; row < rows; row++) {
    float prev = arr[row * wSize];
    printf("%.0f ", prev);
    for (uint cell = 1; cell < wSize; cell++) {
      float val = arr[row * wSize + cell];
      if (prev > val) {
        printf(C_BGRED);
      }
      prev = val;
      printf("%.0f ", val);
    }
    printf(C_RESET);
    printf("\t median: " GREEN_TEXT("%.1f\n"), median[mIdx++]);
  }
}

#define EPHISILON 0.001f

void assertArray(std::string str, float *exp, float *act, uint N) {
  int assert_failure = 0;
  for (int i = 0; i < N; i++) {
    if (abs(exp[i] - act[i]) > EPHISILON) {
      printf(
          "Assertion failure: %s " RED_TEXT(" widow: %d exp: %f actual: %f\n"),
          str.c_str(), i, exp[i], act[i]);
      assert_failure = 1;
    }
  }
  if (!assert_failure)
    printf(GREEN_TEXT("Assertion passed %s\n"), str.c_str());
}

__device__ inline void Comparator(float &A, float &B, uint dir) {
  float t;

  if ((A > B) == dir) {
    t = A;
    A = B;
    B = t;
  }
}

namespace cg = cooperative_groups;

__global__ void bitonicSortKernel(float *in, float *out, uint real_wsize,
                                  uint wsize, uint dir, uint batch, uint batchX,
                                  uint batchY, uint batchSize2D,
                                  uint noOfBatches) {

  cg::thread_block cta = cg::this_thread_block();
  __shared__ float shmem[SHARED_MEMORY_SIZE];

  in += blockIdx.x * SHARED_MEMORY_SIZE + threadIdx.x;

  shmem[threadIdx.x] = in[0];
  shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)] = in[(SHARED_MEMORY_SIZE / 2)];

  // Bitonic merge
  for (uint size = 2; size < real_wsize; size <<= 1) {
    // Bitonic direction
    uint ddd = dir ^ ((threadIdx.x & (size / 2)) != 0);

    for (uint stride = size / 2; stride > 0; stride >>= 1) {
      cg::sync(cta);
      uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
      Comparator(shmem[pos + 0], shmem[pos + stride], ddd);
    }
  }

  // ddd == dir for the last bitonic merge step
  {
    for (uint stride = real_wsize / 2; stride > 0; stride >>= 1) {
      cg::sync(cta);
      uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
      Comparator(shmem[pos + 0], shmem[pos + stride], dir);
    }
  }

  cg::sync(cta);

  uint mid = wsize / 2;
  float median = shmem[mid];
  median += (wsize & 1) ? shmem[mid] : shmem[mid - 1];
  median /= 2;

  in[0] = fabs(median - shmem[threadIdx.x]);
  in[(SHARED_MEMORY_SIZE / 2)] =
      fabs(median - shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)]);

  if (threadIdx.x == 0) {
    // TODO: Batching logic
    out[blockIdx.x] = median;
  }
}

__global__ void bitonicSortKernelLarge(float *in, uint dir, uint wsize,
                                       uint batch, uint batchX, uint batchY,
                                       uint batchSize2D, uint noOfBatches) {

  cg::thread_block cta = cg::this_thread_block();
  __shared__ float shmem[SHARED_MEMORY_SIZE];

  in += blockIdx.x * SHARED_MEMORY_SIZE + threadIdx.x;

  shmem[threadIdx.x] = in[0];
  shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)] = in[(SHARED_MEMORY_SIZE / 2)];

  // Bitonic merge
  for (uint size = 2; size < SHARED_MEMORY_SIZE; size <<= 1) {
    uint ddd = (threadIdx.x & (size / 2)) != 0;

    for (uint stride = size / 2; stride > 0; stride >>= 1) {
      cg::sync(cta);
      uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
      Comparator(shmem[pos + 0], shmem[pos + stride], ddd);
    }
  }

  uint ddd = blockIdx.x & 1;
  {
    for (uint stride = SHARED_MEMORY_SIZE / 2; stride > 0; stride >>= 1) {
      cg::sync(cta);
      uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
      Comparator(shmem[pos + 0], shmem[pos + stride], ddd);
    }
  }

  cg::sync(cta);

  in[0] = shmem[threadIdx.x];
  in[(SHARED_MEMORY_SIZE / 2)] = shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)];
}

__global__ void bitonicMergeGlobal(float *in, uint arrayLen, uint size,
                                   uint stride, uint dir) {
  uint global_comparatorI = blockIdx.x * blockDim.x + threadIdx.x;
  uint comparatorI = global_comparatorI & (arrayLen / 2 - 1);

  // Bitonic merge
  uint ddd = dir ^ ((comparatorI & (size / 2)) != 0);
  uint pos = 2 * global_comparatorI - (global_comparatorI & (stride - 1));

  float A = in[pos];
  float B = in[pos + stride];

  Comparator(A, B, ddd);

  in[pos] = A;
  in[pos + stride] = B;
}

__global__ void bitonicMergeShared(float *in, uint arrayLength, uint size,
                                   uint dir) {
  cg::thread_block cta = cg::this_thread_block();
  __shared__ float shmem[SHARED_MEMORY_SIZE];

  in += blockIdx.x * SHARED_MEMORY_SIZE + threadIdx.x;

  shmem[threadIdx.x] = in[0];
  shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)] = in[SHARED_MEMORY_SIZE / 2];

  uint comparatorI =
      UMAD(blockIdx.x, blockDim.x, threadIdx.x) & ((arrayLength / 2) - 1);
  uint ddd = dir ^ ((comparatorI & (size / 2)) != 0);

  for (uint stride = SHARED_MEMORY_SIZE / 2; stride > 0; stride >>= 1) {
    cg::sync(cta);
    uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
    Comparator(shmem[pos], shmem[pos + stride], ddd);
  }

  cg::sync(cta);
  in[0] = shmem[threadIdx.x];
  in[SHARED_MEMORY_SIZE / 2] = shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)];
}

__global__ void extract_median_transform(float *in, float *out, uint real_wsize,
                                         uint wsize) {

  in += blockIdx.x * real_wsize;

  __shared__ float shmem[SHARED_MEMORY_SIZE];

  uint mid = wsize / 2;
  float median = in[mid];
  median += (wsize & 1) ? in[mid] : in[mid - 1];
  median /= 2;

  in +=  threadIdx.x;

  for (int stride = 0; stride < real_wsize; stride += SHARED_MEMORY_SIZE) {
    shmem[threadIdx.x] = in[stride];
    shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)] =
        in[stride + (SHARED_MEMORY_SIZE / 2)];

    in[stride] = fabs(median - shmem[threadIdx.x]);
    in[stride + (SHARED_MEMORY_SIZE / 2)] =
        fabs(median - shmem[threadIdx.x + (SHARED_MEMORY_SIZE / 2)]);
  }

  if (threadIdx.x == 0) {
    out[blockIdx.x] = median;
  }
}

unsigned int highest_power_2(unsigned int n) {
  unsigned int t = n - 1;
  t |= t >> 1;
  t |= t >> 2;
  t |= t >> 4;
  t |= t >> 8;
  t |= t >> 16;
  return t + 1;
}

void bitonicSortBatches(float *median, float *madfm, float *arr, uint arrSize,
                        uint real_wsize, uint wsize, uint noOfWindows) {
  uint dir = 1;

  float *arr_d;
  float *median_d;
  float *madfm_d;

  checkCudaErrors(cudaMalloc(&arr_d, arrSize * sizeof(float)));
  checkCudaErrors(cudaMalloc(&median_d, noOfWindows * sizeof(float)));
  checkCudaErrors(cudaMalloc(&madfm_d, noOfWindows * sizeof(float)));

  checkCudaErrors(cudaMemcpyAsync(arr_d, arr, arrSize * sizeof(float),
                                  cudaMemcpyHostToDevice, 0));

  cudaEvent_t start, stop;

  checkCudaErrors(cudaEventCreate(&start));
  checkCudaErrors(cudaEventCreate(&stop));

  checkCudaErrors(cudaEventRecord(start));

#ifdef DEBUG
  printf("Multi kernel sort arrsize: %d r_wsize: %d wsize: %d sh_size: %d\n",
         arrSize, real_wsize, wsize, SHARED_MEMORY_SIZE);
#endif

  // TODO: Introduce batching
  uint batchSize = noOfWindows;
  uint threadCount = SHARED_MEMORY_SIZE / 2;

  printf("blocks: %d threads: %d\n", batchSize, threadCount);

  // Median
  if (real_wsize <= SHARED_MEMORY_SIZE) {
    // FIXME: There is an assertion missing, N has to be divisible by
    // SHARED_SIZE_LIMIT assert(N % SHARED_SIZE_LIMIT == 0);
    bitonicSortKernel<<<batchSize, threadCount>>>(arr_d, median_d, real_wsize,
                                                  wsize, dir, 0, 0, 0, 0, 0);
  } else {
    // TODO: Test this
    uint blockCount = arrSize / SHARED_MEMORY_SIZE;

    bitonicSortKernelLarge<<<blockCount, threadCount>>>(arr_d, dir, 0, 0, 0, 0,
                                                        0, 0);

    for (uint size = 2 * SHARED_MEMORY_SIZE; size <= real_wsize; size <<= 1) {
      for (unsigned stride = size / 2; stride > 0; stride >>= 1) {
        if (stride >= SHARED_MEMORY_SIZE) {
          bitonicMergeGlobal<<<arrSize / SHMEM_HALF, (SHMEM_HALF / 2)>>>(
              arr_d, real_wsize, size, stride, dir);
        } else {
          bitonicMergeShared<<<blockCount, threadCount>>>(arr_d, real_wsize,
                                                          size, dir);
          break;
        }
      }
    }
#ifdef DEBUG
    printf("Before sub median\n");
    checkCudaErrors(cudaMemcpy(arr, arr_d, arrSize * sizeof(float),
                               cudaMemcpyDeviceToHost));

    printArray(arr, arrSize, real_wsize);
#endif

    extract_median_transform<<<batchSize, threadCount>>>(arr_d, median_d,
                                                         real_wsize, wsize);
  }

#ifdef DEBUG
  printf("\n\nAfter sub median\n");
  checkCudaErrors(
      cudaMemcpy(arr, arr_d, arrSize * sizeof(float), cudaMemcpyDeviceToHost));

  printArray(arr, arrSize, real_wsize);
#endif

  // Madfm
  if (real_wsize <= SHARED_MEMORY_SIZE) {
    bitonicSortKernel<<<batchSize, threadCount>>>(arr_d, madfm_d, real_wsize,
                                                  wsize, dir, 0, 0, 0, 0, 0);
  } else {
    uint blockCount = arrSize / SHARED_MEMORY_SIZE;

    bitonicSortKernelLarge<<<blockCount, threadCount>>>(arr_d, dir, 0, 0, 0, 0,
                                                        0, 0);

    for (uint size = 2 * SHARED_MEMORY_SIZE; size <= real_wsize; size <<= 1) {
      for (unsigned stride = size / 2; stride > 0; stride >>= 1) {
        if (stride >= SHARED_MEMORY_SIZE) {
          bitonicMergeGlobal<<<arrSize / SHMEM_HALF, (SHMEM_HALF / 2)>>>(
              arr_d, real_wsize, size, stride, dir);
        } else {
          bitonicMergeShared<<<blockCount, threadCount>>>(arr_d, real_wsize,
                                                          size, dir);
          break;
        }
      }
    }

#ifdef DEBUG
    printf("\n\nFinal array");
    checkCudaErrors(cudaMemcpy(arr, arr_d, arrSize * sizeof(float),
                               cudaMemcpyDeviceToHost));

    printArray(arr, arrSize, real_wsize);
#endif
    extract_median_transform<<<batchSize, threadCount>>>(arr_d, madfm_d,
                                                         real_wsize, wsize);
  }

  // checkCudaErrors(
  //     cudaMemcpy(arr, arr_d, arrSize * sizeof(float),
  //     cudaMemcpyDeviceToHost));

  // printArray(arr, arrSize, real_wsize);

  checkCudaErrors(cudaMemcpyAsync(median, median_d, noOfWindows * sizeof(float),
                                  cudaMemcpyDeviceToHost));
  checkCudaErrors(cudaMemcpyAsync(madfm, madfm_d, noOfWindows * sizeof(float),
                                  cudaMemcpyDeviceToHost));

  checkCudaErrors(cudaEventRecord(stop));
  checkCudaErrors(cudaEventSynchronize(stop));

  float elapsedTime;
  checkCudaErrors(cudaEventElapsedTime(&elapsedTime, start, stop));
  printf("Elements: " C_GREEN "%d" C_RESET " Elapsed " C_GREEN "%.3fms" C_RESET
         " throughput: " C_GREEN "%.3f" C_RESET " MElements/s \n",
         arrSize, elapsedTime, arrSize * 1e-6 / elapsedTime);

  checkCudaErrors(cudaEventDestroy(start));
  checkCudaErrors(cudaEventDestroy(stop));

  checkCudaErrors(cudaFree(arr_d));
  checkCudaErrors(cudaFree(median_d));
  checkCudaErrors(cudaFree(madfm_d));
}

static int compfloat(const void *p1, const void *p2) {
  float *v1 = (float *)p1;
  float *v2 = (float *)p2;
  if (*v1 < *v2)
    return -1;
  if (*v1 > *v2)
    return 1;
  return 0;
}

void calculate_assertion_values(int N, float *expect_arr, float *expect_median,
                                float *expect_madfm, uint real_wsize,
                                uint wsize) {
  // Calculate Median
  for (uint window = 0; window < N; window++) {
    float *win_ptr = expect_arr + (window * real_wsize);
    std::qsort(win_ptr, real_wsize, sizeof(float), compfloat);
    int mid = wsize / 2;
    if (wsize & 1) {
      expect_median[window] = win_ptr[mid];
    } else {
      expect_median[window] = (win_ptr[mid] + win_ptr[mid - 1]) / 2;
    }
  }

  // Transform the array for MADFM
  for (uint window = 0; window < N; window++) {

    float *win_ptr = expect_arr + (window * real_wsize);

#ifdef DEBUG
    for (int cell = 0; cell < real_wsize; cell++) {
      printf("%.2f ", win_ptr[cell]);
    }
    printf("\t median: %f", expect_median[window]);
    printf("\n");
#endif

    for (int cell = 0; cell < real_wsize; cell++) {
      win_ptr[cell] = fabs(expect_median[window] - win_ptr[cell]);
    }
  }
#ifdef DEBUG
  printf("\n\n");
#endif

#ifdef DEBUG
  for (uint window = 0; window < N; window++) {
    float *win_ptr = expect_arr + (window * real_wsize);

    for (int cell = 0; cell < real_wsize; cell++) {
      printf("%.2f ", win_ptr[cell]);
    }
    printf("\n");
  }
  printf("\n\n");
#endif

  // Calculate MADFM
  for (uint window = 0; window < N; window++) {
    float *win_ptr = expect_arr + (window * real_wsize);

    std::qsort(win_ptr, real_wsize, sizeof(float), compfloat);
    int mid = wsize / 2;
    if (wsize & 1) {
      expect_madfm[window] = win_ptr[mid];
    } else {
      expect_madfm[window] = (win_ptr[mid] + win_ptr[mid - 1]) / 2;
    }

#ifdef DEBUG
    for (int cell = 0; cell < real_wsize; cell++) {
      printf("%.2f ", win_ptr[cell]);
    }
    printf("\t madfm: %f", expect_madfm[window]);
    printf("\n");
#endif
  }
}

int main(int argc, char **argv) {

  uint N = 64;
  int wdim = 32;

  if (argc > 1) {
    N = atoi(argv[1]);
    wdim = atoi(argv[2]);
  }

  const uint wsize = wdim * wdim;
  uint real_wsize = highest_power_2(wsize);

  int arrSize = real_wsize * N;

  float *arr = (float *)malloc(sizeof(float) * arrSize);
  float *median = (float *)malloc(sizeof(float) * N);
  float *madfm = (float *)malloc(sizeof(float) * N);

  float *expect_arr = (float *)malloc(sizeof(float) * arrSize);
  float *expect_median = (float *)malloc(sizeof(float) * N);
  float *expect_madfm = (float *)malloc(sizeof(float) * N);

  float range = 10.0f;
  int idx = 0;
  for (uint window = 0; window < N; window++) {
    for (uint cell = 0; cell < real_wsize; cell++) {
      if (cell < wsize) {
        auto val = (double)std::rand() / (double)(RAND_MAX / range);
        arr[idx] = val;
        expect_arr[idx] = val;
      } else {
        arr[idx] = std::numeric_limits<float>::infinity();
        expect_arr[idx] = arr[idx];
      }
      idx++;
    }
  }

  calculate_assertion_values(N, expect_arr, expect_median, expect_madfm,
                             real_wsize, wsize);

#ifdef DEBUG
  printf("Input:\n");
  printArray(arr, arrSize, real_wsize);
#endif

  for (int i = 0; i < N; i++) {
    median[i] = 0.0f;
    madfm[i] = 0.0f;
  }

  bitonicSortBatches(median, madfm, arr, arrSize, real_wsize, wsize, N);

#ifdef DEBUG
  printf("Output:\n");
  printArrayMedian(arr, median, arrSize, real_wsize);
#endif

  assertArray("median", median, expect_median, N);
  assertArray("madfm", madfm, expect_madfm, N);

  free(madfm);
  free(median);
  free(arr);
}
