# Network for Scan Statistics GPU implementation

This project is a GPU accelerated version of Casacore's `slidingArrayMath` for median and MADFM. This has been developed to accelerate the Variable Threshold step in the ASKAP source finder [Selavy](https://bitbucket.csiro.au/projects/ASKAPSDP/repos/askap-analysis/browse). This implements a Network for Scan Statistic to achieve acceleration.

This project can also fall back to a OpenMP based multithreaded approach for environments without GPU; the OpenMP version does not implement network for scan statistics, it optimises the `slidingArrayMath` from Casacore using OpenMP for multithreading.

The source code of the algorithm can be found in the **bitonic-median** folder. The CMake file has been made configurable to work with Nvidia and AMD GPUs.

**Dependencies:**

1. Cuda
2. ROCM (for AMD GPUs)
3. Casacore
4. CMake

> **Note:** Casacore is available from apt in the Ubuntu distro, for other platforms installing from source is needed. Refer to [building Casacore from scratch](./Selavy.md#dependencies-for-selavy) or the [casacore repo](https://github.com/casacore/casacore)

### General Information

1. For using this as a external library refer to [usage section](#usage-as-an-external-project-in-cmake)
2. For running the benchmarks and creating the visualization refer to [benchmark section](#benchmarks)
3. For information about the API signatures in the library and it's structure refer to [project information section](#project-information)
4. For installing this in the ASKAP Source Finder Selavy and running Selavy with the optimization enabled refer to [Selavy.md](./Selavy.md)

## Usage as an external project in CMake

This project can be installed as follows, add as an external project with `ExternalProject_Add`, then add the library to the link libraries.

```cmake
option (SNR_OPT "Optimization technique (CUDA/HIP/OMP) for SNR calculation" "OMP")

ExternalProject_Add(bitonic-median
    GIT_REPOSITORY https://gitlab.com/nimalan-m/sliding-array-math-casestudy.git
    SOURCE_DIR ${EXTERNAL_INSTALL_LOCATION}/bitonic-median
    CMAKE_ARGS -DSNR_OPT=${SNR_OPT} -DCMAKE_INSTALL_PREFIX=${EXTERNAL_INSTALL_LOCATION}
    SOURCE_SUBDIR bitonic-median
)

# 
target_link_libraries(analysis PUBLIC
    # Other dependencies
    SlidingMedianCASA # Accelerated library for slidingArrayMath
)
```

## Benchmarks

For building and running the benchmark of this project there is a utility script `coach` inside the **script** folder. Running the benchmarks needs a workstation class machine with a decent Nvidia or AMD GPU.

### Building benchmark with Coach

```sh
cd bitonic-median
./scripts/coach build         # Build for OpenMP (fallback)
./scripts/coach build -o CUDA # Build for a Nvidia GPU
./scripts/coach build -o HIP  # Build for a AMD GPU
```

### Running the benchmark

The primary benchmark is the executable `tbitonicMedianCASA`. Examples of using the benchmark are below

```sh
# Default benchmark
# Calculate moving window median and MADFM for 1024x1024 matrix with window size 101x101
./build/bench/tbitonicMedianCASA

# Run the benchmark against the Casacore baseline using a Casacore MaskedArray and assert the results
./build/bench/tbitonicMedianCASA --masked --assertion

# Calculate moving window median and MADFM for 2048x2048 matrix with window size 255x255 (we pass half box size 127 as input, window size is 2n+1)
./build/bench/tbitonicMedianCASA --masked --assertion --sizex 2048 --sizey 2048 --masked --wsize 127

```

> Note: The baseline is computationally expensive. For a 1024x1024 matrix (75% valid points) with a window size of 101x101, on a Intel i7-9750H the baseline takes 291s.

**Benchmarking with Coach**

The `coach` script can also run the benchmark `tbitonicMedianCASA` and accepts all the parameters that the benchmark takes.

```sh
# Default benchmark
# Calculate moving window median and MADFM for 1024x1024 matrix with window size 101x101
./scripts/coach bench

# Run the benchmark against the Casacore baseline using a Casacore MaskedArray and assert the results
./scripts/coach bench -- --masked --assertion

# Calculate moving window median and MADFM for 2048x2048 matrix with window size 255x255 (we pass half box size 127 as input, window size is 2n+1)
./scripts/coach bench -- --masked --assertion --sizex 2048 --sizey 2048 --masked --wsize 127
```

### Benchmark and Visualization

For a complete comparison of different scenarios the benchmark `tbitonicMedianCASA` will have to be run multiple times. For convenience there are scripts which can run the benchmark for various scenarios and generate a graph.

> Note: The baseline benchmark is computationally expensive running all the benchmark suits will take approx 2 hours

```sh
cd bitonic-median

# Run the baseline benchmark
./scripts/bench_baseline.sh > visuals/singlemachine/baseline_run.log

# Run the OpenMP benchmark
./scripts/bench_omp.sh > visuals/singlemachine/omp_run.log

# Run one of the GPU benchmarks
# Either the Cuda benchmark if you have a Nvidia GPU
./scripts/bench_cuda.sh > visuals/singlemachine/gpu_run.log

# or the HIP benchmark if you have a Nvidia GPU
./scripts/bench_hip.sh > visuals/singlemachine/gpu_run.log

# Run the thread scaling benchmark
./scripts/bench_thread_scaling.sh > visuals/singlemachine/omp_thread_scaling_run.log

# Run the window scaling benchmark for GPU
./scripts/bench_window_scaling.sh > visuals/singlemachine/gpu_window_2048.log
```

**About each benchmark**

1. **bench_baseline:** Runs the baseline (Casacore) approach for different matrix sizes
2. **bench_omp:** Runs the OpenMP optimization for different matrix sizes
3. **bench_<cuda/hip>:** Runs the GPU optimization for different matrix sizes
4. **bench_thread_scaling:** Runs the OpenMP optimization for a single matrix size and different number of threads
5. **bench_window_scaling:** Runs the GPU optimization for a single matrix size and different window sizes

> Note: The benchmark to test the thread scaling `bench_thread_scaling.sh` requires a machine that has atleast 64 cores.

**Visualization**

The scripts to generate graphs combine the outputs from the previously descriped benchmarks and create graphs.

Three graphs are created:

1. **graphs/performance/performance_overall.pdf** - Line plot with comparison of time taken between the baseline (Casacore), multithreaded OpenMP optimization and the GPU accelerated network for scan statistics optimization in log scale
2. **graphs/performance/thread_scaling.pdf** - Line plot with comparison of time taken by multithreaded OpenMP optimization with the number of threads used
3. **graphs/performance/window_scaling.pdf** - Line plot with comparison of time taken by GPU optimization with the window size

> Note: The script will work if some of the log files are missing and will generate a empty graph for the missing data

For generating the visualization the following dependencies are needed; `gnuplot`, `sqlite3`(for processing results)

```sh
cd bitonic-median/visuals/singlemachine

./process_stats.sh
```

## Project Information

### API Signatures GPU implementation

**ArrayMathOpt.h**

```cpp
namespace arraymath {
void slidingArrayMedianMADFM(casacore::Array<float> &median,
                         casacore::Array<float> &madfm,
                         casacore::Array<float> &array,
                         const casacore::IPosition &halfBoxSize,
                         const unsigned int block_size);

void slidingArrayMedianMADFM(casacore::Array<float> &median,
                         casacore::Array<float> &madfm,
                         casacore::MaskedArray<float> &array,
                         const casacore::IPosition &halfBoxSize,
                         const unsigned int block_size);
void slidingMedianMadfmOptimized(float *median, float *rms, float *arr,
                            unsigned int arrdimx, unsigned int arrdimy,
                            unsigned int wdim, unsigned int block_dim)
} // namespace arraymath

```

### Folder Structure

```
bitonic-median
|
|---- include
|       |
|       |--- ArrayMathOpt.h                 - Header containing optimised API
|
|---- casa_array_wrapper.cc                 - C++ wrapper to give Casacore API
|
|---- array_math_omp.cc                     - OpenMP implementation
|---- array_math_cuda.cu                    - CUDA/HIP implementation
|
|---- compute_helpers.cuh                   - Helpers, data structures, containers
|---- transform_kernels.cuh                 - Kernels for transform logic in scan network
|---- sort_kernels.cuh                      - Kernels for Bitonic sort
|
|---- bench
|      |------ tbitonicMedianCASA.cc        - Primary test for overall usage
|      |------ tbitonicMedianCASAMPI.cc     - MPI wrapper around tbitonicMedianCASA.cc
|
|---- tests
|      |-------- twsize_kernel.cu           - Test for window size calculating transform kernel
|      |-------- tsort_kernel.cu            - Test for piecewise sort kernel
|      |-------- tload_kernel.cu            - Test for window transform kernel
|      |-------- tdimensionality.cu         - Test for container
```
