# Usage in Selavy

The fork of [askap-analysis](https://gitlab.com/nimalan-m/askap-analysis/-/tree/feature/gpu-varthres) contains changes in Variable Thresholding using the GPU Network for Scan Statistics approach. Running Selavy would need MPI installed.

## Dependencies for Selavy

1. [Casacore](https://github.com/casacore/casacore)

> **Note:** In the Ubuntu disto Casacore can be installed from apt, we have worked and tested the Selavy runs with Casacore built from scratch

**Dependencies needed for building Casacore from scratch:**

```
sudo apt-get install gcc cmake g++ wcslib-dev libcfitsio-dev gfortran openmpi-bin \
  libreadline-dev flex bison libhdf5-dev libopenblas-dev fftw-dev libfftw3-dev \
  libboost-dev libgsl-dev libboost-filesystem-dev libboost-program-options-dev subversion \
  libboost-thread-dev liblog4cxx-dev libcppunit-dev libxerces-c-dev libboost-regex-dev
```

**Building Casacore from scratch:**

```sh
git clone https://github.com/casacore/casacore
cd casacore
git checkout v3.5.0

cmake -B build -S . -DCMAKE_INSTALL_PREFIX=<desired-path> -DBUILD_PYTHON=OFF -DBUILD_PYTHON3=OFF -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF
cmake --build build -j 4

cd build
make install

```

2. [Casarest](https://github.com/casacore/casarest)

```sh
git clone https://github.com/casacore/casarest
cd casarest
git checkout v1.8.1

cmake -B build -S . -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF -DCMAKE_INSTALL_PREFIX=<desired-path>
cmake --build build -j 4
cd build
make install
```

## Installing Selavy

```sh

git clone https://gitlab.com/nimalan-m/askap-analysis
cd askap-analysis
git checkout feature/gpu-varthres

# For Nvidia GPUs
cmake -B build -S . -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=<desired-path> \
    -DSNR_OPT=CUDA \
    -DASKAPSDP_GIT_URL=https://bitbucket.csiro.au/scm/askapsdp \
    -DBUILD_TESTING=OFF

# For AMD GPUs
cmake -B build -S . -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=<desired-path> \
    -DSNR_OPT=HIP \
    -DASKAPSDP_GIT_URL=https://bitbucket.csiro.au/scm/askapsdp \
    -DBUILD_TESTING=OFF

# Multithreading optimzation for non GPU environments
cmake -B build -S . -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=<desired-path> \
    -DSNR_OPT=OMP \
    -DASKAPSDP_GIT_URL=https://bitbucket.csiro.au/scm/askapsdp \
    -DBUILD_TESTING=OFF

# If there are issues finding casacore and casarest
cmake -B build -S . -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=<desired-path> \
    -DSNR_OPT=CUDA \
    -DASKAPSDP_GIT_URL=https://bitbucket.csiro.au/scm/askapsdp \
    -DBUILD_TESTING=OFF \
    -DCASACORE_ROOT_DIR=<path-to-casacore> \
    -DCOMPONENTS_LIBRARY=<path-to-casarest>/lib/libcasa_components.so \
    -DCOMPONENTS_INCLUDE_DIR=<path-to-casarest>/include

cmake --build build -j 4
cd build
make install
```

## Datasets

These changes have been tested using the data of observations SB33442, SB40625, SB40905 from the ASKAP Pilot Survey for EMU. Data products are publically available from the CASDA data archive. Downloading the data products will require an [OPAL account](https://opal.atnf.csiro.au/home/), which can be registered at https://opal.atnf.csiro.au/register

[ASKAP Data Products for Project AS101 (ASKAP Pilot Survey for EMU)](https://data.csiro.au/collection/csiro:40939)

1. [Search query for SB33442](https://data.csiro.au/domain/casdaObservation/results?dataProducts=%5B%7B%22dataProduct%22%3A%22CATALOGUE%22%7D,%7B%22dataProduct%22%3A%22IMAGE_CUBE%22%7D,%7B%22dataProduct%22%3A%22IMAGE_CUBE_ANCILLARY%22%7D,%7B%22dataProduct%22%3A%22SPECTRUM%22%7D,%7B%22dataProduct%22%3A%22MEASUREMENT_SET%22%7D%5D&facets=%5B%5D&showRejected=false&releasedFilter=released&includeCommensalProjects=false&showFacets=true&project=AS101%20-%20ASKAP%20Pilot%20Survey%20for%20EMU&schedulingBlockId=33442)

**Data products needed:**
  - image.i.EMU_0208+00B.SB33442.cont.taylor.0.restored.conv.fits
  - image.i.EMU_0208+00B.SB33442.cont.taylor.1.restored.conv.fits
  - weights.i.EMU_0208+00B.SB33442.cont.taylor.0.fits
  - weights.i.EMU_0208+00B.SB33442.cont.taylor.1.fits
2. [Search query for SB40905](https://data.csiro.au/domain/casdaObservation/results?dataProducts=%5B%7B%22dataProduct%22%3A%22CATALOGUE%22%7D,%7B%22dataProduct%22%3A%22IMAGE_CUBE%22%7D,%7B%22dataProduct%22%3A%22IMAGE_CUBE_ANCILLARY%22%7D,%7B%22dataProduct%22%3A%22SPECTRUM%22%7D,%7B%22dataProduct%22%3A%22MEASUREMENT_SET%22%7D%5D&facets=%5B%5D&showRejected=false&releasedFilter=released&includeCommensalProjects=false&showFacets=true&project=AS101%20-%20ASKAP%20Pilot%20Survey%20for%20EMU&schedulingBlockId=40905)

**Data products needed:**
  - selavy-image.i.EMU_1554-55_band2.SB40625.cont.taylor.0.restored.conv.components.xml
  - selavy-image.i.EMU_1554-55_band2.SB40625.cont.taylor.0.restored.conv.islands.xml
  - weights.i.EMU_1554-55_band2.SB40625.cont.taylor.0.fits
  - weights.i.EMU_1554-55_band2.SB40625.cont.taylor.1.fits
3. [Search query for SB40625](https://data.csiro.au/domain/casdaObservation/results?dataProducts=%5B%7B%22dataProduct%22%3A%22CATALOGUE%22%7D,%7B%22dataProduct%22%3A%22IMAGE_CUBE%22%7D,%7B%22dataProduct%22%3A%22IMAGE_CUBE_ANCILLARY%22%7D,%7B%22dataProduct%22%3A%22SPECTRUM%22%7D,%7B%22dataProduct%22%3A%22MEASUREMENT_SET%22%7D%5D&facets=%5B%5D&showRejected=false&releasedFilter=released&includeCommensalProjects=false&showFacets=true&project=AS101%20-%20ASKAP%20Pilot%20Survey%20for%20EMU&schedulingBlockId=40625)

**Data products needed:**
  - image.i.NGC5044_3B_band2.SB40905.cont.taylor.0.restored.conv.fits
  - image.i.NGC5044_3B_band2.SB40905.cont.taylor.1.restored.conv.fits
  - weights.i.NGC5044_3B_band2.SB40905.cont.taylor.0.fits
  - weights.i.NGC5044_3B_band2.SB40905.cont.taylor.1.fits

## Running Selavy

The optimization can be turned ON with the `VariableThreshold.useSNROptimization` option in the parset passed to selavy. For more information on the options in the parset refer to the [Source Finding Documentation](https://yandasoft.readthedocs.io/en/latest/analysis/index.html)

Running Selavy will require a server grade machine with atleast 64 CPUs and approx 4GB memory per MPI process. Additionally a data center grade GPU like a Nvidia Tesla will be needed. The parsets used in our evaluation runs can be found in the [assets folder](./assets/)

```
# selavy.in
Selavy.VariableThreshold                        = true
Selavy.VariableThreshold.useSNROptimization     = true
```

```sh
# MPI run or Slurm
mpiexec -n 17 selavy -c selavy.in > selavy.log

# For multithreading optimization

# Spawn Selavy with each MPI working having access to 2 threads
mpiexec --map-by numa:PE=2 -n 17 selavy -c selavy.in > selavy.log

# Spawn Selavy with each MPI working having access to 4 threads
mpiexec --map-by numa:PE=4 -n 17 selavy -c selavy.in > selavy.log
```
